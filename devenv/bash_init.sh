[[ -f .venv/Scripts/activate ]] && source .venv/Scripts/activate
[[ -f .venv/bin/activate ]] && source .venv/bin/activate

debugpython() {
  # Usage: debugpy -m some_python_executable_package args
  # Then press F5 in VSCode with a remote attach debug config selected
  # that connects to localhost:5678
  python -m debugpy --listen 5678 --wait-for-client "$@"
}

debugpytest() {
  # Usage: debugpytest pytest_args
  # Then press F5 in VSCode with a remote attach debug config selected
  # that connects to localhost:5678
  # Coverage is explicitly disabled because it messes up with VScode breakpoints
  python -m debugpy --listen 5678 --wait-for-client -m pytest --no-cov "$@"
}

# source: https://gist.github.com/judy2k/7656bfe3b322d669ef75364a46327836
# usage: export_envs FILE
export_envs() {
  local envFile=${1:-.env}
  while IFS='=' read -r key temp || [ -n "$key" ]; do
    local isComment='^[[:space:]]*#'
    local isBlank='^[[:space:]]*$'
    [[ $key =~ $isComment ]] && continue
    [[ $key =~ $isBlank ]] && continue
    value=$(eval echo "$temp")
    eval export "$key='$value'";
  done < $envFile
}

# You need to add these variables to your secret .env file at the repository root
export C2BA_WORKER_KEY_PASSPHRASE="the c2ba passphrase"
export C2BA_WORKER_CONFIG_DIRPATH="config"
export C2BA_ENV="develop"
export C2BA_VERBOSE="1"

# For c2ba-crypto utility, same as in config/config.toml:
export C2BA_KEY_PASSPHRASE=$C2BA_WORKER_KEY_PASSPHRASE
export C2BA_KEY_SALT=230c4cc6afd70e156d1c0d13b25c9179796e490e4fadac8aa114f2126391340b

build_docker_dev() {
  VERSION=`git describe --tags --dirty`
  docker build --pull --tag registry.gitlab.com/c2ba/c2ba-toolbox/worker-dev:$VERSION --file docker/worker.Dockerfile . --target development
}

build_docker_prod() {
  VERSION=`git describe --tags --dirty`
  docker build --pull --tag registry.gitlab.com/c2ba/c2ba-toolbox/worker:$VERSION --file docker/worker.Dockerfile . --target production
}

run_docker_dev() {
  VERSION=`git describe --tags --dirty`
  MSYS2_ARG_CONV_EXCL="*" MSYS_NO_PATHCONV=1 docker run \
    -v "${PWD}:/app" --name c2ba-worker --rm -it registry.gitlab.com/c2ba/c2ba-toolbox/worker-dev:$VERSION "$@"
}

python_docker() {
  VERSION=`git describe --tags --dirty`
  MSYS2_ARG_CONV_EXCL="*" MSYS_NO_PATHCONV=1 docker run \
    -v "${PWD}:/app" --name c2ba-worker --rm -it registry.gitlab.com/c2ba/c2ba-toolbox/worker-dev:$VERSION python "$@"
}

debugpython_docker() {
  VERSION=`git describe --tags --dirty`
  MSYS2_ARG_CONV_EXCL="*" MSYS_NO_PATHCONV=1 docker run \
    -p 5678:5678 -v "${PWD}:/app" --name c2ba-worker --rm -it registry.gitlab.com/c2ba/c2ba-toolbox/worker-dev:$VERSION python -m debugpy --listen 0.0.0.0:5678 --wait-for-client "$@"
}

pytest_docker() {
  VERSION=`git describe --tags --dirty`
  MSYS2_ARG_CONV_EXCL="*" MSYS_NO_PATHCONV=1 docker run \
    -v "${PWD}:/app" --rm -it registry.gitlab.com/c2ba/c2ba-toolbox/worker-dev:$VERSION python -m pytest "$@"
}

debugpytest_docker() {
  VERSION=`git describe --tags --dirty`
  MSYS2_ARG_CONV_EXCL="*" MSYS_NO_PATHCONV=1 docker run \
    -p 5678:5678 -v "${PWD}:/app" --rm -it registry.gitlab.com/c2ba/c2ba-toolbox/worker-dev:$VERSION python -m debugpy --listen 0.0.0.0:5678 --wait-for-client -m pytest --no-cov "$@"
}

run_docker_worker_dev() {
  build_docker_dev
  VERSION=`git describe --tags --dirty`

  docker kill c2ba-worker || true
  MSYS2_ARG_CONV_EXCL="*" MSYS_NO_PATHCONV=1 docker run \
    --env-file .env \
    -e "C2BA_REBALANCE_DRY_RUN=1" \
    -e "C2BA_WORKER_KEY_PASSPHRASE=$C2BA_WORKER_KEY_PASSPHRASE" \
    -t \
    --name c2ba-worker \
    -p 8000:8000 \
    --rm \
    -v ${PWD}:/app \
    registry.gitlab.com/c2ba/c2ba-toolbox/worker-dev:$VERSION
}

run_docker_worker_prod() {
  build_docker_prod
  VERSION=`git describe --tags --dirty`

  docker kill c2ba-worker || true
  MSYS2_ARG_CONV_EXCL="*" MSYS_NO_PATHCONV=1 docker run \
    --env-file .env \
    -e "C2BA_REBALANCE_DRY_RUN=1" \
    -e "C2BA_WORKER_KEY_PASSPHRASE=$C2BA_WORKER_KEY_PASSPHRASE" \
    -t \
    --name c2ba-worker \
    -p 8000:8000 \
    --rm \
    -v ${PWD}/config:/app/config \
    registry.gitlab.com/c2ba/c2ba-toolbox/worker:$VERSION
}

run_test_rebalance() {
  c2ba-worker-config get-rebalance-config test 76.4 | c2ba-coinbase rebalance --dry-run
}

# source: https://gist.github.com/judy2k/7656bfe3b322d669ef75364a46327836
# usage: export_envs FILE
export_envs() {
  local envFile=${1:-.env}
  while IFS='=' read -r key temp || [ -n "$key" ]; do
    local isComment='^[[:space:]]*#'
    local isBlank='^[[:space:]]*$'
    [[ $key =~ $isComment ]] && continue
    [[ $key =~ $isBlank ]] && continue
    value=$(eval echo "$temp")
    eval export "$key='$value'";
  done < $envFile
}

if [ -f .local/.env ]
then
  export_envs .local/.env
else
  echo "No .local/.env file found. You should probably create one by copying and changing devenv/templates/.env file"
fi
