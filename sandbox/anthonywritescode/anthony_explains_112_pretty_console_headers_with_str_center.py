import shutil

from c2ba.utils.cli import (
    make_app,
)
from c2ba.utils.logging import (
    get_logger,
)

app = make_app("0.1.0")
logger = get_logger(__name__)


@app.command()
def main() -> None:
    width, _ = shutil.get_terminal_size()
    print(" header ".center(width, "="))
    print("aligned right".rjust(width))
    print("aligned left".ljust(width) + "after")


if __name__ == "__main__":
    app()
