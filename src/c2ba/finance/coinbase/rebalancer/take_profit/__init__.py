"""
Take Profit Strategy
The goal of this simple strategy is to interpolate crypto allocs according to price targets.
"""
from __future__ import (
    annotations,
)

from decimal import (
    Decimal,
)
from typing import (
    Any,
    List,
)

from pandas import (
    DataFrame,
)

import c2ba.finance.coinbase.rebalancer.take_profit.exceptions as errors

CANDLE_HIGH_INDEX = 2


class TakeProfitConfig:
    crypto_allocs: List[Decimal]
    prices: List[Decimal]

    def __init__(self, crypto_allocs: List[Decimal], prices: List[Decimal]) -> None:
        self.crypto_allocs = crypto_allocs
        self.prices = prices
        self.check_validity()

    def to_json(self) -> Any:
        return {
            "crypto_allocs": [str(value) for value in self.crypto_allocs],
            "prices": [str(value) for value in self.prices],
        }

    def check_validity(self) -> None:
        if len(self.crypto_allocs) != len(self.prices):
            raise errors.TakeProfitConfigMatchError()

        if len(self.crypto_allocs) == 0:
            raise errors.TakeProfitConfigEmptyError()

        for crypto_alloc in self.crypto_allocs:
            if crypto_alloc < Decimal(0) or crypto_alloc > Decimal(100):
                raise errors.TakeProfitConfigNonPercentageCryptoAllocError()

        for price in self.prices:
            if price < Decimal(0):
                raise errors.TakeProfitConfigNegativePriceError()

        current_crypto_alloc = self.crypto_allocs[0]
        for crypto_alloc in self.crypto_allocs:
            if crypto_alloc > current_crypto_alloc:
                raise errors.TakeProfitConfigNonDecreasingCryptoAllocsError()
            current_crypto_alloc = crypto_alloc

        current_price = self.prices[0]
        for price in self.prices:
            if price < current_price:
                raise errors.TakeProfitConfigNonIncreasingPricesError()
            current_price = price


class TakeProfitState:
    ath: Decimal
    state: int
    take_profit_config: TakeProfitConfig

    def __init__(
        self, ath: Decimal, state: int, take_profit_config: TakeProfitConfig
    ) -> None:
        self.ath = ath
        self.state = state
        self.take_profit_config = take_profit_config

    def to_json(self) -> Any:
        return {
            "ath": str(self.ath),
            "state": self.state,
            "take_profit_config": self.take_profit_config.to_json(),
        }

    def update(
        self,
        high_price: Decimal,
    ) -> TakeProfitState:
        ath = max(self.ath, high_price)
        return TakeProfitState(
            ath=ath,
            state=_compute_state_from_all_time_high(
                self.state,
                ath,
                self.take_profit_config,
            ),
            take_profit_config=self.take_profit_config,
        )

    def crypto_alloc(self) -> Decimal:
        take_profit = self.take_profit_config
        if self.state == 0:
            return take_profit.crypto_allocs[0]
        if self.state >= len(take_profit.prices):
            return take_profit.crypto_allocs[-1]

        # linear interpolation of ath in current price range
        start_crypto_alloc = take_profit.crypto_allocs[self.state - 1]
        crypto_alloc_range = (
            take_profit.crypto_allocs[self.state]
            - take_profit.crypto_allocs[self.state - 1]
        )
        price_range = (
            take_profit.prices[self.state] - take_profit.prices[self.state - 1]
        )
        t = (self.ath - take_profit.prices[self.state - 1]) / price_range
        return start_crypto_alloc + t * crypto_alloc_range


def make_take_profit_state_from_candles(
    take_profit_config: TakeProfitConfig,
    candles: List[List[float]],
) -> TakeProfitState:
    ath = compute_all_time_high_from_candles(candles)
    return TakeProfitState(
        ath=ath,
        state=_compute_state_from_all_time_high(
            0,
            ath,
            take_profit_config,
        ),
        take_profit_config=take_profit_config,
    )


def compute_all_time_high_from_candles(
    candles: List[List[float]],
) -> Decimal:
    ath = 0.0
    for candle in candles:
        ath = max(ath, candle[CANDLE_HIGH_INDEX])
    return Decimal(str(ath))


def _compute_state_from_all_time_high(
    current_state: int,
    ath: Decimal,
    take_profit: TakeProfitConfig,
) -> int:
    while current_state < len(take_profit.prices):
        if ath < take_profit.prices[current_state]:
            return current_state
        current_state += 1
    return current_state


def compute_take_profit_projection_dataframe(
    take_profit: TakeProfitConfig,
    ath: Decimal,
    last_price: Decimal,
    current_take_profit_crypto_alloc: Decimal,
    portfolio_total: Decimal,
) -> DataFrame:
    current_crypto_alloc = take_profit.crypto_allocs[0]
    crypto_alloc = []
    prices = []
    prices_increase = []
    portfolio_estimate_stable = []
    portfolio_estimate_crypto = []
    portfolio_estimate_total = []
    state = []

    current_total = portfolio_total
    current_stable = current_total * (100 - current_take_profit_crypto_alloc) / 100
    current_crypto = current_total * current_take_profit_crypto_alloc / 100

    current_state = 1
    while current_crypto_alloc >= take_profit.crypto_allocs[-1]:
        price_range = (
            take_profit.prices[current_state] - take_profit.prices[current_state - 1]
        )
        crypto_alloc_range = (
            take_profit.crypto_allocs[current_state]
            - take_profit.crypto_allocs[current_state - 1]
        )
        price = round(
            (current_crypto_alloc - take_profit.crypto_allocs[current_state - 1])
            * price_range
            / crypto_alloc_range
            + take_profit.prices[current_state - 1],
            2,
        )

        if price > ath:
            state.append(current_state)

            crypto_alloc.append(current_crypto_alloc)
            prices.append(price)

            price_increase = (price - last_price) / last_price
            prices_increase.append(round(100 * price_increase, 2))

            last_price = price

            current_crypto = current_crypto * (1 + price_increase)
            current_total = current_stable + current_crypto

            current_crypto = current_total * (current_crypto_alloc / 100)
            current_stable = current_total * (100 - current_crypto_alloc) / 100

            portfolio_estimate_stable.append(round(current_stable, 2))
            portfolio_estimate_crypto.append(Decimal(round(current_crypto)))
            portfolio_estimate_total.append(Decimal(round(current_total)))

        # crypto_alloc_range is negative so we add to current_crypto_alloc
        current_crypto_alloc += crypto_alloc_range * Decimal("0.1")
        if current_crypto_alloc < take_profit.crypto_allocs[current_state]:
            current_state += 1
    return DataFrame(
        [
            prices,
            state,
            crypto_alloc,
            prices_increase,
            portfolio_estimate_total,
            portfolio_estimate_stable,
            portfolio_estimate_crypto,
        ],
        index=[
            "price",
            "state",
            "crypto alloc",
            "% PnL",
            "total estimate",
            "stable estimate",
            "crypto estimate",
        ],
    )
