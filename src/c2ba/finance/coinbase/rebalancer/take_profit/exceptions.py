class TakeProfitConfigError(ValueError):
    pass


class TakeProfitConfigMatchError(TakeProfitConfigError):
    def __init__(self) -> None:
        super().__init__(
            "Number of take profit crypto allocs should match number of prices"
        )


class TakeProfitConfigEmptyError(TakeProfitConfigError):
    def __init__(self) -> None:
        super().__init__(
            "At least one take profit crypto alloc and price should be provided"
        )


class TakeProfitConfigNonPercentageCryptoAllocError(TakeProfitConfigError):
    def __init__(self) -> None:
        super().__init__("Crypto allocs should be percentages in range [0,100]")


class TakeProfitConfigNegativePriceError(TakeProfitConfigError):
    def __init__(self) -> None:
        super().__init__("Prices should not be negative")


class TakeProfitConfigNonDecreasingCryptoAllocsError(TakeProfitConfigError):
    def __init__(self) -> None:
        super().__init__("Crypto allocs must decrease as prices increase")


class TakeProfitConfigNonIncreasingPricesError(TakeProfitConfigError):
    def __init__(self) -> None:
        super().__init__("Prices must increase as crypto alloc decrease")
