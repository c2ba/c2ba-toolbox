from __future__ import (
    annotations,
)

import logging
import re
from contextlib import (
    redirect_stdout,
)
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from functools import (
    reduce,
)
from io import (
    StringIO,
)
from operator import (
    add,
)
from typing import (
    Any,
    Dict,
    List,
    Mapping,
    Optional,
)

import pandas as pd

from c2ba.finance.coinbase.exchange import (
    CoinbaseExchange,
    CoinbaseSubmittedOrder,
    ExchangeContext,
    make_exchange_context,
    submit_orders,
)

logger = logging.getLogger(__name__)


@dataclass
class RebalanceConfig:
    asset_allocs: Dict[str, Decimal]  # Asset allocations in percentage
    threshold: Decimal

    def to_json(self) -> Any:
        return {
            "threshold": str(self.threshold),
            "asset_allocs": {
                asset: str(alloc) for asset, alloc in self.asset_allocs.items()
            },
        }

    @classmethod
    def from_json(cls, json_dict: Dict) -> RebalanceConfig:
        return RebalanceConfig(
            {
                asset: Decimal(alloc)
                for asset, alloc in json_dict["asset_allocs"].items()
            },
            Decimal(json_dict["threshold"]),
        )


def round_from_precision(precision: str) -> int:
    if re.match("^0\.[0]*1[0]*$", precision):  # noqa: W605
        decimals = precision.split(".")[1]
        offset = len(decimals.split("1")[0])
        return offset + 1
    if precision == "1" or re.match("^1\.[0]+$", precision):  # noqa: W605
        return 0
    raise ValueError(f"Invalid precision {precision}")


def get_asset_round(currencies_dict: Mapping, asset: str) -> int:
    return round_from_precision(currencies_dict[asset]["max_precision"])


def get_product_round(products_dict: Mapping, product: str, increment: str) -> int:
    return round_from_precision(products_dict[product][f"{increment}_increment"])


def safe_divide(numerator: Decimal, denominator: Decimal) -> Decimal:
    return numerator / denominator if denominator != Decimal(0) else Decimal(0)


@dataclass
class PortfolioRecord:
    asset: str
    relative_percent_diff: Decimal
    global_percent_diff: Decimal
    current_percent: Decimal
    target_percent: Decimal
    price_btc: Decimal
    price_usdc: Decimal
    current_value_btc: Decimal
    target_value_btc: Decimal
    diff_btc: Decimal
    current_value_usdc: Decimal
    target_value_usdc: Decimal
    diff_usdc: Decimal
    current_qty: Decimal
    target_qty: Decimal
    diff_qty: Decimal

    def to_json(self) -> Any:
        return {
            "asset": self.asset,
            "relative_percent_diff": str(self.relative_percent_diff),
            "global_percent_diff": str(self.global_percent_diff),
            "current_percent": str(self.current_percent),
            "target_percent": str(self.target_percent),
            "price_btc": str(self.price_btc),
            "price_usdc": str(self.price_usdc),
            "current_value_btc": str(self.current_value_btc),
            "target_value_btc": str(self.target_value_btc),
            "diff_btc": str(self.diff_btc),
            "current_value_usdc": str(self.current_value_usdc),
            "target_value_usdc": str(self.target_value_usdc),
            "diff_usdc": str(self.diff_usdc),
            "current_qty": str(self.current_qty),
            "target_qty": str(self.target_qty),
            "diff_qty": str(self.diff_qty),
        }


@dataclass
class MarketOrderRecord:
    asset: str  # asset to rebalance
    product: str
    side: str
    pivot_asset_qty: Decimal  # probably BTC
    asset_qty: Decimal
    usdc_qty: Decimal
    usdc_fees: Decimal
    fees_portfolio_percent: Decimal

    def to_json(self) -> Any:
        return {
            "asset": self.asset,
            "product": self.product,
            "side": self.side,
            "pivot_asset_qty": str(self.pivot_asset_qty),
            "asset_qty": str(self.asset_qty),
            "usdc_qty": str(self.usdc_qty),
            "usdc_fees": str(self.usdc_fees),
            "fees_portfolio_percent": str(self.fees_portfolio_percent),
        }

    def to_coinbase_orders(
        self, products_dict: Mapping, tickers: Mapping
    ) -> CoinbaseOrders:
        price = Decimal(tickers[self.product]["price"])
        valid_orders = []
        invalid_orders = []
        if self.asset == self.product.split("-")[0]:
            remaining_qty = round(
                self.pivot_asset_qty,
                get_product_round(products_dict, self.product, "quote"),
            )
            split_index = 0
            while remaining_qty > 0:
                funds = round(
                    remaining_qty,
                    get_product_round(products_dict, self.product, "quote"),
                )
                min_market_funds = Decimal(
                    products_dict[self.product]["min_market_funds"]
                )
                if funds < min_market_funds:
                    logger.debug(
                        f"Invalid order on {self.product} with funds {funds} < {min_market_funds}. Trying size order..."
                    )
                    size = round(
                        safe_divide(funds, price),
                        get_product_round(products_dict, self.product, "base"),
                    )
                    base_min_size = Decimal(
                        products_dict[self.product]["base_min_size"]
                    )
                    if size < base_min_size:
                        logger.debug(
                            f"Invalid order on {self.product} with size {size} < {base_min_size}"
                        )
                        invalid_orders.append(
                            {
                                "asset": self.asset,
                                "type": "market",
                                "side": self.side.lower(),
                                "product_id": self.product,
                                "funds": funds,
                                "min_market_funds": min_market_funds,
                                "size": size,
                                "base_min_size": base_min_size,
                                "split_index": split_index,
                            }
                        )
                        break
                    base_max_size = Decimal(
                        products_dict[self.product]["base_max_size"]
                    )
                    size_in_order = min(size, base_max_size)
                    valid_orders.append(
                        {
                            "asset": self.asset,
                            "type": "market",
                            "side": self.side.lower(),
                            "product_id": self.product,
                            "size": str(size_in_order),
                            "split_index": split_index,
                        }
                    )
                    remaining_qty -= round(
                        size_in_order * price,
                        get_product_round(products_dict, self.product, "quote"),
                    )
                else:
                    max_market_funds = Decimal(
                        products_dict[self.product]["max_market_funds"]
                    )
                    funds_in_order = min(funds, max_market_funds)
                    valid_orders.append(
                        {
                            "asset": self.asset,
                            "type": "market",
                            "side": self.side.lower(),
                            "product_id": self.product,
                            "funds": str(funds_in_order),
                            "split_index": split_index,
                        }
                    )
                    remaining_qty -= funds_in_order
                split_index += 1
        else:
            remaining_qty = round(
                self.pivot_asset_qty,
                get_product_round(products_dict, self.product, "base"),
            )
            split_index = 0
            while remaining_qty > 0:
                size = round(
                    remaining_qty,
                    get_product_round(products_dict, self.product, "base"),
                )
                base_min_size = Decimal(products_dict[self.product]["base_min_size"])
                if size < base_min_size:
                    logger.debug(
                        f"Invalid order on {self.product} with size {size} < {base_min_size}. Trying funds order..."
                    )
                    funds = round(
                        size * price,
                        get_product_round(products_dict, self.product, "quote"),
                    )
                    min_market_funds = Decimal(
                        products_dict[self.product]["min_market_funds"]
                    )
                    if funds < min_market_funds:
                        logger.debug(
                            f"Invalid order on {self.product} with funds {funds} < {min_market_funds}"
                        )
                        invalid_orders.append(
                            {
                                "asset": self.asset,
                                "type": "market",
                                "side": self.side.lower(),
                                "product_id": self.product,
                                "funds": funds,
                                "min_market_funds": min_market_funds,
                                "size": size,
                                "base_min_size": base_min_size,
                                "split_index": split_index,
                            }
                        )
                        break
                    max_market_funds = Decimal(
                        products_dict[self.product]["max_market_funds"]
                    )
                    funds_in_order = min(funds, max_market_funds)
                    valid_orders.append(
                        {
                            "asset": self.asset,
                            "type": "market",
                            "side": self.side.lower(),
                            "product_id": self.product,
                            "funds": str(funds_in_order),
                            "split_index": split_index,
                        }
                    )
                    remaining_qty -= round(
                        safe_divide(funds_in_order, price),
                        get_product_round(products_dict, self.product, "base"),
                    )
                else:
                    base_max_size = Decimal(
                        products_dict[self.product]["base_max_size"]
                    )
                    size_in_order = min(size, base_max_size)
                    valid_orders.append(
                        {
                            "asset": self.asset,
                            "type": "market",
                            "side": self.side.lower(),
                            "product_id": self.product,
                            "size": str(size_in_order),
                            "split_index": split_index,
                        }
                    )
                    remaining_qty -= size_in_order
                split_index += 1
        return CoinbaseOrders(valid_orders, invalid_orders)


@dataclass
class CoinbaseOrders:
    valid_orders: List[Dict]
    invalid_orders: List[Dict]


@dataclass
class ComputeRebalanceOutput:
    start_time: datetime
    portfolio_records: List[PortfolioRecord]
    rebalance_order_records: List[MarketOrderRecord]
    total_usdc_fees: Decimal

    def to_json(self) -> Any:
        return {
            "start_time": self.start_time.strftime("%Y-%m-%d %H:%M:%S"),
            "portfolio_records": [
                record.to_json() for record in self.portfolio_records
            ],
            "rebalance_order_records": [
                record.to_json() for record in self.rebalance_order_records
            ],
            "total_usdc_fees": str(self.total_usdc_fees),
        }

    @property
    def total_usdc_value(self) -> Decimal:
        total = Decimal(0)
        for record in self.portfolio_records:
            total += record.current_value_usdc
        return total

    def stable_usdc_value(self, currencies: Dict[str, Dict]) -> Decimal:
        total = Decimal(0)
        for record in self.portfolio_records:
            group_types = currencies[record.asset]["details"]["group_types"]
            if "stablecoin" in group_types or "fiat" in group_types:
                total += record.current_value_usdc
        return total

    def crypto_usdc_value(self, currencies: Dict[str, Dict]) -> Decimal:
        total = Decimal(0)
        for record in self.portfolio_records:
            group_types = currencies[record.asset]["details"]["group_types"]
            if "stablecoin" not in group_types and "fiat" not in group_types:
                total += record.current_value_usdc
        return total


def compute_rebalance(  # noqa: C901 too complex
    target_alloc: Dict[str, Decimal],
    exchange_context: ExchangeContext,
) -> ComputeRebalanceOutput:
    fees = exchange_context.fees
    taker_fee_rate = Decimal(fees["taker_fee_rate"])
    currencies_dict = exchange_context.currencies
    products_dict = exchange_context.products
    all_tickers = exchange_context.tickers

    if "BTC-USDC" not in all_tickers:
        raise ValueError("BTC-USDC not in all_tickers")

    for asset in target_alloc:
        if asset in ("USDC", "BTC"):
            continue
        if f"{asset}-BTC" not in all_tickers:
            raise ValueError(f"{asset}-BTC not in all_tickers")

    portfolio_quantities = {
        asset: exchange_context.asset_quantities.get(asset, Decimal(0))
        for asset in target_alloc
    }

    btc_usdc = Decimal(all_tickers["BTC-USDC"]["price"])
    usd_btc = round(
        safe_divide(Decimal(1), btc_usdc),
        get_product_round(products_dict, "BTC-USDC", "base"),
    )
    logger.debug(f"BTC/USDC: {btc_usdc} USDC/BTC: {usd_btc}")

    btc_round = get_asset_round(currencies_dict, "BTC")

    def get_price_btc(asset: str) -> Decimal:
        if asset == "BTC":
            return round(Decimal(1), btc_round)
        if asset == "USDC":
            return round(usd_btc, btc_round)
        return round(Decimal(all_tickers[f"{asset}-BTC"]["price"]), btc_round)

    def get_value(asset: str) -> Decimal:
        return round(portfolio_quantities[asset] * get_price_btc(asset), btc_round)

    portfolio_values = {asset: get_value(asset) for asset in portfolio_quantities}

    total_value = reduce(add, portfolio_values.values(), Decimal(0))

    portfolio_alloc = {
        asset: safe_divide(portfolio_values[asset], total_value)
        for asset in portfolio_values
    }
    portfolio_percents = {
        asset: round(100 * portfolio_alloc[asset], 2) for asset in portfolio_values
    }

    portfolio_records = []

    for asset in target_alloc:
        target_value = (target_alloc[asset] / 100) * total_value
        current_value = portfolio_values[asset]
        diff_value = target_value - current_value
        if current_value > Decimal(0):
            relative_percent_diff = round(
                100 * safe_divide(diff_value, current_value), 2
            )
        else:
            if target_value > Decimal(0):
                relative_percent_diff = Decimal("inf")
            else:
                relative_percent_diff = Decimal(0)
        global_percent_diff = round(100 * safe_divide(diff_value, total_value), 2)

        current_value_usdc = round(current_value * btc_usdc, 2)
        target_value_usdc = round(target_value * btc_usdc, 2)

        asset_round = get_asset_round(currencies_dict, asset)

        current_qty = round(portfolio_quantities[asset], asset_round)
        target_qty = round(target_value / get_price_btc(asset), asset_round)

        diff_qty = target_qty - current_qty

        price_btc = get_price_btc(asset)

        portfolio_records.append(
            PortfolioRecord(
                asset=asset,
                relative_percent_diff=relative_percent_diff,
                global_percent_diff=global_percent_diff,
                price_btc=price_btc,
                current_value_btc=current_value,
                target_value_btc=target_value,
                diff_btc=target_value - current_value,
                price_usdc=price_btc * btc_usdc,
                current_value_usdc=current_value_usdc,
                target_value_usdc=target_value_usdc,
                diff_usdc=target_value_usdc - current_value_usdc,
                current_qty=current_qty,
                target_qty=target_qty,
                diff_qty=diff_qty,
                current_percent=portfolio_percents[asset],
                target_percent=target_alloc[asset],
            )
        )

    def pf_record_display(record: PortfolioRecord) -> Dict:
        return {
            **record.__dict__,
            "diff_btc": round(record.diff_btc, btc_round),
            "target_percent": round(record.target_percent, 2),
            "price_usdc": round(record.price_usdc, 2),
            "target_value_btc": round(record.target_value_btc, btc_round),
        }

    portfolio_df = pd.DataFrame.from_records(
        [pf_record_display(record) for record in portfolio_records]
    )
    pd.set_option("display.max_rows", None)
    pd.set_option("display.max_columns", None)
    pd.set_option("display.width", None)
    pd.set_option("display.max_colwidth", None)

    sum_row = portfolio_df.sum(axis=0)
    sum_row["asset"] = ""
    sum_row["relative_percent_diff"] = portfolio_df.relative_percent_diff.abs().max()
    sum_row["global_percent_diff"] = portfolio_df.global_percent_diff.abs().sum()
    sum_row["price_btc"] = ""
    sum_row["price_usdc"] = ""
    sum_row["current_qty"] = ""
    sum_row["target_qty"] = ""
    sum_row["diff_qty"] = ""

    portfolio_df = portfolio_df.append(sum_row, ignore_index=True)

    start_time = datetime.now()
    print(f"<h1>Rebalance Report {start_time}</h1>")

    print("<h2>Config</h2>")
    print(
        pd.DataFrame.from_records(
            [
                {"asset": key, "alloc": round(value, 2)}
                for key, value in target_alloc.items()
            ]
        )
        .transpose()
        .to_html(header=False)
    )

    print("<h2>Portfolio</h2>")
    print(portfolio_df.transpose().to_html(header=False, na_rep=""))

    order_records: List[MarketOrderRecord] = []

    for record in portfolio_records:
        if record.asset != "BTC" and record.asset != "USDC":
            if record.diff_btc > 0:
                quantity_to_buy = round(
                    record.diff_btc,
                    get_product_round(products_dict, f"{record.asset}-BTC", "quote"),
                )
                logger.debug(
                    f"\t BUY on {record.asset}-BTC for {quantity_to_buy} BTC (diff: {record.relative_percent_diff} %)"
                )
                order_records.append(
                    MarketOrderRecord(
                        asset=record.asset,
                        product=f"{record.asset}-BTC",
                        side="BUY",
                        pivot_asset_qty=abs(record.diff_btc),
                        asset_qty=abs(record.diff_qty),
                        usdc_qty=abs(record.diff_btc * btc_usdc),
                        usdc_fees=abs(record.diff_btc * btc_usdc * taker_fee_rate),
                        fees_portfolio_percent=100
                        * abs(
                            record.diff_btc * safe_divide(taker_fee_rate, total_value)
                        ),
                    )
                )
            else:
                quantity_to_sell = abs(
                    round(
                        record.diff_qty,
                        get_product_round(products_dict, f"{record.asset}-BTC", "base"),
                    )
                )
                logger.debug(
                    f"\t SELL on {record.asset}-BTC for {quantity_to_sell} {record.asset} (diff: {record.relative_percent_diff} %)"
                )
                order_records.append(
                    MarketOrderRecord(
                        asset=record.asset,
                        product=f"{record.asset}-BTC",
                        side="SELL",
                        pivot_asset_qty=abs(record.diff_btc),
                        asset_qty=abs(record.diff_qty),
                        usdc_qty=abs(record.diff_btc * btc_usdc),
                        usdc_fees=abs(record.diff_btc * btc_usdc * taker_fee_rate),
                        fees_portfolio_percent=100
                        * abs(
                            record.diff_btc * safe_divide(taker_fee_rate, total_value)
                        ),
                    )
                )
        if record.asset == "USDC":
            if record.diff_btc < 0:
                # buying btc on btc-usdc is like selling usdc
                quantity_to_buy = round(
                    record.diff_qty,
                    get_product_round(products_dict, f"BTC-{record.asset}", "quote"),
                )
                logger.debug(
                    f"\t BUY on BTC-{record.asset} for {quantity_to_buy} {record.asset} (diff: {record.relative_percent_diff} %)"
                )
                order_records.append(
                    MarketOrderRecord(
                        asset=record.asset,
                        product=f"BTC-{record.asset}",
                        side="BUY",
                        pivot_asset_qty=abs(record.diff_btc),
                        asset_qty=abs(record.diff_qty),
                        usdc_qty=abs(record.diff_btc * btc_usdc),
                        usdc_fees=abs(record.diff_btc * btc_usdc * taker_fee_rate),
                        fees_portfolio_percent=100
                        * abs(
                            record.diff_btc * safe_divide(taker_fee_rate, total_value)
                        ),
                    )
                )
            else:
                # selling btc on btc-usdc is like buying btc
                quantity_to_sell = abs(
                    round(
                        record.diff_btc,
                        get_product_round(products_dict, f"BTC-{record.asset}", "base"),
                    )
                )
                logger.debug(
                    f"\t SELL on BTC-{record.asset} for {quantity_to_sell} BTC (diff: {record.relative_percent_diff} %)"
                )
                order_records.append(
                    MarketOrderRecord(
                        asset=record.asset,
                        product=f"BTC-{record.asset}",
                        side="SELL",
                        pivot_asset_qty=abs(record.diff_btc),
                        asset_qty=abs(record.diff_qty),
                        usdc_qty=abs(record.diff_btc * btc_usdc),
                        usdc_fees=abs(record.diff_btc * btc_usdc * taker_fee_rate),
                        fees_portfolio_percent=100
                        * abs(
                            record.diff_btc * safe_divide(taker_fee_rate, total_value)
                        ),
                    )
                )

    order_records = sorted(
        order_records, key=lambda record: 0 if record.side == "SELL" else 1
    )

    def market_order_record_display(record: MarketOrderRecord) -> Dict:
        return {
            **record.__dict__,
            "fees_portfolio_percent": round(record.fees_portfolio_percent, 4),
            "pivot_asset_qty": round(record.pivot_asset_qty, btc_round),
            "usdc_qty": round(record.usdc_qty, 2),
            "usdc_fees": round(record.usdc_fees, 2),
        }

    order_df = pd.DataFrame.from_records(
        [market_order_record_display(order) for order in order_records]
    )
    sum_row = order_df.sum(axis=0)
    sum_row["asset"] = ""
    sum_row["product"] = ""
    sum_row["side"] = ""
    sum_row["asset_qty"] = ""

    total_usdc_fees = sum_row["usdc_fees"]

    order_df = order_df.append(sum_row, ignore_index=True)

    print("<h2>Rebalance Orders</h2>")
    print(order_df.to_html(index=False, na_rep=""))

    return ComputeRebalanceOutput(
        start_time=start_time,
        portfolio_records=portfolio_records,
        rebalance_order_records=order_records,
        total_usdc_fees=total_usdc_fees,
    )


@dataclass
class ComputeRebalanceInput:
    rebalance_config: RebalanceConfig
    exchange_context: ExchangeContext

    def to_json(self) -> Any:
        return {
            "rebalance_config": self.rebalance_config.to_json(),
            "exchange_context": self.exchange_context.to_json(),
        }


@dataclass
class SubmitOrdersOutput:
    sell_orders: List[Dict]
    buy_orders: List[Dict]
    submitted_sell_orders: Dict[str, CoinbaseSubmittedOrder]
    submitted_buy_orders: Dict[str, CoinbaseSubmittedOrder]

    def to_json(self) -> Any:
        return {
            "sell_orders": self.sell_orders,
            "buy_orders": self.buy_orders,
            "submitted_sell_orders": {
                key: value.to_json()
                for key, value in self.submitted_sell_orders.items()
            },
            "submitted_buy_orders": {
                key: value.to_json() for key, value in self.submitted_buy_orders.items()
            },
        }


@dataclass
class RebalanceResult:
    compute_rebalance_output: ComputeRebalanceOutput
    compute_rebalance_input: ComputeRebalanceInput
    exchange_order_records: List[Dict]
    should_rebalance: bool
    submit_orders_output: Optional[SubmitOrdersOutput]
    html_report: List[str]

    def to_json(self) -> Any:
        return {
            "compute_rebalance_output": self.compute_rebalance_output.to_json(),
            "compute_rebalance_input": self.compute_rebalance_input.to_json(),
            "exchange_order_records": self.exchange_order_records,
            "should_rebalance": self.should_rebalance,
            "submit_orders_output": self.submit_orders_output.to_json()
            if self.submit_orders_output
            else None,
            "html_report": self.html_report,
        }

    def get_html_report(self) -> str:
        return "\n".join(self.html_report).strip()

    @property
    def total_usdc_fees(self) -> Decimal:
        return self.compute_rebalance_output.total_usdc_fees

    @property
    def total_usdc_value(self) -> Decimal:
        return self.compute_rebalance_output.total_usdc_value

    def stable_usdc_value(self) -> Decimal:
        return self.compute_rebalance_output.stable_usdc_value(
            self.compute_rebalance_input.exchange_context.currencies
        )

    def crypto_usdc_value(self) -> Decimal:
        return self.compute_rebalance_output.crypto_usdc_value(
            self.compute_rebalance_input.exchange_context.currencies
        )


def rebalance(
    rebalance_config: RebalanceConfig,
    exchange: CoinbaseExchange,
    dry_run: bool,
) -> RebalanceResult:
    report = StringIO()
    with redirect_stdout(report):
        exchange_context = make_exchange_context(
            exchange,
            list(set(["BTC", "USDC"] + list(rebalance_config.asset_allocs.keys()))),
        )

        compute_rebalance_output = compute_rebalance(
            rebalance_config.asset_allocs, exchange_context
        )

        coinbase_orders = make_coinbase_orders(
            compute_rebalance_output.rebalance_order_records,
            exchange_context.products,
            exchange_context.tickers,
        )
        valid_orders = coinbase_orders.valid_orders

        def make_result(
            should_rebalance: bool,
            submit_orders_output: Optional[SubmitOrdersOutput],
        ) -> RebalanceResult:
            return RebalanceResult(
                compute_rebalance_input=ComputeRebalanceInput(
                    rebalance_config=rebalance_config,
                    exchange_context=exchange_context,
                ),
                compute_rebalance_output=compute_rebalance_output,
                exchange_order_records=valid_orders,
                should_rebalance=should_rebalance,
                submit_orders_output=submit_orders_output,
                html_report=report.getvalue().splitlines(),
            )

        # todo: Check if trigger assets are in coinbase orders - if not, don't rebalance

        trigger_records = [
            record
            for record in compute_rebalance_output.portfolio_records
            if abs(record.relative_percent_diff) >= rebalance_config.threshold
            and (
                record.asset != "BTC"
                or rebalance_config.asset_allocs.get("BTC", Decimal("0")) > 0
            )  # BTC cannot be a trigger asset if not wanted in portfolio
        ]

        if len(trigger_records) == 0:
            message = f"No line has percentage difference > threshold (threshold={rebalance_config.threshold}): No rebalance"
            logger.debug(message)

            print("<h2>Log</h2>")
            print(f"<p>{message}</p>")

            return make_result(False, submit_orders_output=None)

        # Each trigger record must have a valid order, otherwise the portfolio will remain unbalanced
        trigger_asset_without_valid_order = {
            record.asset for record in trigger_records
        }.difference(
            {"BTC"}.union({order["asset"] for order in coinbase_orders.valid_orders})
        )

        if len(trigger_asset_without_valid_order) > 0:
            message = "The following trigger assets do not have a valid order, waiting for increased difference to rebalance."
            logger.debug(
                f"{message}\n\t" + "\t\n".join(trigger_asset_without_valid_order)
            )

            print("<h2>Log</h2>")
            print(
                f"<p>{message}\n<ul>\n"
                + "\n".join(
                    (
                        f"<li>{asset}</li>\n"
                        for asset in trigger_asset_without_valid_order
                    )
                )
                + "</ul>\n</p>"
            )

            return make_result(False, submit_orders_output=None)

        if dry_run:
            message = "Dry run: No rebalance"
            logger.debug(message)
            print("<h2>Log</h2>")
            print(f"<p>{message}</p>")

            return make_result(True, submit_orders_output=None)

        # Cleanup our fields to ensure coinbase don't reject
        for order in valid_orders:
            del order["asset"]
            del order["split_index"]

        submit_orders_output = submit_coinbase_orders(valid_orders, exchange)
        return make_result(True, submit_orders_output=submit_orders_output)


def submit_coinbase_orders(
    coinbase_orders: List[Dict],
    exchange: CoinbaseExchange,
) -> SubmitOrdersOutput:
    sell_orders = [order for order in coinbase_orders if order["side"] == "sell"]
    submitted_sell_orders = submit_orders(sell_orders, exchange)

    buy_orders = [order for order in coinbase_orders if order["side"] == "buy"]
    submitted_buy_orders = submit_orders(buy_orders, exchange)

    print("<h2>Submitted Orders Responses</h2>")
    print("<h3>Sell Orders</h3>")
    print(
        pd.DataFrame.from_records(
            [order.submit_response for order in submitted_sell_orders.values()],
        ).to_html(index=False, na_rep=""),
    )
    print(
        pd.DataFrame.from_records(
            reduce(
                add,
                (
                    order.status_query_responses
                    for order in submitted_sell_orders.values()
                ),
                [],
            )
        ).to_html(index=False, na_rep=""),
    )

    print("<h3>Buy Orders</h3>")
    print(
        pd.DataFrame.from_records(
            [order.submit_response for order in submitted_buy_orders.values()],
        ).to_html(index=False, na_rep=""),
    )
    print(
        pd.DataFrame.from_records(
            reduce(
                add,
                (
                    order.status_query_responses
                    for order in submitted_buy_orders.values()
                ),
                [],
            )
        ).to_html(index=False, na_rep=""),
    )

    return SubmitOrdersOutput(
        sell_orders=sell_orders,
        buy_orders=buy_orders,
        submitted_sell_orders=submitted_sell_orders,
        submitted_buy_orders=submitted_buy_orders,
    )


def make_coinbase_orders(
    rebalance_order_records: List[MarketOrderRecord], products: Dict, tickers: Dict
) -> CoinbaseOrders:
    valid_orders = []
    invalid_orders = []
    for order_record in rebalance_order_records:
        coinbase_orders = order_record.to_coinbase_orders(products, tickers)
        valid_orders.extend(coinbase_orders.valid_orders)
        invalid_orders.extend(coinbase_orders.invalid_orders)

    print("<h2>Invalid Coinbase Orders</h2>")
    invalid_orders_df = pd.DataFrame.from_records(invalid_orders)
    print(invalid_orders_df.to_html(index=False, na_rep=""))

    print("<h2>Valid Coinbase Orders to Submit</h2>")
    valid_order_df = pd.DataFrame.from_records(valid_orders)
    print(valid_order_df.to_html(index=False, na_rep=""))

    return CoinbaseOrders(valid_orders, invalid_orders)
