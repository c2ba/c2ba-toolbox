from __future__ import (
    annotations,
)

import base64
import hashlib
import hmac
import json
import logging
import re
import time
from contextlib import (
    contextmanager,
)
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
    ContextManager,
    Dict,
    List,
    Mapping,
    Optional,
)

import requests
from requests.auth import (
    AuthBase,
)

logger = logging.getLogger(__name__)

COINBASE_TIME_TO_SLEEP = 0.3
CBPRO_API_URL = "https://api.pro.coinbase.com/"


@dataclass
class ExchangeRequests:
    method: str
    endpoint: str
    data: Optional[str]
    params: Optional[Dict]

    response_status_code: int
    cb_after: Optional[str]
    response_json: Dict


@dataclass
class ProductStats:
    open: Decimal  # noqa: A003
    high: Decimal
    low: Decimal
    volume: Decimal
    last: Decimal
    volume_30day: Decimal


class CoinbaseExchange:
    _session: requests.Session
    auth: Optional[CoinbaseExchangeAuth]
    previous_request_time: float
    _history: Optional[List[ExchangeRequests]] = None

    def __init__(
        self,
        auth: Optional[CoinbaseExchangeAuth],
        api_url: str = CBPRO_API_URL,
    ) -> None:
        self.api_url = api_url if api_url.endswith("/") else f"{api_url}/"
        self.auth = auth
        self.previous_request_time = 0
        self._session = requests.Session()

    def client_request(  # noqa: C901
        self,
        method: str,
        endpoint: str,
        data: Optional[Mapping],
        params: Optional[Dict],
        paginated: bool = False,
    ) -> Any:
        current_time = time.time()
        elapsed_time = current_time - self.previous_request_time
        time.sleep(max(0, COINBASE_TIME_TO_SLEEP - elapsed_time))

        timeout_count = 0
        max_timeout = 10

        error_500_count = 0
        max_error_500 = 10

        paginated_result = []
        while True:
            try:
                req = requests.Request(
                    method.upper(),
                    self.api_url + endpoint,
                    data=json.dumps(data) if data is not None else None,
                    params=params,
                )
                prepped = req.prepare()
                prepped.prepare_auth(auth=self.auth)

                response = self._session.send(prepped, timeout=30)

                if self._history is not None:
                    self._history.append(
                        ExchangeRequests(
                            method.upper(),
                            endpoint,
                            req.data,
                            req.params,
                            response.status_code,
                            response.headers.get("cb-after"),
                            json.loads(response.content),
                        )
                    )

                response.raise_for_status()
                self.previous_request_time = current_time
                if not paginated:
                    return response.json()
                paginated_result.extend(response.json())
                if not response.headers.get("cb-after") or (
                    params is not None and params.get("before") is not None
                ):
                    break
                elif params is not None:
                    params = {
                        **params,
                        "after": response.headers["cb-after"],
                    }
                    logger.info("paginate !")
            except requests.Timeout as e:
                if timeout_count > max_timeout:
                    logging.error(
                        f"More than {timeout_count} timeouts on {method.upper()} {endpoint}, giving up."
                    )
                    raise e
                else:
                    logging.warning(
                        f"Timeout on {method.upper()} {endpoint} (trial {timeout_count + 1} / {max_timeout})"
                    )
                    time.sleep(0.5 * COINBASE_TIME_TO_SLEEP)
                    timeout_count += 1
            except requests.RequestException as e:
                if e.response.status_code == 400:
                    # Bad request, probably a dev error
                    logging.error(
                        f"Exception received on request: {e} ({e.response.json()['message']})"
                    )
                    raise e

                if e.response.status_code == 500:
                    # Internal Server Error for url [500]
                    if error_500_count > max_error_500:
                        logging.error(
                            f"More than {max_error_500} error 500 received on request: {e} ({e.response.json()['message']})"
                        )
                        raise e
                    else:
                        logging.warning(
                            f"Error 500 received on request: {e} ({e.response.json()['message']}) (trial {error_500_count + 1} / {error_500_count})"
                        )
                        time.sleep(0.5 * COINBASE_TIME_TO_SLEEP)
                        error_500_count += 1
                elif e.response.status_code == 429:
                    # Public rate limit exceeded [429]
                    logging.warning("Public rate limit exceeded, waiting a bit...")
                    time.sleep(COINBASE_TIME_TO_SLEEP)
                else:
                    raise e
        return paginated_result

    def products(self) -> List:
        return sorted(
            self.client_request("get", "products", None, None), key=lambda e: e["id"]
        )

    def candles(
        self,
        product_id: str,
        start: Optional[datetime],
        end: Optional[datetime],
        granularity: Optional[int],
    ) -> List:
        params = {}
        if start is not None:
            params["start"] = start.strftime("%Y-%m-%d %H:%M:%S")
        if end is not None:
            params["end"] = end.strftime("%Y-%m-%d %H:%M:%S")
        if granularity is not None:
            params["granularity"] = str(granularity)
        return self.client_request(
            "get", f"products/{product_id}/candles", None, params
        )

    def currencies(self) -> List:
        return sorted(
            self.client_request("get", "currencies", None, None), key=lambda e: e["id"]
        )

    def ticker(self, product_id: str) -> Dict:
        return self.client_request("get", f"products/{product_id}/ticker", None, None)

    def stats(self, product_id: str) -> ProductStats:
        stats_dict: Dict = self.client_request(
            "get", f"products/{product_id}/stats", None, None
        )
        return ProductStats(
            **{key: Decimal(value) for key, value in stats_dict.items()}
        )

    def accounts(self) -> List:
        return sorted(
            self.client_request("get", "accounts", None, None), key=lambda e: e["id"]
        )

    def fees(self) -> Dict:
        return self.client_request("get", "fees", None, None)

    def transfers(self) -> List:
        return self.client_request("get", "transfers", None, None)

    def fills(self, product_id: str) -> List:
        return self.client_request(
            "get", "fills", None, {"product_id": product_id}, True
        )

    def time(self) -> Dict:
        return self.client_request("get", "time", None, None)

    def get_order(self, order_id: str, retry_count: int = 1) -> Mapping[str, Any]:
        all_exceptions: List[Exception] = []
        for i in range(retry_count):
            try:
                return self.client_request("get", f"orders/{order_id}", None, None)
            except requests.RequestException as e:
                logging.error(
                    f"Exception received on request: {e} ({e.response.json()['message']}) (retry {i + 1}/{retry_count})"
                )
                all_exceptions.append(e)
                time.sleep(10 * COINBASE_TIME_TO_SLEEP)
            except Exception as e:
                logging.error(
                    f"Exception received on request: {e} (retry {i + 1}/{retry_count})"
                )
                all_exceptions.append(e)
                time.sleep(10 * COINBASE_TIME_TO_SLEEP)
        raise CoinbaseExchangeError(
            f"Unable to get order {order_id}: too many retries", all_exceptions
        )

    def submit_order(self, order: Mapping[str, str]) -> Mapping[str, Any]:
        return self.client_request("post", "orders", order, None)

    @contextmanager  # type: ignore
    def record_history(self) -> ContextManager[List]:  # type: ignore
        self._history = []
        try:
            yield self._history
        finally:
            self._history = None

    @property
    def valid_candle_granularities(self) -> List[int]:
        return [60, 300, 900, 3600, 21600, 86400]


# Create custom authentication for Exchange
class CoinbaseExchangeAuth(AuthBase):
    def __init__(self, api_key: str, secret_key: str, passphrase: str) -> None:
        self.api_key = api_key
        self.secret_key = secret_key
        self.passphrase = passphrase

    def __call__(self, request: Any) -> Any:
        timestamp = str(time.time())
        message_str = "".join(
            [timestamp, request.method, request.path_url, (request.body or "")]
        )
        message = message_str.encode("ascii")
        hmac_key = base64.b64decode(self.secret_key)
        signature = hmac.new(hmac_key, message, hashlib.sha256)
        signature_b64 = base64.b64encode(signature.digest()).decode("utf-8")

        request.headers.update(
            {
                "CB-ACCESS-SIGN": signature_b64,
                "CB-ACCESS-TIMESTAMP": timestamp,
                "CB-ACCESS-KEY": self.api_key,
                "CB-ACCESS-PASSPHRASE": self.passphrase,
                "Content-Type": "application/json",
            }
        )
        return request


class CoinbaseExchangeError(Exception):
    causes: List[Exception]

    def __init__(self, message: str, causes: Optional[List[Exception]] = None) -> None:
        super().__init__(message)
        self.causes = causes if causes is not None else []


@dataclass
class ExchangeContext:
    # todo replace with class instances:
    currencies: Dict[str, Dict]
    products: Dict[str, Dict]
    asset_quantities: Dict[str, Decimal]
    fees: Dict[str, str]
    tickers: Dict[str, Dict]

    def to_json(self) -> Any:
        return {
            "currencies": self.currencies,
            "products": self.products,
            "asset_quantities": {
                asset: str(quantity)
                for asset, quantity in self.asset_quantities.items()
            },
            "fees": self.fees,
            "tickers": self.tickers,
        }

    @classmethod
    def from_json(cls, json_dict: Dict) -> ExchangeContext:
        return ExchangeContext(
            json_dict["currencies"],
            json_dict["products"],
            {
                asset: Decimal(quantity)
                for asset, quantity in json_dict["asset_quantities"].items()
            },
            json_dict["fees"],
            json_dict["tickers"],
        )


def make_exchange_context(
    exchange: CoinbaseExchange,
    whitelist_currencies: List[str],
) -> ExchangeContext:
    currencies = exchange.currencies()
    products = exchange.products()
    accounts = exchange.accounts()
    fees = exchange.fees()

    currencies_dict = {currency["id"]: currency for currency in currencies}
    products_dict = {product["id"]: product for product in products}

    asset_quantities = {
        account["currency"]: Decimal(account["balance"])
        for account in accounts
        if account["currency"] in whitelist_currencies
    }

    tickers = {}

    for i, p in enumerate(products):
        if p["base_currency"] not in whitelist_currencies or p[
            "quote_currency"
        ] not in [
            "BTC",
            "USDC",
        ]:
            continue
        logger.debug(f"Getting tickler for {p['id']} ({i+1} / {len(products)})")
        tickers[p["id"]] = exchange.ticker(p["id"])

    return ExchangeContext(
        currencies=currencies_dict,
        products=products_dict,
        asset_quantities=asset_quantities,
        fees=fees,
        tickers=tickers,
    )


@dataclass
class CoinbaseSubmittedOrder:
    order_id: str
    status: str  # open, done, error
    submit_response: Mapping  # response from exchange, json
    status_query_responses: List[
        Mapping
    ]  # each time we query order status and it is not settled, store the result
    exception: Optional[
        Exception
    ]  # On error, store the exception that caused the error status

    def to_json(self) -> Any:
        return self.__dict__


def submit_orders(
    orders: List[Dict], exchange: CoinbaseExchange
) -> Dict[str, CoinbaseSubmittedOrder]:
    out_dict = {}
    open_orders = []
    for order in orders:
        try:
            logger.info(f"Placing market order {order}")
            order_response = exchange.submit_order(order)
            logger.info(f"\tOK order_id={order_response['id']}")
            submitted_order = CoinbaseSubmittedOrder(
                order_id=order_response["id"],
                status="open",
                submit_response=order_response,
                status_query_responses=[],
                exception=None,
            )

            open_orders.append(submitted_order)
            out_dict[submitted_order.order_id] = submitted_order
        except Exception as e:
            logger.error(f"Error for order {order}: {e}")
    time.sleep(0.5)

    while len(open_orders) > 0:
        for submitted_order in open_orders:
            try:
                logger.info(f"Querying status for order {submitted_order.order_id}")
                # Try 10 times before error because most often orders remains "pending" several seconds
                status_query_response = exchange.get_order(submitted_order.order_id, 10)
                logger.info(f"status = {status_query_response['status']}")

                submitted_order.status_query_responses.append(status_query_response)
                if status_query_response["settled"] is True:
                    submitted_order.status = "done"
            except Exception as e:
                logger.error(f"Error for order {submitted_order.order_id}: {e}")
                submitted_order.status = "error"
                submitted_order.exception = e
        open_orders = [order for order in open_orders if order.status == "open"]

    return out_dict


class InvalidTimeframeError(Exception):
    pass


def timeframe_to_seconds(timeframe: str) -> int:
    suffix_to_seconds = {
        "s": 1,
        "m": 60,
        "h": 60 * 60,
        "d": 3600 * 24,
        "w": 3600 * 24 * 7,
    }
    pattern = f"^([1-9][0-9]*)([{','.join(suffix_to_seconds.keys())}])$"  # noqa: W605
    re_search = re.search(pattern, timeframe)
    if not re_search:
        raise InvalidTimeframeError(
            f"Invalid {timeframe} timeframe. Should match regexp {pattern}."
        )
    return int(re_search.group(1)) * suffix_to_seconds[re_search.group(2)]
