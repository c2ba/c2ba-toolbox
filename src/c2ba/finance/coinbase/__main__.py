import json
import logging
import sys
from decimal import (
    Decimal,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
    List,
    Optional,
)

import moment
import toml
import typer

from c2ba.finance.coinbase.exchange import (
    CBPRO_API_URL,
    CoinbaseExchange,
    CoinbaseExchangeAuth,
    InvalidTimeframeError,
    make_exchange_context,
    timeframe_to_seconds,
)
from c2ba.finance.coinbase.rebalancer import (
    RebalanceConfig,
)
from c2ba.finance.coinbase.rebalancer import rebalance as run_rebalance
from c2ba.utils import (
    abort,
)
from c2ba.utils.logging import (
    LogLevel,
    init_logging,
)

app = typer.Typer(help="CLI to query coinbase exchange")
logger = logging.getLogger(__name__)


def get_exchange(ctx: typer.Context) -> CoinbaseExchange:
    if ctx.obj is None:
        raise abort("Exchange not found in context.")
    return ctx.obj


@app.callback()
def main(
    ctx: typer.Context,
    api_key: Optional[str] = typer.Option(  # noqa: B008
        None,
        envvar="CBPRO_API_KEY",
    ),
    api_secret: Optional[str] = typer.Option(  # noqa: B008
        None,
        envvar="CBPRO_API_SECRET",
    ),
    api_pass: Optional[str] = typer.Option(  # noqa: B008
        None,
        envvar="CBPRO_API_PASS",
    ),
    api_url: str = typer.Option(  # noqa: B008
        CBPRO_API_URL,
        envvar="CBPRO_API_URL",
    ),
) -> None:
    auth: Optional[CoinbaseExchangeAuth] = None
    if api_key is not None and api_secret is not None and api_pass is not None:
        auth = CoinbaseExchangeAuth(api_key, api_secret, api_pass)
    else:
        if api_key is not None or api_secret is not None or api_pass is not None:
            logger.warning(
                "Specify all params api_key, api_secret and api_pass, or none of them."
            )
    ctx.obj = CoinbaseExchange(auth, api_url)


@app.command()
def get(
    ctx: typer.Context,
    endpoint: str,
    params: Optional[List[str]] = typer.Argument(  # noqa: B008
        None,
        help="List of URL parameters. Should be a list key1=value1 key2=value2 ...",
    ),
    paginated: bool = typer.Option(False),  # noqa: B008
    save_history: bool = typer.Option(False),  # noqa: B008
) -> None:
    """
    Generic command to query GET endpoints on coinbasepro API.
    """

    def _do_request() -> Any:
        return get_exchange(ctx).client_request(
            "get",
            endpoint,
            None,
            {x.split("=")[0]: x.split("=")[1] for x in params}
            if params is not None
            else None,
            paginated,
        )

    if not save_history:
        typer.echo(
            json.dumps(
                _do_request(),
                indent=4,
            )
        )
    else:
        with get_exchange(ctx).record_history() as history:  # type: ignore
            response = _do_request()
        typer.echo(
            json.dumps(
                {
                    "response": response,
                    "requests_history": [h.__dict__ for h in history],
                },
                indent=4,
            )
        )


@app.command()
def candles(
    ctx: typer.Context,
    base_currency: str,
    quote_currency: str,
    timeframe: str = typer.Argument("1m"),  # noqa: B008
    start: Optional[str] = typer.Option(None),  # noqa: B008
    end: Optional[str] = typer.Option(None),  # noqa: B008
    save_history: bool = typer.Option(False),  # noqa: B008
) -> None:
    # interesting interface could also be:
    # candles week BASE QUOTE TF XX --year=YYYY
    #   where XX would be the index from 01 to 52 of a week, YYYY default to current year
    # candles month BASE QUOTE TF MM --year=YYYY
    # candles year BASE QUOTE TF YYYY
    # candles day BASE QUOTE TF DD --month=YYYY-MM
    # candles hour BASE QUOTE TF --day=YYYY-MM-DD
    # Also add --format for output format, or provide a utility json-to-csv for conversion

    exchange = get_exchange(ctx)

    try:
        seconds = timeframe_to_seconds(timeframe)
    except InvalidTimeframeError as e:
        raise abort(e)

    if seconds not in exchange.valid_candle_granularities:
        raise abort(
            f"Invalid {timeframe} timeframe for coinbase. "
            "Valid timeframes should be convertible to a number of seconds in "
            f"{exchange.valid_candle_granularities} but {timeframe} converts to "
            "{seconds} seconds."
        )

    if end is not None and start is None:
        raise abort(
            "Option '--start' must be specified when option '--end' is specified."
        )

    end = "now" if end is None else end

    start_datetime = moment.date(start).date if start is not None else None
    end_datetime = moment.date(end).date

    try:

        def _do_request() -> List:
            return get_exchange(ctx).candles(
                f"{base_currency}-{quote_currency}",
                start_datetime,
                end_datetime,
                timeframe_to_seconds(timeframe),
            )

        if not save_history:
            typer.echo(_do_request())
        else:
            with get_exchange(ctx).record_history() as history:  # type: ignore
                candles = _do_request()
            typer.echo(
                json.dumps(
                    {
                        "candles": candles,
                        "requests_history": [h.__dict__ for h in history],
                    },
                    indent=4,
                )
            )
    except Exception as e:
        raise abort(e)


@app.command()
def exchange_context_regression_bundle(
    ctx: typer.Context,
    currencies: Optional[List[str]] = typer.Argument(None),  # noqa: B008
) -> None:
    with get_exchange(ctx).record_history() as history:  # type: ignore
        exchange_context = make_exchange_context(
            get_exchange(ctx), currencies if currencies is not None else []
        )
    typer.echo(
        json.dumps(
            {
                "currencies": currencies,
                "exchange_context": exchange_context.to_json(),
                "requests_history": [h.__dict__ for h in history],
            },
            indent=4,
        ).strip()
    )


@app.command()
def rebalance(
    ctx: typer.Context,
    config: Optional[str] = typer.Option(  # noqa: B008
        None,
        help="If specified, use as configuration instead of std input.",
    ),
    dry_run: bool = typer.Option(  # noqa: B008
        True,
        help="If specified, do not execute orders on exchange.",
    ),
    out_file: Optional[Path] = typer.Option(  # noqa: B008
        None,
        file_okay=True,
        dir_okay=False,
        exists=False,
        help="If specified, write html output to this file. Otherwise, write on stdout.",
    ),
    out_test_bundle: Optional[Path] = typer.Option(  # noqa: B008
        None,
        file_okay=True,
        dir_okay=False,
        exists=False,
        help="If specified, write output json test bundle file for regression tests",
    ),
) -> None:
    init_logging(LogLevel.INFO)

    if get_exchange(ctx).auth is None:
        raise abort("Exchange authentication is required for rebalance")

    if config is None:
        logging.info("Reading configuration from standard input")
        config = sys.stdin.read().strip()

    try:
        config_dict = toml.loads(config)
        rebalance_config = RebalanceConfig(
            asset_allocs={
                asset: Decimal(value)
                for asset, value in config_dict["asset_allocs"].items()
            },
            threshold=Decimal(config_dict["threshold"]),
        )

        rebalance_result = run_rebalance(
            rebalance_config,
            get_exchange(ctx),
            dry_run,
        )
    except Exception as e:
        raise abort(e)

    if out_file is not None:
        out_file.parent.mkdir(parents=True, exist_ok=True)
        with open(out_file, "w", encoding="utf-8") as out_file_io:
            out_file_io.write(rebalance_result.get_html_report())
    else:
        print(rebalance_result.get_html_report())

    if out_test_bundle is not None:
        out_test_bundle.parent.mkdir(parents=True, exist_ok=True)

        with open(out_test_bundle, "w", encoding="utf-8") as out_file_io:
            out_file_io.write(
                json.dumps(
                    rebalance_result.to_json(),
                    indent=4,
                ).strip()
                + "\n"
            )


if __name__ == "__main__":  # pragma: no cover
    app()
