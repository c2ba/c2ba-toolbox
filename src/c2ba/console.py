"""Console script for c2ba-toolbox."""

import typer

import c2ba.finance.coinbase.__main__ as coinbase
from c2ba import (
    __version__,
)

app = typer.Typer()
app.add_typer(coinbase.app, name="coinbase")


@app.command()
def version() -> None:
    """
    Display version and exit.
    """
    typer.echo(f"c2ba's Toolbox version {__version__}")


if __name__ == "__main__":  # pragma: no cover
    app()
