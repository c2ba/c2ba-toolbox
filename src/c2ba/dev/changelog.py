from __future__ import (
    annotations,
)

import logging
import os
from enum import (
    Enum,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Tuple,
)

import tomlkit
import typer
from semantic_version import (
    Version,
)
from tomlkit.toml_document import (
    TOMLDocument,
)

from c2ba.utils import (
    abort,
    run_command,
)
from c2ba.utils.cli import (
    LogLevelOption,
    VersionOption,
    make_app,
)

__version__ = "0.1.0"
app = make_app(__version__)
logger = logging.getLogger(__name__)
state = {
    "git.branch.name": "",
    "git.user.name": "",
    "git.user.email": "",
    "remote.url": "",
}


@app.callback()
def main(
    ctx: typer.Context,
    log_level: str = LogLevelOption(),  # noqa: B008
    version: bool = VersionOption(__version__),  # noqa: B008
    project_path: Path = typer.Option(  # noqa: B008
        Path(".").resolve(),
        file_okay=False,
        exists=True,
        help="Path to project, default to current working directory.",
    ),
) -> None:
    """
    Developer changelog tool.
    """
    os.chdir(project_path)
    state["git.branch.name"] = safe_run_command("git symbolic-ref --short HEAD").strip()
    state["git.user.name"] = safe_run_command("git config --get user.name").strip()
    state["git.user.email"] = safe_run_command("git config --get user.email").strip()
    state["remote.url"] = get_remote_url()


def get_remote_url() -> str:
    poetry_repository_url = get_poetry_repository_url()
    if poetry_repository_url is not None:
        return poetry_repository_url
    return safe_run_command("git remote get-url origin").strip()


def get_poetry_repository_url() -> Optional[str]:
    pyproject_path = Path("pyproject.toml")
    if not pyproject_path.exists():
        return None

    with open(pyproject_path, "r") as pyproject_io:
        pyproject_file = tomlkit.parse(pyproject_io.read())
    try:
        return pyproject_file["tool"]["poetry"]["repository"]
    except Exception:
        return None


@app.command()
def gen(
    version: Optional[str] = typer.Option(  # noqa: B008
        None,
        help="Optional version string. If specified, only generate the part for that version.",
    ),
) -> None:
    """
    Generate full changelog, or version changelog, from individual changelog toml entries.
    Output to stdout.
    """
    changelogs_dir = Path("changelogs")
    try:
        if version is not None:
            typer.echo(get_release_markdown_description(1, version, changelogs_dir))
            return
        typer.echo(get_full_changelog(changelogs_dir))
    except Exception as e:
        raise abort(e)


def get_full_changelog(
    changelogs_dir: Path,
) -> str:
    output_str = "# Changelog\n\n"
    output_str += (
        "All notable changes to this project will be documented in this file\n\n"
    )
    output_str += (
        "The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), "
        "and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).\n\n"
    )
    versions_directory_entries = sorted(
        (
            entry
            for entry in changelogs_dir.iterdir()
            if entry.is_dir() and entry.name != "unreleased"
        ),
        key=lambda filepath: Version(filepath.name[1:]),
        reverse=True,  # most recent versions first
    )
    if (changelogs_dir / "unreleased").exists():
        versions_directory_entries = [
            changelogs_dir / "unreleased"
        ] + versions_directory_entries
    for entry in versions_directory_entries:
        output_str += get_release_markdown_description(2, entry.name, changelogs_dir)
        output_str += "\n\n"
    output = output_str.strip("\n")
    output += f"\n\n---\n*Generated with c2ba.changelog {__version__}*"
    return output


def get_release_markdown_description(
    level: int, version: str, changelogs_dir: Path
) -> str:
    if version != "unreleased":
        version, _semver = validate_version(version)
        title = f"{_semver}"
    else:
        title = "Unreleased"
    version_dir = changelogs_dir / version
    if not version_dir.exists() or not version_dir.is_dir():
        raise RuntimeError(f"Not a version directory: {version_dir}")
    output_str = ("#" * level) + f" {title}\n\n"
    changelogs: Dict[str, List[Changelog]] = {}
    changelog_filepaths = sorted(
        (
            filepath
            for filepath in version_dir.iterdir()
            if filepath.is_file() and filepath.suffix == ".toml"
        )
    )
    for changelog_file in changelog_filepaths:
        changelog = Changelog(changelog_file)
        changelogs.setdefault(changelog.changelog_type, []).append(changelog)
    for changelog_type in ChangelogType:
        if changelog_type.value in changelogs:
            output_str += ("#" * (level + 1)) + f" {changelog_type.value}\n\n"
            for changelog in changelogs[changelog_type.value]:
                output_str += f"- {get_changelog_markdown(changelog)}\n"
            output_str += "\n"
    output_str += "\n"
    return output_str.strip("\n")


def validate_version(version: str) -> Tuple[str, Optional[Version]]:
    if version == "unreleased":
        return version, None

    if not version.startswith("v"):
        version = f"v{version}"

    return version, Version(version[1:])


class ChangelogType(str, Enum):
    Added = "Added"
    Changed = "Changed"
    Deprecated = "Deprecated"
    Removed = "Removed"
    Fixed = "Fixed"
    Security = "Security"
    Documentation = "Documentation"
    Other = "Other"


class Changelog:
    toml_document: TOMLDocument
    tool_changelog_version: Version
    filepath: Path
    name: str
    version: str

    changelog_type: ChangelogType
    description: str
    tags: List[str]

    remote_url: Optional[str] = None
    remote_merge_requests: Optional[List[str]] = None
    remote_issues: Optional[List[str]] = None

    author_name: Optional[str] = None
    author_email: Optional[str] = None

    def __init__(self, filepath: Path) -> None:
        with open(filepath, "r") as filepath_io:
            self.toml_document = tomlkit.parse(filepath_io.read())
        self.tool_changelog_version = get_tool_changelog_version(self.toml_document)

        if upgrade_changelog(self.toml_document):
            logger.warning(
                f"The changelog {filepath} has been upgraded for loading. "
                "Consider running 'upgrade' command to upgrade all changelogs"
            )
        self.filepath = filepath
        self.name = filepath.stem
        self.version = filepath.parent.name

        self.changelog_type = self.toml_document["type"]
        self.description = self.toml_document["description"]
        self.tags = self.toml_document["tags"]
        if "remote" in self.toml_document:
            if "url" in self.toml_document["remote"]:
                self.remote_url = self.toml_document["remote"]["url"]
            if "merge-requests" in self.toml_document["remote"]:
                self.remote_merge_request_id = self.toml_document["remote"][
                    "merge-requests"
                ]
            if "issues" in self.toml_document["remote"]:
                self.remote_issues = self.toml_document["remote"]["issues"]
        if "author" in self.toml_document:
            if "name" in self.toml_document["author"]:
                self.author_name = self.toml_document["author"]["name"]
            if "email" in self.toml_document["author"]:
                self.author_email = self.toml_document["author"]["email"]


def get_tool_changelog_version(toml_document: TOMLDocument) -> Version:
    if (
        "tool" not in toml_document
        or "changelog" not in toml_document["tool"]
        or "version" not in toml_document["tool"]["changelog"]
    ):
        raise RuntimeError(
            "Invalid changelog {filepath}: no tool.changelog.version entry."
        )
    return Version(toml_document["tool"]["changelog"]["version"])


def get_changelog_markdown(changelog: Changelog) -> str:
    line = f"{changelog.description}."
    if len(changelog.tags) > 0:
        line += f" *{', '.join(changelog.tags)}.*"
    if changelog.author_name is not None:
        line += f" (by {changelog.author_name}"
        if changelog.author_email is not None:
            line += f" \\<{changelog.author_email}\\>)"
        else:
            line += ")"
    elif changelog.author_email is not None:
        line += f" (by {changelog.author_email})"
    return line


def state_getter(key: str) -> Callable[[], str]:
    def _get() -> str:
        return state[key]

    return _get


@app.command()
def add(  # noqa: C901 too complex
    changelog_type: ChangelogType = typer.Option(  # noqa: B008
        ...,
        "--type",
        prompt=True,
        help="Type of changelog entry",
    ),
    description: str = typer.Option(  # noqa: B008
        ...,
        prompt=True,
        help="Description of changelog entry",
    ),
    filename: str = typer.Option(  # noqa: B008
        state_getter("git.branch.name"),
        prompt=True,
        help="Filename without extension for the changelog entry, defaults to git branch name",
    ),
    tags: str = typer.Option(  # noqa: B008
        "",
        prompt=True,
        help="List of tags to assign (space separated list)",
    ),
    version: str = typer.Option(  # noqa: B008
        "unreleased",
        prompt=True,
        help="For which version of the sofwtare is the entry",
    ),
    remote_url: str = typer.Option(  # noqa: B008
        state_getter("remote.url"),
        prompt=True,
        help=(
            "URL of remote repository hosting the merge requests and issues IDs, default to poetry repository URL "
            "or output of `git remote get-url origin`"
        ),
    ),
    merge_requests: str = typer.Option(  # noqa: B008
        "",
        prompt=True,
        help="List of merge or pull request IDs integrating this contribution in the codebase (space separated list)",
    ),
    issues: str = typer.Option(  # noqa: B008
        "",
        prompt=True,
        help="List of issue IDs addressed by this contribution (space separated list)",
    ),
    author_name: str = typer.Option(  # noqa: B008
        state_getter("git.user.name"),
        prompt=True,
        help="Name of the author, defaults to git user name",
    ),
    author_email: str = typer.Option(  # noqa: B008
        state_getter("git.user.email"),
        prompt=True,
        help="Email address of the author, defaults to git user email",
    ),
) -> None:
    """
    Add a new changelog TOML entry to changelog directory.
    """

    try:
        version, _semver = validate_version(version)
    except Exception as e:
        raise abort(e)

    changelog = tomlkit.document()
    changelog.add("description", description)
    changelog.add("type", f"{changelog_type}")
    changelog.add("tags", tags.split())

    if remote_url != "" or merge_requests != "" or issues != "":
        remote = tomlkit.table()
        if remote_url != "":
            remote.add("url", remote_url)
        if merge_requests != "":
            remote.add("merge-requests", merge_requests.split())
        if issues != "":
            remote.add("issues", issues.split())
        changelog.add("remote", remote)

    if author_name != "" or author_email != "":
        author = tomlkit.table()
        if author_name != "":
            author.add("name", author_name)
        if author_email != "":
            author.add("email", author_email)
        changelog.add("author", author)

    tool_changelog = tomlkit.table()
    tool_changelog.add("version", __version__)

    tool = tomlkit.table()
    tool._is_super_table = True
    tool.add("changelog", tool_changelog)

    changelog.add(tomlkit.nl())
    changelog.add("tool", tool)

    file_path = Path("changelogs") / Path(version) / Path(f"{filename}.toml")
    if file_path.exists():
        raise abort(f"{file_path} already exists.")

    file_path.parent.mkdir(parents=True, exist_ok=True)

    with open(file_path, "w", encoding="utf-8", newline="\n") as f:
        f.write(tomlkit.dumps(changelog))


@app.command()
def upgrade() -> None:
    """
    Upgrade all changelogs to current version of this tool.
    A changelog written in compatible version is not upgraded.
    """
    for root, _directories, files in os.walk("changelogs"):
        for filename in files:
            filepath = Path(root) / Path(filename)
            if not filepath.suffix == ".toml":
                continue
            with open(filepath, "r") as filepath_io:
                changelog = tomlkit.parse(filepath_io.read())
            if upgrade_changelog(changelog):
                with open(filepath, "w", encoding="utf-8", newline="\n") as filepath_io:
                    filepath_io.write(tomlkit.dumps(changelog))
    return  # Apparently useless, but otherwise I get a "partial" coverage on the outer loop line...


migration_functions: Dict[str, Callable[[tomlkit.api._TOMLDocument], None]] = {
    # No migration for now
}


def upgrade_changelog(changelog: tomlkit.api._TOMLDocument) -> bool:
    """
    Upgrade changelog if necessary. Returns True if the changelog was updated.
    """
    changelog_semversion = get_tool_changelog_version(changelog)

    updated = False
    for version, function in migration_functions.items():
        semversion = Version(version)
        if changelog_semversion < semversion:
            function(changelog)
            changelog_semversion = semversion
            updated = True

    return updated


def safe_run_command(command_line: str, *args: Any, **kwargs: Any) -> str:
    try:
        return run_command(command_line, *args, **kwargs)
    except Exception:
        return ""


if __name__ == "__main__":  # pragma: nocover
    app()
