import logging
import os
import re
from pathlib import (
    Path,
)
from typing import (
    List,
)

import tomlkit
import typer
from semantic_version import (
    Version,
)
from tomlkit.toml_document import (
    TOMLDocument,
)

from c2ba.utils import (
    abort,
    run_command,
)
from c2ba.utils.cli import (
    make_app,
)

logger = logging.getLogger(__name__)

__version__ = "0.1.0"
app = make_app(__version__)
create_app = typer.Typer()
app.add_typer(create_app, name="create")


@create_app.callback()
def create_main(
    project_path: Path = typer.Option(  # noqa: B008
        Path(".").resolve(),
        file_okay=False,
        exists=True,
        help="Path to project, default to current working directory.",
    ),
) -> None:
    """
    Create version tags.
    """
    os.chdir(project_path)
    if not is_working_directory_clean():
        raise abort("Commit or discard/stash your changes before running this script.")
    return  # Apparently useless, but otherwise I get a "partial" coverage on the if line...


POETRY_VERSION_PATTERN = re.compile(
    r"""
    ^
    v?
    (?:
        (?:(?P<epoch>[0-9]+)!)?                           # epoch
        (?P<release>[0-9]+(?:\.[0-9]+)*)                  # release segment
        (?P<pre>                                          # pre-release
            [-_.]?
            (?P<pre_l>(a|b|c|rc|alpha|beta|pre|preview))
            [-_.]?
            (?P<pre_n>[0-9]+)?
        )?
        (?P<post>                                         # post release
            (?:-(?P<post_n1>[0-9]+))
            |
            (?:
                [-_.]?
                (?P<post_l>post|rev|r)
                [-_.]?
                (?P<post_n2>[0-9]+)?
            )
        )?
        (?P<dev>                                          # dev release
            [-_.]?
            (?P<dev_l>dev)
            [-_.]?
            (?P<dev_n>[0-9]+)?
        )?
    )
    (?:\+(?P<local>[a-z0-9]+(?:[-_.][a-z0-9]+)*))?       # local version
    $
""",
    re.IGNORECASE | re.VERBOSE,
)


@create_app.command("new")
def create_new(  # noqa: C901 'create_new' is too complex
    version_string: str,
) -> None:
    if version_string.startswith("v"):
        version_string = version_string[1:]

    poetry_match_version = POETRY_VERSION_PATTERN.match(version_string)
    if not poetry_match_version:
        raise abort(f"Invalid version for poetry project: {version_string}")

    version = Version(version_string)

    version_tag = f"v{version}"
    if tag_exists(version_tag):
        raise abort(
            f"Version tag {version_tag} found in history, define another one or remove it."
        )

    paths_to_commit = []

    pyproject_toml_file = Path("pyproject.toml")
    paths_to_commit.append(pyproject_toml_file)
    try:
        poetry_document = load_valid_poetry_pyproject(pyproject_toml_file)
    except Exception as e:
        raise abort(e)

    package_name = poetry_document["tool"]["poetry"]["name"]
    version_module_file = Path("src") / package_name / "version.py"
    paths_to_commit.append(version_module_file)
    if not version_module_file.exists():
        raise abort(f"{version_module_file} not found")

    write_version_module(version, version_module_file)

    poetry_document["tool"]["poetry"]["version"] = f"{version}"
    with open(pyproject_toml_file, "w") as pyproject_io:
        pyproject_io.write(tomlkit.dumps(poetry_document))

    unreleased_changelogs_path = Path("changelogs") / "unreleased"
    if unreleased_changelogs_path.exists():
        paths_to_commit.append(unreleased_changelogs_path)
        new_changelogs_path = Path("changelogs") / version_tag
        paths_to_commit.append(new_changelogs_path)
        unreleased_changelogs_path.rename(new_changelogs_path)

    commit_and_tag(paths_to_commit, version_tag)

    typer.echo(version_tag)


def load_valid_poetry_pyproject(pyproject_toml_file: Path) -> TOMLDocument:
    if not pyproject_toml_file.exists():
        raise RuntimeError("Only python project with pyproject.toml supported for now")

    with open(pyproject_toml_file, "r") as pyproject_io:
        doc = tomlkit.loads(pyproject_io.read())

    if "tool" not in doc or "poetry" not in doc["tool"]:
        raise RuntimeError("No table [tool.poetry] in pyproject.toml")

    if "name" not in doc["tool"]["poetry"]:
        raise RuntimeError("No package name in table [tool.poetry] of pyproject.toml")

    return doc


def write_version_module(version: Version, version_module_file: Path) -> None:
    with open(version_module_file, "r") as version_file_io:
        version_file_content = version_file_io.read()
    with open(version_module_file, "w") as version_file_io:
        version_file_io.write(
            re.sub(
                '__version__ = "[^"]*"',
                f'__version__ = "{version}"',
                version_file_content,
            )
        )


@create_app.command("major")
def create_major() -> None:
    raise NotImplementedError("'create major' not implemented yet.")  # pragma: no cover


@create_app.command("minor")
def create_minor() -> None:
    raise NotImplementedError("'create minor' not implemented yet.")  # pragma: no cover


@create_app.command("patch")
def create_patch() -> None:
    raise NotImplementedError("'create patch' not implemented yet.")  # pragma: no cover


def is_working_directory_clean() -> bool:
    return run_command("git status --untracked-files=no --porcelain") == ""


def tag_exists(tag_name: str) -> bool:
    return run_command(f"git tag -l {tag_name}") != ""


def commit_and_tag(paths_to_commit: List[Path], tag_name: str) -> None:
    logger.info("Creating commit")
    run_command(f"git add {' '.join([str(p) for p in paths_to_commit])}")
    run_command(f'git commit -m "Release {tag_name}"')
    logger.info(f"Creating tag {tag_name}")
    run_command(f"git tag {tag_name}")


if __name__ == "__main__":  # pragma: no cover
    app()
