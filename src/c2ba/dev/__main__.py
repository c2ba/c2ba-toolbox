import typer

from c2ba.dev import (
    __version__,
    changelog,
    release,
)

app = typer.Typer(
    name="c2ba.dev",
    invoke_without_command=True,
)
app.add_typer(changelog.app, name="changelog")
app.add_typer(release.app, name="release")


@app.callback()
def main(
    ctx: typer.Context,
    version: bool = typer.Option(False, help="Display version and exit."),  # noqa: B008
) -> None:
    """
    Developer toolbox commands.
    """
    if version:
        typer.echo(__version__)
        raise typer.Exit()

    if ctx.invoked_subcommand is None:
        typer.echo(ctx.get_help())

    return


if __name__ == "__main__":  # pragma: no cover
    app()
