import os
from contextlib import (
    contextmanager,
)
from copy import (
    deepcopy,
)
from typing import (
    Any,
    ContextManager,
    List,
    Mapping,
)

from typer.testing import (
    CliRunner,
)


def run_test_version(module: Any) -> None:
    cli_runner = CliRunner(mix_stderr=False)
    app_result = cli_runner.invoke(
        module.app,
        [
            "--version",
        ],
    )
    assert app_result.exit_code == 0  # noqa: S101
    assert app_result.stdout.strip() == module.__version__  # noqa: S101


def run_test_nosubcommand_displays_help(module: Any) -> None:
    cli_runner = CliRunner(mix_stderr=False)
    app_result_nosubcommand = cli_runner.invoke(
        module.app,
        [],
    )
    app_result_help = cli_runner.invoke(
        module.app,
        [
            "--help",
        ],
    )
    assert app_result_nosubcommand.exit_code == 0  # noqa: S101
    assert (  # noqa: S101
        app_result_nosubcommand.stdout.strip() == app_result_help.stdout.strip()
    )


def run_test_subcommands(module: Any, subcommands: List[str]) -> None:
    cli_runner = CliRunner(mix_stderr=False)
    for name in subcommands:
        app_result_parent = cli_runner.invoke(
            module.app,
            [name],
        )
        assert app_result_parent.exit_code == 0  # noqa: S101


@contextmanager  # type: ignore
def scoped_environ(  # type: ignore
    scoped_environ: Mapping[str, Any], inherit_current_env: bool
) -> ContextManager:
    # initialization
    previous_environ = deepcopy(os.environ)
    try:
        if not inherit_current_env:
            os.environ.clear()
        os.environ.update(scoped_environ)
        yield
    finally:
        os.environ.clear()
        os.environ.update(previous_environ)
