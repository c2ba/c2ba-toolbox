import logging
import os
import shlex
from contextlib import (
    contextmanager,
)
from decimal import (
    Decimal,
)
from pathlib import (
    Path,
)
from subprocess import (  # noqa: S404 Consider possible security implications associated with PIPE module.
    PIPE,
    run,
)
from typing import (
    Any,
    ContextManager,
    Dict,
    List,
    Sequence,
    Union,
)

import typer

logger = logging.getLogger(__name__)


# Ignoring type here because mypy do not agree with typeguard
@contextmanager  # type: ignore
def pushd(dir_path: Path) -> ContextManager:  # type: ignore
    cwd = os.getcwd()
    os.chdir(dir_path)
    try:
        yield
    finally:
        os.chdir(cwd)


def make_records(elements: Sequence[Any]) -> Sequence[Dict]:
    return [elmt.__dict__ for elmt in elements]


def make_decimal(value: Union[int, float, str]) -> Decimal:
    return Decimal(str(value))


def make_decimals(values: Sequence[Union[int, float, str]]) -> List[Decimal]:
    return [make_decimal(value) for value in values]


def divide_or_zero(numerator: Any, denominator: Any) -> Any:
    return (
        numerator / denominator
        if denominator != type(numerator)(0)
        else type(numerator)(0)
    )


def convert_decimals_to_str(d: Dict) -> None:
    for k in d:
        if isinstance(d[k], dict):
            convert_decimals_to_str(d[k])
        if isinstance(d[k], Decimal):
            d[k] = str(d[k])


def abort(cause: Union[str, Exception]) -> Exception:  # pragma: nocover
    """
    Abort with user friendly exception in production mode.
    Raise back exception or RuntimeError in develop env to ease debugging.
    """
    typer.echo(f"{cause}", err=True)
    if os.environ.get("C2BA_CATCH_EXCEPTIONS", "1") == "0":
        if isinstance(cause, Exception):
            return cause
        return RuntimeError(cause)
    if os.environ.get("C2BA_VERBOSE") == "1":
        logger.error(cause, exc_info=True, stack_info=True)
    return typer.Abort(cause)


def run_command(command_line: str, *args: Any, **kwargs: Any) -> str:
    command_line = command_line.replace(
        "\\", "/"
    )  # Replace windows path separators with unix as they mess with shlex.split
    # note: Using posix=False is also a solution for path separator, but then multiple word arguments are quote...
    return run(  # type: ignore # noqa: S603 subprocess call - check for execution of untrusted input.
        shlex.split(command_line),
        universal_newlines=True,
        check=True,
        stdout=PIPE,
        *args,
        **kwargs,
    ).stdout
