import logging
import os
from enum import (
    Enum,
)
from pathlib import (
    Path,
)
from typing import (
    Union,
)

import colorama
from colorama import (
    Fore,
)

MODULE_PATH = Path(__file__).parent.parent.parent.parent.parent


class LogLevel(str, Enum):
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"


COLORS = {
    LogLevel.DEBUG.value: Fore.CYAN,
    LogLevel.INFO.value: Fore.BLUE,
    LogLevel.WARNING.value: Fore.YELLOW,
    LogLevel.ERROR.value: Fore.RED,
}


def set_level(level: LogLevel, *loggers: Union[str, logging.Logger]) -> None:
    level_code = getattr(logging, level)
    for logger in loggers:
        if isinstance(logger, str):
            logger = logging.getLogger(logger)
        logger.setLevel(level_code)


class Formatter(logging.Formatter):
    def format(self, record: logging.LogRecord) -> str:  # noqa: A003
        """
        The role of this custom formatter is:
        - append filepath and lineno to logging format but shorten path to files, to make logs more clear
        - to append "./" at the begining to permit going to the line quickly with VS Code CTRL+click from terminal
        """
        try:
            relpathname = (
                f"{os.curdir}{os.sep}{Path(record.pathname).relative_to(MODULE_PATH)}"
            )
        except Exception:
            relpathname = record.pathname
        record.asctime = self.formatTime(record, self.datefmt)
        message = super().format(
            record
        )  # note: format() injects important fields to record + append exec and stack info
        return (
            f"{COLORS[record.levelname]}[{record.asctime}] {record.levelname:<7} "
            f"{record.name} {record.threadName} {record.processName} {relpathname}:{record.lineno}{Fore.RESET}\n"
            f"{message}"
        )


_INITIALIZED = False


def init_logging(level: LogLevel) -> None:
    global _INITIALIZED
    root_logger = logging.getLogger()
    if not _INITIALIZED:
        colorama.init()

        handler = logging.StreamHandler()
        handler.setFormatter(Formatter())

        root_logger.addHandler(handler)
        set_level(level, root_logger)

        root_logger.debug("Logging init with level %s", level.value)
        _INITIALIZED = True
    else:
        root_logger.debug("Logging already initialized")


def get_logger(name: str) -> logging.Logger:
    return {name: logging.getLogger(name), "__main__": logging.getLogger()}[name]
