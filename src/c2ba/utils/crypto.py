from __future__ import (
    annotations,
)

import logging
import os
import sys
from base64 import (
    urlsafe_b64encode,
)
from dataclasses import (
    dataclass,
)
from hashlib import (
    pbkdf2_hmac,
)
from typing import (
    Any,
    Optional,
)

import toml
import typer
from cryptography.fernet import (
    Fernet,
)

from c2ba import (
    __version__,
)
from c2ba.utils import (
    abort,
)
from c2ba.utils.cli import (
    LogLevelOption,
    VersionOption,
)

logger = logging.getLogger(__name__)

_salt_byte_length = 32
_salt_hex_length = _salt_byte_length * 2

# todo: we could use passlib with bcrypt algorithm to hash password
# see https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt/


def random_salt() -> str:
    return bytes.hex(os.urandom(_salt_byte_length))


def hash_password(password: str, salt: Optional[str] = None) -> str:
    """
    Utility function to hash a password with a salt.
    https://nitratine.net/blog/post/how-to-hash-passwords-in-python/
    """
    if salt is None:
        salt = random_salt()
    return salt + bytes.hex(
        pbkdf2_hmac(
            hash_name="sha512",
            password=password.encode("utf-8"),
            salt=bytes.fromhex(salt),
            iterations=100000,
        )
    )


def get_salt_from_hash(password_hash: str) -> str:
    return password_hash[:_salt_hex_length]


def check_password(password: str, password_hash: str) -> bool:
    return password_hash == hash_password(password, get_salt_from_hash(password_hash))


class Encrypter:
    # https://eqxtech.com/engineering/symmetric-encryption-in-python/
    _fernet: Fernet

    def __init__(
        self,
        passphrase: str,
        key_salt: str,
    ) -> None:
        # key stretching
        key = pbkdf2_hmac(
            hash_name="sha512",
            password=passphrase.encode("utf-8"),
            salt=bytes.fromhex(key_salt),
            iterations=100000,
            dklen=32,
        )
        self._fernet = Fernet(urlsafe_b64encode(key))

    def encrypt(self, message: str) -> str:
        return self._fernet.encrypt(message.encode()).decode()

    def decrypt(self, message: str) -> str:
        return self._fernet.decrypt(message.encode()).decode()


class EncryptionError(Exception):
    pass


@dataclass
class KeyPassphraseConfig:
    passphrase_hash: str
    key_salt: str

    def check_passphrase(self, passphrase_candidate: str) -> bool:
        if not check_password(passphrase_candidate, self.passphrase_hash):
            return False
        return True

    def to_json(self) -> Any:
        return self.__dict__


def make_passphrase_config(
    passphrase: str,
    key_salt: Optional[str] = None,
    passphrase_salt: Optional[str] = None,
) -> KeyPassphraseConfig:
    hash_value = hash_password(passphrase, passphrase_salt)
    if key_salt is None:
        key_salt = random_salt()

    return KeyPassphraseConfig(
        hash_value,
        key_salt,
    )


def get_ctx_encrypter(ctx: Any) -> Encrypter:
    if ctx.obj is None:
        raise abort(
            "A key passphrase and salt must be provided. "
            "Use --key-passphrase with --key-salt or --key-config to provide it."
        )
    return ctx.obj


app = typer.Typer(help="CLI to manipulate cryptographic functions")
app_key = typer.Typer()
app_password = typer.Typer()

app.add_typer(app_key, name="key")
app.add_typer(app_password, name="password")


@app_key.callback()
def main(
    ctx: typer.Context,
    key_passphrase: Optional[str] = typer.Option(  # noqa: B008
        None,
        envvar="C2BA_KEY_PASSPHRASE",
        help="Passphrase to encrypt/decrypt messages.",
    ),
    key_salt: Optional[str] = typer.Option(  # noqa: B008
        None,
        envvar="C2BA_KEY_SALT",
        help="Salt to encrypt/decrypt messages.",
    ),
    key_config: Optional[str] = typer.Option(  # noqa: B008
        None,
        envvar="C2BA_KEY_CONFIG",
        help=(
            "Can be provided instead of --key-salt. "
            "Should match the format of config subcommand. "
            "The validity of the passphrase will be checked."
        ),
    ),
    log_level: str = LogLevelOption(envvar_scope="KEY"),  # noqa: B008
    version: bool = VersionOption(__version__),  # noqa: B008
) -> None:
    if key_passphrase is not None:
        if key_config is not None:
            config = KeyPassphraseConfig(**toml.loads(key_config))
            if not config.check_passphrase(key_passphrase):
                raise abort("Provided passphrase does not match config.")
            ctx.obj = Encrypter(key_passphrase, config.key_salt)
        elif key_salt is not None:
            ctx.obj = Encrypter(key_passphrase, key_salt)
    return


@app_key.command(name="encrypt")
def key_encrypt(
    ctx: typer.Context,
    message: Optional[str] = typer.Argument(None),  # noqa: B008
) -> None:
    if message is None:
        message = sys.stdin.read()
    typer.echo(get_ctx_encrypter(ctx).encrypt(message))


@app_key.command(name="encrypt-hide")
def key_encrypt_hide(
    key_salt: str = typer.Argument(  # noqa: B008
        ...,
        envvar="C2BA_KEY_SALT",
    ),
    key_passphrase: str = typer.Option(  # noqa: B008
        ..., prompt=True, confirmation_prompt=True, hide_input=True
    ),
    message: str = typer.Option(  # noqa: B008
        ..., prompt=True, confirmation_prompt=True, hide_input=True
    ),
) -> None:
    logger.info(f"Encrypting with salt = {key_salt}")
    encrypter = Encrypter(key_passphrase, key_salt)
    typer.echo(encrypter.encrypt(message))


@app_key.command(name="decrypt")
def key_decrypt(
    ctx: typer.Context,
    message: Optional[str] = typer.Argument(None),  # noqa: B008
) -> None:
    if message is None:
        message = sys.stdin.read()
    typer.echo(get_ctx_encrypter(ctx).decrypt(message))


@app_key.command(name="decrypt-hide")
def key_decrypt_hide(
    message: str,
    key_salt: str = typer.Argument(  # noqa: B008
        ...,
        envvar="C2BA_KEY_SALT",
    ),
    key_passphrase: str = typer.Option(..., prompt=True, hide_input=True),  # noqa: B008
) -> None:
    logger.info(f"Decrypting with salt = {key_salt}")
    encrypter = Encrypter(key_passphrase, key_salt)
    typer.echo(encrypter.decrypt(message))


@app_key.command(name="config")
def key_config(
    passphrase: str = typer.Argument(  # noqa: B008
        ...,
        envvar="C2BA_KEY_PASSPHRASE",
    ),
) -> None:
    key = make_passphrase_config(passphrase)
    typer.echo(toml.dumps(key.to_json()))


@app_password.command("hash")
def password_hash(password: str) -> None:
    hash_value = hash_password(password)
    typer.echo(f"{hash_value}")


@app_password.command("check")
def password_check(password: str, password_hash: str) -> None:
    if check_password(password, password_hash):
        typer.echo("Success")
        raise typer.Exit(0)
    typer.echo("Failure")
    raise typer.Exit(1)
