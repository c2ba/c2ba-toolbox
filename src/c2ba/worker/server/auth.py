from __future__ import (
    annotations,
)

import logging
from datetime import (
    datetime,
    timedelta,
)
from typing import (
    Optional,
)

from fastapi import (
    Depends,
    HTTPException,
    status,
)
from fastapi.responses import (
    Response,
)
from fastapi.security.oauth2 import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
)
from jose import (
    JWTError,
    jwt,
)
from pydantic import (
    BaseModel,
)
from starlette.requests import (
    Request,
)

from c2ba.utils.crypto import (
    check_password,
)
from c2ba.worker.config import (
    Config,
    User,
)
from c2ba.worker.server.depends import (
    get_config,
)

logger = logging.getLogger(__name__)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token", auto_error=False)

cookie_name = "access-token"
cookie_expires = int(timedelta(days=7).total_seconds())
jwt_algorithm = "HS256"

InvalidCredentialsException = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


class NotAuthenticatedException(Exception):
    pass


class Token(BaseModel):
    access_token: str
    token_type: str


def authenticate_and_get_token(
    response: Response,
    config: Config = Depends(get_config),  # noqa: B008
    form_data: OAuth2PasswordRequestForm = Depends(),  # noqa: B008
) -> Token:
    user = authenticate_user(form_data.username, form_data.password, config)
    return Token(  # noqa: S106
        access_token=make_access_token(user.username, config, response),
        token_type="bearer",
    )


def authenticate_user(username: str, password: str, config: Config) -> User:
    user = config.load_user(username)
    if not user:
        raise InvalidCredentialsException
    else:
        if check_password(password, user.admin_password_hash):
            logger.info(f"Logged as admin for user {username}")
        elif check_password(password, user.guest_password_hash):
            logger.info(f"Logged as guest for user {username}")
        else:
            logger.warning("Authentication failed: invalid password")
            raise InvalidCredentialsException
    return user


def make_access_token(username: str, config: Config, response: Response) -> str:
    expires = timedelta(days=7)
    access_token = create_access_token(
        config.webapp_token_auth_secret_key(),
        username,
        datetime.utcnow() + expires,
    )
    refresh_access_token_cookie(access_token, response)
    return access_token


def refresh_access_token_cookie(
    access_token: str,
    response: Response,
) -> None:
    response.delete_cookie(cookie_name)
    response.set_cookie(
        key=cookie_name,
        value=access_token,
        httponly=True,
        expires=cookie_expires,
    )


def create_access_token(secret: str, username: str, expires: datetime) -> str:
    return jwt.encode(
        {"sub": username, "exp": expires},
        secret,
        jwt_algorithm,
    )


def get_user_from_token(token: str, config: Config) -> User:
    try:
        payload = jwt.decode(
            token, config.webapp_token_auth_secret_key(), algorithms=[jwt_algorithm]
        )
        # the identifier should be stored under the sub (subject) key
        username = payload.get("sub")
        if username is None:
            raise InvalidCredentialsException
    # This includes all errors raised by pyjwt
    except JWTError:
        raise InvalidCredentialsException

    user = config.load_user(username)

    if user is None:
        raise InvalidCredentialsException

    return user


def get_token_from_cookie(
    request: Request,
    oauth_token: str = Depends(oauth2_scheme),  # noqa: B008
) -> Optional[str]:
    return request.cookies.get(cookie_name) or oauth_token


def get_current_user(
    response: Response,
    token: str = Depends(get_token_from_cookie),  # noqa: B008
    config: Config = Depends(get_config),  # noqa: B008
) -> User:
    if not token:
        raise InvalidCredentialsException
    user = get_user_from_token(token, config)
    refresh_access_token_cookie(token, response)

    return user
