from c2ba.worker.config import (
    Config,
    load_config_from_env,
)


def get_config() -> Config:
    # By default load config from environment, but can be overriden using FastAPI
    # dependency overrides
    return load_config_from_env()
