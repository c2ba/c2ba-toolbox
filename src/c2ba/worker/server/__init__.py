from __future__ import (
    annotations,
)

import logging

from fastapi import (
    Depends,
    FastAPI,
    status,
)
from fastapi.requests import (
    Request,
)
from fastapi.responses import (
    HTMLResponse,
    RedirectResponse,
)
from starlette.responses import (
    Response,
)

from c2ba.worker.config import (
    Config,
    User,
)
from c2ba.worker.data_access_layer import (
    get_rebalancer_report,
)
from c2ba.worker.server.auth import (
    NotAuthenticatedException,
    Token,
    authenticate_and_get_token,
    get_current_user,
    get_token_from_cookie,
    refresh_access_token_cookie,
)
from c2ba.worker.server.depends import (
    get_config,
)

# todo: follow evolution of issue https://github.com/tiangolo/fastapi/issues/2881
#  pytest fail to discover tests because of type annotation in routes and Depends functions
#  so this code cannot be tested right now with pytest and typeguard

# todo: better auth management with https://kernelpanic.io/demystifying-authentication-with-fastapi-and-a-frontend
#  see exemple code https://github.com/fuegoio/fastapi-frontend-auth-example
#  contains frontend in Vue.js and backend with fastapi + sqlalchemy

logger = logging.getLogger(__name__)

app = FastAPI(redoc_url=None)


@app.post("/token", response_model=Token)
def get_token(
    token: Token = Depends(authenticate_and_get_token),  # noqa: B008
) -> Token:
    return token


@app.get("/login")
async def get_login() -> HTMLResponse:
    form = (
        '<form class="container" action="/login" method="post">'
        '<label for="username"><b>Username</b></label>'
        '<input type="text" placeholder="Enter Username" name="username" required>'
        '<label for="password"><b>Password</b></label>'
        '<input type="password" placeholder="Enter Password" name="password" required>'
        '<button type="submit">Login</button>'
        "</form >"
    )
    return HTMLResponse(form)


@app.post("/login")
async def post_login(
    token: Token = Depends(authenticate_and_get_token),  # noqa: B008
) -> RedirectResponse:
    redirect_response = RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)
    refresh_access_token_cookie(token.access_token, redirect_response)
    return redirect_response


@app.exception_handler(NotAuthenticatedException)
def not_authenticated_exception_handler(
    request: Request, exc: NotAuthenticatedException
) -> RedirectResponse:
    return RedirectResponse(url="/login")


def get_current_user_or_redirect_to_login(
    response: Response,
    token: str = Depends(get_token_from_cookie),  # noqa: B008
    config: Config = Depends(get_config),  # noqa: B008
) -> User:
    if not token:
        raise NotAuthenticatedException()
    return get_current_user(response, token, config)


@app.get("/", response_class=HTMLResponse)
async def app_root(
    user: User = Depends(get_current_user_or_redirect_to_login),  # noqa: B008
) -> str:
    return get_rebalancer_report()


@app.post("/marketing/emails/")
async def post_email(
    email: str,
    user: User = Depends(get_current_user),  # noqa: B008
) -> str:
    # todo:
    #  - we need this endpoint to be called from front-end, so replace authentication by:
    #  - send confirmation email to 'email' with tokenized link => reply with html page
    #       - needed: a dedicated gmail account + api keys for sending that email
    #  - add an endpoint for confirmation => check token
    #  - if token is good, add email to file / database
    # Tips to protect against mail spamming: https://elasticemail.com/blog/marketing_tips/how-to-prevent-bots-from-spamming-your-sign-up-forms
    return email


@app.delete("/marketing/emails/", response_model=User)
async def delete_email(
    user: User = Depends(get_current_user),  # noqa: B008
) -> User:
    return user


@app.get("/marketing/emails/", response_model=User)
async def get_emails(
    user: User = Depends(get_current_user),  # noqa: B008
) -> User:
    return user
