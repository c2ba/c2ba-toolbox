from threading import (
    Lock,
)

current_report: str = "<h1>No report yet</h1>"
current_report_lock: Lock = Lock()


def get_rebalancer_report() -> str:
    with current_report_lock:
        return current_report


def set_rebalancer_report(report: str) -> None:
    global current_report
    with current_report_lock:
        current_report = report
