import json
import logging
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from pathlib import (
    Path,
)
from time import (
    sleep,
)
from typing import (
    Optional,
)

import moment

from c2ba.finance.coinbase.exchange import (
    CoinbaseExchange,
    CoinbaseExchangeAuth,
    timeframe_to_seconds,
)
from c2ba.finance.coinbase.rebalancer import (
    RebalanceConfig,
    rebalance,
)
from c2ba.finance.coinbase.rebalancer.take_profit import (
    compute_take_profit_projection_dataframe,
    make_take_profit_state_from_candles,
)
from c2ba.worker.config import (
    Config,
    compute_crypto_assets_allocations,
    compute_portfolio_allocations,
)
from c2ba.worker.data_access_layer import (
    set_rebalancer_report,
)

logger = logging.getLogger(__name__)


def run(
    config: Config,
    dry_run: bool,
    output_dir: Optional[Path],
) -> None:
    if dry_run:
        logging.warning("Running in dry mode: no order will be placed")
        execution_mode = "dry run"
    else:
        logging.warning("Running in live mode: orders will be placed on exchange")
        execution_mode = "live"

    start_date = datetime.now()

    try:
        portfolios_config = config.get_portfolios()

        if len(portfolios_config) != 1:
            logger.warning(
                "Only one portfolio supported at this time, processing the first one"
            )

        portfolio_config = list(portfolios_config.values())[0]
    except (ValueError, TypeError) as e:
        logger.exception(f"Value error rebalancer initialization: {e}")
        set_rebalancer_report(
            f"Error: {e}<br/>Please check your configuration and redeploy."
        )
        return
    except Exception as e:
        logger.exception(f"Uncaught exception at rebalancer initialization: {e}")
        set_rebalancer_report(
            f"Uncaught exception at rebalancer initialization: {e}<br/>Please contact a developer."
        )
        return

    try:
        exchange = CoinbaseExchange(
            CoinbaseExchangeAuth(
                portfolio_config.coinbasepro.api_key,
                portfolio_config.coinbasepro.api_secret,
                portfolio_config.coinbasepro.api_pass,
            )
        )

        start_date = moment.date(
            portfolio_config.rebalance.watch_product_ath_origin
        ).date
        end_date = moment.utcnow().date
        granularity = timeframe_to_seconds(
            portfolio_config.rebalance.watch_product_ath_timeframe
        )
        candles = exchange.candles(
            portfolio_config.rebalance.watch_product,
            start_date,
            end_date,
            granularity=granularity,
        )
        take_profit_state = make_take_profit_state_from_candles(
            portfolio_config.rebalance.crypto_alloc, candles
        )

        crypto_assets_allocs = compute_crypto_assets_allocations(
            config.get_asset_collections()[
                portfolio_config.rebalance.crypto_assets_collection
            ],
            portfolio_config.rebalance.crypto_assets_default_weight,
            portfolio_config.rebalance.crypto_assets_custom_weights,
        )
    except Exception as e:
        logger.exception(f"Uncaught exception at rebalancer initialization: {e}")
        set_rebalancer_report(
            f"Uncaught exception at rebalancer initialization: {e}<br/>Please contact a developer."
        )
        return

    rebalance_count = Decimal(0)
    total_fees_usdc = Decimal(0)

    report_history = ""
    rebalance_index = 0

    while True:
        try:
            logger.info(
                f"Getting stats for watch product {portfolio_config.rebalance.watch_product}"
            )
            stats = exchange.stats(portfolio_config.rebalance.watch_product)
            take_profit_state = take_profit_state.update(stats.high)

            asset_allocs = compute_portfolio_allocations(
                crypto_assets_allocs, take_profit_state.crypto_alloc()
            )

            rebalance_config = RebalanceConfig(
                asset_allocs,
                portfolio_config.rebalance.threshold,
            )

            logger.info(
                f"Try rebalancing {portfolio_config.pf_id} (owner: {portfolio_config.user})"
            )
            with exchange.record_history() as exchange_history:  # type: ignore
                rebalance_result = rebalance(rebalance_config, exchange, dry_run)

            take_profit_projection_df = compute_take_profit_projection_dataframe(
                portfolio_config.rebalance.crypto_alloc,
                take_profit_state.ath,
                stats.last,
                take_profit_state.crypto_alloc(),
                rebalance_result.total_usdc_value,
            )

            def _save_report(file_name: str) -> None:
                if output_dir is None:
                    return
                (output_dir / file_name).write_text(
                    rebalance_result.get_html_report(),
                    encoding="utf-8",
                )

            def _save_test_bundle(file_name: str) -> None:
                if output_dir is None:
                    return
                (output_dir / file_name).write_text(
                    json.dumps(
                        {
                            "rebalance_result": rebalance_result.to_json(),
                            "requests_history": [h.__dict__ for h in exchange_history],
                        },
                        indent=4,
                    ).strip()
                    + "\n",
                    encoding="utf-8",
                )

            if rebalance_result.should_rebalance:
                rebalance_count += 1
                total_fees_usdc += rebalance_result.total_usdc_fees

                logger.info(f"Rebalance {rebalance_count} total fees {total_fees_usdc}")

                report_history = (
                    f"{rebalance_result.get_html_report()}\n{report_history}"
                )

            if output_dir is not None:
                logger.info("No Rebalance")

                _save_report("rebalance_report.html")

                suffix = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                prefix = "" if rebalance_result.should_rebalance else "hourly_"

                if rebalance_result.should_rebalance or rebalance_index % 3600 == 0:
                    _save_report(f"{prefix}rebalance_report_{suffix}.html")
                    _save_test_bundle(f"{prefix}rebalance_test_bundle_{suffix}.json")

            current_stable = rebalance_result.stable_usdc_value()
            current_crypto = rebalance_result.crypto_usdc_value()
            alloc_stable = 100 * current_stable / rebalance_result.total_usdc_value
            alloc_crypto = 100 * current_crypto / rebalance_result.total_usdc_value

            front_matter = (
                "<h1>Rebalancer</h1>"
                + "<ul>"
                + f"  <li>start date: {start_date}</li>"
                + f"  <li>execution mode: {execution_mode}"
                + f"  <li>rebalance count: {rebalance_count}</li>"
                + f"  <li>total fees: {total_fees_usdc} $</li>"
                + f"  <li>total value: {rebalance_result.total_usdc_value} $</li>"
                + f"  <li>total stable: {round(current_stable, 2)} $ ({round(alloc_stable, 2)} %)</li>"
                + f"  <li>total crypto: {round(current_crypto, 2)} $ ({round(alloc_crypto, 2)} %)</li>"
                + "</ul>"
                + "<ul>"
                + f"  <li>take profit state: {take_profit_state.state}</li>"
                + f"  <li>current crypto alloc: {round(take_profit_state.crypto_alloc(), 2)}</li>"
                + f"  <li>ath price: {take_profit_state.ath} $</li>"
                + f"  <li>last price: {stats.last} $</li>"
                + f"  <li>drawdown from ath: {round(100 * (stats.last - take_profit_state.ath) / take_profit_state.ath, 2)} %</li>"
                + f"  <li>start date to compute initial ath: {start_date}</li>"
                + f"  <li>end date to compute initial ath: {end_date}</li>"
                + f"  <li>granularity to compute initial ath: {granularity}</li>"
                + "</ul>"
                + "<h2>Take Profit Strategy</h2>"
                + take_profit_projection_df.to_html(header=False)
            )

            rebalance_index += 1

            set_rebalancer_report(
                f"{front_matter}\n{rebalance_result.get_html_report()}\n<hr/>\n{report_history}"
            )
        except Exception as e:
            logger.exception(f"Uncaught exception: {e}")
        finally:
            sleep(1)
