from __future__ import (
    annotations,
)

import decimal
import logging
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
    timedelta,
)
from decimal import (
    Decimal,
)
from os import (
    environ,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
    Dict,
    List,
    Mapping,
    MutableMapping,
    Optional,
)

import click
import toml
import typer
from pydantic import (
    BaseModel,
)

from c2ba.finance.coinbase.rebalancer.take_profit import (
    TakeProfitConfig,
)
from c2ba.utils import (
    abort,
)
from c2ba.utils.cli import (
    LogLevelOption,
)
from c2ba.utils.crypto import (
    Encrypter,
    KeyPassphraseConfig,
    hash_password,
    make_passphrase_config,
    random_salt,
)
from c2ba.utils.logging import (
    LogLevel,
    init_logging,
)

logger = logging.getLogger(__name__)
DEFAULT_CONFIG_DIR_PATH = Path("/app/config")
ENV_VAR_KEY_PASSPHRASE = "C2BA_WORKER_KEY_PASSPHRASE"  # noqa: S105
ENV_VAR_CONFIG_DIR_PATH = "C2BA_WORKER_CONFIG_DIRPATH"
ENV_VAR_REBALANCE_OUTPUT_DIR = "C2BA_REBALANCE_OUTPUT_DIR"
ENV_VAR_REBALANCE_DRY_RUN = "C2BA_REBALANCE_DRY_RUN"
ENV_VAR_MARKETING_EMAILS_OUTPUT_FILE = "C2BA_MARKETING_EMAILS_OUTPUT_FILE"
ENV_VAR_WORKER_SERVER_PORT = "C2BA_WORKER_SERVER_PORT"


@dataclass
class AssetCollection:
    assets: List[str]
    sub_collections: Dict[str, AssetCollection]

    @classmethod
    def from_config_dict(cls, config: Mapping[str, Any]) -> AssetCollection:
        collection = AssetCollection([], {})
        for key, value in config.items():
            if key != "assets":
                if type(value) != dict:
                    raise ConfigError(f"{key} entry must be a dict")
                collection.sub_collections[key] = AssetCollection.from_config_dict(
                    value
                )
            else:
                if type(value) != list:
                    raise ConfigError(f"{key} entry must be a list")
                collection.assets = value
        return collection


@dataclass
class PortfolioRebalanceConfig:
    watch_product: str
    watch_product_ath_timeframe: str
    watch_product_ath_origin: str
    crypto_alloc: TakeProfitConfig

    crypto_assets_collection: str
    crypto_assets_default_weight: Decimal
    crypto_assets_custom_weights: Dict[str, Decimal]

    threshold: Decimal

    @classmethod
    def from_config_dict(cls, config: Mapping[str, Any]) -> PortfolioRebalanceConfig:
        crypto_assets = get_config_entry(config, "crypto_assets")
        crypto_alloc = get_config_entry(config, "crypto_alloc")
        crypto_alloc_watch = get_config_entry(crypto_alloc, "watch")
        return PortfolioRebalanceConfig(
            watch_product=get_config_entry(crypto_alloc_watch, "product"),
            watch_product_ath_timeframe=get_config_entry(
                crypto_alloc_watch, "ath_timeframe"
            ),
            watch_product_ath_origin=get_config_entry(crypto_alloc_watch, "ath_origin"),
            crypto_alloc=TakeProfitConfig(
                crypto_allocs=config_entry_to_decimal_list(
                    crypto_alloc["take_profit_strategy"], "crypto_allocs"
                ),
                prices=config_entry_to_decimal_list(
                    crypto_alloc["take_profit_strategy"], "prices"
                ),
            ),
            crypto_assets_collection=get_config_entry(crypto_assets, "collection"),
            crypto_assets_default_weight=config_entry_to_decimal(
                crypto_assets, "default_weight"
            ),
            crypto_assets_custom_weights=config_entry_to_decimal_dict(
                crypto_assets, "custom_weights"
            ),
            threshold=Decimal(config_entry_to_decimal(config, "threshold")),
        )


class PortfolioConfig:
    pf_id: str
    user: str
    coinbasepro: CoinbaseproConfig
    rebalance: PortfolioRebalanceConfig

    def __init__(self, pf_id: str, dict_cfg: Mapping, config: Config) -> None:
        self.pf_id = pf_id
        self.user = dict_cfg["user"]
        self.coinbasepro = CoinbaseproConfig(dict_cfg["coinbasepro"], config)
        self.rebalance = PortfolioRebalanceConfig.from_config_dict(
            dict_cfg["rebalance"]
        )


class CoinbaseproConfig:
    api_key: str
    api_secret: str
    api_pass: str

    def __init__(self, dict_cfg: Mapping, config: Config) -> None:
        for attr in (
            "api_key",
            "api_secret",
            "api_pass",
        ):
            if f"CBPRO_{attr.upper()}" in environ:
                setattr(self, attr, environ[f"CBPRO_{attr.upper()}"])
            else:
                try:
                    setattr(self, attr, config.decrypt(dict_cfg[attr]))
                except Exception as e:
                    logger.error(
                        f"Unable to decrypt coinbasepro entry {dict_cfg[attr]}.\n"
                        "Either encrypt a value with master passphrase, or specify the unencrypted value "
                        f"in env var CBPRO_{attr.upper()}."
                    )
                    raise e


class ConfigError(Exception):
    pass


class User(BaseModel):
    username: str
    admin_password_hash: str
    guest_password_hash: str


class Config:
    _config_dir_path: Path
    _config: MutableMapping[str, Any]
    _encrypter: Encrypter

    def __init__(
        self,
        config_directory_path: Path,
        key_passphrase: str,
    ) -> None:
        self._config_dir_path = config_directory_path
        self._config = toml.load(f"{self._config_dir_path}/config.toml")

        key_passphrase_config = KeyPassphraseConfig(**self._config["encrypt"])
        if not key_passphrase_config.check_passphrase(key_passphrase):
            raise ConfigError("Provided passphrase does not match config.")
        self._encrypter = Encrypter(key_passphrase, key_passphrase_config.key_salt)

    def encrypt(self, message: str) -> str:
        return self._encrypter.encrypt(message)

    def decrypt(self, message: str) -> str:
        return self._encrypter.decrypt(message)

    def host(self) -> str:
        return self.decrypt(self._config["host"])

    def ssl_key_file(self) -> str:
        return f"{self._config_dir_path}/ssl/c2ba-worker-key.pem"

    def ssl_cert_file(self) -> str:
        return f"{self._config_dir_path}/ssl/c2ba-worker-cert.pem"

    def webapp_token_auth_secret_key(self) -> str:
        return self.decrypt(self._config["webapp_token_auth_secret_key"])

    def get_users(self) -> Mapping[str, User]:
        return {
            key: User(**{"username": key, **value})
            for key, value in self._config["users"].items()
        }

    def log_config(self) -> None:
        logger.info(f"Config:\n\tconfig path = {self._config_dir_path}")

    def get_asset_collections(self) -> Dict[str, AssetCollection]:
        return {
            name: AssetCollection.from_config_dict(coll)
            for name, coll in self._config["asset_collections"].items()
        }

    def get_portfolios(self) -> Dict[str, PortfolioConfig]:
        return {
            pf_id: PortfolioConfig(pf_id, portfolio, self)
            for pf_id, portfolio in self._config["portfolios"].items()
        }

    def get(self, key: str, decrypt: bool) -> Any:
        current = self._config
        if key != "":
            tokens = key.split(".")
            for token in tokens:
                current = current[token]
        if decrypt and isinstance(current, str):
            return self.decrypt(current)
        return current

    def load_user(self, username: str) -> Optional[User]:
        return self.get_users().get(username)


def make_sample_config_dict(
    key_passphrase: str,
    key_salt: str,
    passphrase_salt: str,
    username: str,
    admin_password: str,
    admin_password_salt: str,
    guest_password: str,
    guest_password_salt: str,
    portfolio_name: str,
    portfolio_cbpro_api_key: str,
    portfolio_cbpro_api_secret: str,
    portfolio_cbpro_api_pass: str,
    hostname: str,
    webapp_token_auth_secret_key: str,
) -> Dict:
    key_config = make_passphrase_config(key_passphrase, key_salt, passphrase_salt)
    encrypter = Encrypter(key_passphrase, key_config.key_salt)

    return {
        "host": encrypter.encrypt(hostname),
        "webapp_token_auth_secret_key": encrypter.encrypt(webapp_token_auth_secret_key),
        "encrypt": key_config.to_json(),
        "users": {
            username: {
                "admin_password_hash": hash_password(
                    admin_password, admin_password_salt
                ),
                "guest_password_hash": hash_password(
                    guest_password, guest_password_salt
                ),
            }
        },
        "asset_collections": {
            "sample_collection": {
                "majors": {
                    "assets": [
                        "BTC",
                        "ETH",
                    ],
                },
                "defi": {
                    "assets": [
                        "SNX",
                        "UNI",
                        "AAVE",
                    ]
                },
                "others": {
                    "assets": [
                        "LTC",
                        "XLM",
                    ]
                },
            }
        },
        "portfolios": {
            portfolio_name: {
                "user": username,
                "coinbasepro": {
                    "api_key": encrypter.encrypt(portfolio_cbpro_api_key),
                    "api_secret": encrypter.encrypt(portfolio_cbpro_api_secret),
                    "api_pass": encrypter.encrypt(portfolio_cbpro_api_pass),
                },
                "rebalance": {
                    "threshold": 8.5,
                    "crypto_assets": {
                        "collection": "sample_collection",
                        "default_weight": 1,
                        "custom_weights": {
                            "major": 5,
                            "major.BTC": 60,
                            "major.ETH": 40,
                            "defi": 3,
                            "defi.AAVE": 2,
                            "others": 2,
                        },
                    },
                    "crypto_alloc": {
                        "watch": {
                            "product": "BTC-USDC",
                            "ath_timeframe": "1d",
                            "ath_origin": (
                                datetime.now() - timedelta(days=200)
                            ).strftime("%Y-%m-%d"),
                        },
                        "take_profit_strategy": {
                            "crypto_allocs": [80.0, 50.0, 38.2, 10.0],
                            "prices": [61800, 83800, 120000, 270000],
                        },
                    },
                },
            }
        },
        "tool": {
            "config": {
                "version": "1.0.0",
            }
        },
    }


def load_config_from_env() -> Config:
    if ENV_VAR_KEY_PASSPHRASE not in environ:
        raise ConfigError(
            f"{ENV_VAR_KEY_PASSPHRASE} not provided as environment variable."
        )

    return Config(
        Path(environ.get(ENV_VAR_CONFIG_DIR_PATH, DEFAULT_CONFIG_DIR_PATH)),
        environ[ENV_VAR_KEY_PASSPHRASE],
    )


def get_config_entry(
    config: Mapping[str, Any],
    entry: str,
) -> Any:
    try:
        return config[entry]
    except KeyError:
        raise ConfigParsingError(f"Configuration parsing error: missing key {entry}")


def config_entry_to_decimal(
    config: Mapping[str, Any],
    entry: str,
) -> Decimal:
    try:
        return Decimal(str(config[entry]))
    except KeyError:
        raise ConfigParsingError(f"Configuration parsing error: missing key {entry}")
    except decimal.InvalidOperation:
        raise ConfigParsingError(
            f"Configuration parsing error: unable to convert {entry} to decimal number"
        )


def config_entry_to_decimal_list(
    config: Mapping[str, Any],
    entry: str,
) -> List[Decimal]:
    try:
        return [Decimal(str(x)) for x in config[entry]]
    except KeyError:
        raise ConfigParsingError(f"Configuration parsing error: missing key {entry}")
    except decimal.InvalidOperation:
        raise ConfigParsingError(
            f"Configuration parsing error: unable to convert {entry} to decimal number"
        )
    except TypeError:
        raise ConfigParsingError(
            f"Configuration parsing error: type error on key {entry}"
        )


def config_entry_to_decimal_dict(
    config: Mapping[str, Any],
    entry: str,
) -> Dict[str, Decimal]:
    try:
        return {key: Decimal(str(value)) for key, value in config[entry].items()}
    except KeyError:
        raise ConfigParsingError(f"Configuration parsing error: missing key {entry}")
    except decimal.InvalidOperation:
        raise ConfigParsingError(
            f"Configuration parsing error: unable to convert {entry} to decimal number"
        )
    except TypeError:
        raise ConfigParsingError(
            f"Configuration parsing error: type error on key {entry}"
        )


class ConfigParsingError(Exception):
    pass


def worker_app_callback(
    ctx: click.core.Context,
    key_passphrase: str = typer.Option(  # noqa: B008
        ...,
        envvar=ENV_VAR_KEY_PASSPHRASE,
        help="Passphrase to encrypt/decrypt secrets from configuration.",
    ),
    config_dir_path: Path = typer.Option(  # noqa: B008
        DEFAULT_CONFIG_DIR_PATH,
        envvar=ENV_VAR_CONFIG_DIR_PATH,
        file_okay=False,
        dir_okay=True,
    ),
    log_level: str = LogLevelOption(envvar_scope="WORKER"),  # noqa: B008,
) -> None:
    init_logging(log_level)

    try:
        ctx.obj = Config(config_dir_path, key_passphrase)
    except Exception as e:
        raise abort(f"Exception at config loading: {e}")


app = typer.Typer(
    help="CLI to handle worker configuration",
    callback=worker_app_callback,
)


@app.command()
def create() -> None:
    raise NotImplementedError("Command not implemented yet")  # pragma: no cover


@app.command(name="set")
def set_cmd(
    key: str = typer.Argument(  # noqa: B008
        default="",
    )
) -> None:
    raise NotImplementedError("Command not implemented yet")  # pragma: no cover


@app.command()
def encrypt(
    ctx: typer.Context,
    message: str,
) -> None:
    config = ctx.obj
    typer.echo(config.encrypt(message))


@app.command()
def get(
    ctx: typer.Context,
    key: str = typer.Argument(  # noqa: B008
        default="",
    ),
    decrypt: bool = False,
) -> None:
    config = ctx.obj

    value = config.get(key, decrypt)
    if isinstance(value, dict):
        typer.echo(toml.dumps(value))
    elif isinstance(value, list):
        typer.echo(toml.dumps({key.split(".")[-1]: value}))
    else:
        typer.echo(value)


@app.command()
def get_rebalance_config(
    ctx: typer.Context,
    portfolio_name: str,
    crypto_alloc: float,
) -> None:
    config: Config = ctx.obj

    portfolios_config = config.get_portfolios()

    portfolio_config = portfolios_config[portfolio_name]

    crypto_assets_allocs = compute_crypto_assets_allocations(
        config.get_asset_collections()[
            portfolio_config.rebalance.crypto_assets_collection
        ],
        portfolio_config.rebalance.crypto_assets_default_weight,
        portfolio_config.rebalance.crypto_assets_custom_weights,
    )

    asset_allocs = compute_portfolio_allocations(
        crypto_assets_allocs,
        Decimal(str(crypto_alloc)),
    )

    rebalance_config = {
        "asset_allocs": {asset: str(value) for asset, value in asset_allocs.items()},
        "threshold": str(portfolio_config.rebalance.threshold),
    }
    typer.echo(toml.dumps(rebalance_config))


app_make_sample_config = typer.Typer(
    help="CLI to create sample worker configuration",
)


@app_make_sample_config.command()
def run(
    output: Path = typer.Argument(  # noqa: B008
        "config.toml",
        dir_okay=False,
        help="Path to config file",
    ),
    username: str = typer.Option(  # noqa: B008
        ...,
        prompt=True,
        help="Username to connect to the web UI",
    ),
    admin_password: str = typer.Option(  # noqa: B008
        ...,
        prompt=True,
        help="Admin password to connect to the web UI",
    ),
    guest_password: str = typer.Option(  # noqa: B008
        ...,
        prompt=True,
        help="Guest password to connect to the web UI",
    ),
    portfolio_name: str = typer.Option(  # noqa: B008
        "my_portfolio",
        prompt=True,
        help="Name of your portfolio",
    ),
    coinbase_pro_api_pass: str = typer.Option(  # noqa: B008
        ...,
        prompt=True,
        help="API passphrase for coinbase pro",
    ),
    coinbase_pro_api_secret: str = typer.Option(  # noqa: B008
        ...,
        prompt=True,
        help="API secret for coinbase pro",
    ),
    coinbase_pro_api_key: str = typer.Option(  # noqa: B008
        ...,
        prompt=True,
        help="API key for coinbase pro",
    ),
    key_passphrase: Optional[str] = typer.Option(  # noqa: B008
        None,
        help="Key passphrase to encrypt secrets in the config",
        envvar=ENV_VAR_KEY_PASSPHRASE,
    ),
) -> None:
    init_logging(LogLevel.INFO)

    if key_passphrase is None:
        key_passphrase = random_salt(128)

    config_dict = make_sample_config_dict(
        key_passphrase,
        random_salt(),
        random_salt(),
        username,
        admin_password,
        random_salt(),
        guest_password,
        random_salt(),
        portfolio_name,
        coinbase_pro_api_key,
        coinbase_pro_api_secret,
        coinbase_pro_api_pass,
        "0.0.0.0",  # noqa: S104
        random_salt(128),
    )
    output.write_text(toml.dumps(config_dict), encoding="utf-8")

    logger.info(
        "Put the following line in environment file at deployment site "
        "(Usually /home/gitlab-runner/c2ba-worker-app/.env):\n"
        f"{ENV_VAR_KEY_PASSPHRASE}={key_passphrase}"
    )


def compute_crypto_assets_allocations(
    asset_collection: AssetCollection,
    default_weight: Decimal,
    custom_weights: Dict[str, Decimal],
) -> Dict[str, Decimal]:
    def _recursive_walk(
        collection_key_prefix: str,
        collection_name: str,
        asset_collection: AssetCollection,
        current_weight: Decimal,
        out_weights: Dict[str, Decimal],
    ) -> None:
        out_weights_in_collection = {}
        for asset in asset_collection.assets:
            out_weights_in_collection[asset] = custom_weights.get(
                f"{collection_key_prefix}{asset}", default_weight
            )

        for subcollection_name in asset_collection.sub_collections:
            out_weights_in_collection[subcollection_name] = custom_weights.get(
                f"{collection_key_prefix}{subcollection_name}", default_weight
            )

        sum_weights = Decimal("0")
        for weight in out_weights_in_collection.values():
            sum_weights += weight
        for key in out_weights_in_collection:
            out_weights_in_collection[key] /= sum_weights

        for asset in asset_collection.assets:
            out_weights[asset] = (
                out_weights.setdefault(asset, Decimal("0"))
                + current_weight * out_weights_in_collection[asset]
            )

        for (
            subcollection_name,
            subcollection,
        ) in asset_collection.sub_collections.items():
            _recursive_walk(
                f"{collection_key_prefix}{subcollection_name}.",
                f"{collection_key_prefix}{subcollection_name}",
                subcollection,
                current_weight * out_weights_in_collection[f"{subcollection_name}"],
                out_weights,
            )

    out_weights: Dict[str, Decimal] = {}
    _recursive_walk(
        "",
        "",
        asset_collection,
        Decimal("1"),
        out_weights,
    )

    sum_weights = Decimal("0")
    for weight in out_weights.values():
        sum_weights += weight

    out_allocs = {
        asset: Decimal("100") * weight / sum_weights
        for asset, weight in out_weights.items()
    }

    return out_allocs


def compute_portfolio_allocations(
    crypto_assets_allocs: Dict[str, Decimal],
    crypto_alloc: Decimal,
) -> Dict[str, Decimal]:
    if crypto_alloc > Decimal("100") or crypto_alloc < Decimal("0"):
        raise ValueError(f"Invalid crypto_alloc value: {crypto_alloc}")

    crypto_weight = crypto_alloc / Decimal("100")
    usdc_alloc = Decimal("1") - crypto_weight
    asset_allocs = {
        asset: alloc * crypto_weight for asset, alloc in crypto_assets_allocs.items()
    }
    asset_allocs["USDC"] = Decimal("100") * usdc_alloc

    return asset_allocs


if __name__ == "__main__":  # pragma: no cover
    app()

# Use case:
# - User go into a directory
# - User type c2ba-worker-config create PASSPHRASE
# - Template config is created inside directory, PASSPHRASE is put in .env file
# - User can encrypt values to fill fields inside config with c2ba-worker-config encrypt "blabla"
