import logging
import signal
from pathlib import (
    Path,
)
from threading import (
    Thread,
)
from time import (
    sleep,
)
from typing import (
    Any,
    Optional,
)

import typer
import uvicorn

from c2ba.worker.config import (
    ENV_VAR_REBALANCE_DRY_RUN,
    ENV_VAR_REBALANCE_OUTPUT_DIR,
    ENV_VAR_WORKER_SERVER_PORT,
    Config,
    worker_app_callback,
)
from c2ba.worker.rebalancer import run as run_rebalancer
from c2ba.worker.server import app as server_app
from c2ba.worker.server import get_config as server_get_config

logger = logging.getLogger(__name__)

app = typer.Typer(help="Start rebalance loop", callback=worker_app_callback)


def keep_running() -> bool:
    return True


@app.command()
def run(  # noqa: C901 run is too complex
    ctx: typer.Context,
    dry_run: bool = typer.Option(  # noqa: B008
        True,
        envvar=ENV_VAR_REBALANCE_DRY_RUN,
        help="If specified, do not execute orders on exchange.",
    ),
    output_dir: Optional[Path] = typer.Option(  # noqa: B008
        None,
        exists=True,
        file_okay=False,
        writable=True,
        envvar=ENV_VAR_REBALANCE_OUTPUT_DIR,
        help="Output path for reports, if not specified reports are only displayed in web UI",
    ),
    server_port: int = typer.Option(  # noqa: B008
        8000,
        envvar=ENV_VAR_WORKER_SERVER_PORT,
    ),
) -> None:
    global current_report
    global current_report_lock

    for signum in [signal.SIGINT, signal.SIGTERM]:
        signal.signal(signum, raise_exit)

    config = ctx.obj
    config.log_config()

    start_web_server(config, server_port)
    start_rebalancer(config, dry_run, output_dir)

    while keep_running():
        sleep(1)

    logger.info("Exiting rebalancer")


def start_web_server(
    config: Config,
    port: int,
) -> None:
    Thread(
        target=run_web_server,
        args=(
            config,
            port,
        ),
        daemon=True,
    ).start()


def run_web_server(config: Config, port: int) -> None:
    server_app.dependency_overrides[server_get_config] = lambda: config
    uvicorn.run(
        server_app,
        host=config.host(),
        port=port,
        ssl_keyfile=config.ssl_key_file(),
        ssl_certfile=config.ssl_cert_file(),
    )


def start_rebalancer(
    config: Config,
    dry_run: bool,
    output_dir: Optional[Path],
) -> None:
    Thread(
        target=run_rebalancer,
        args=(
            config,
            dry_run,
            output_dir,
        ),
        daemon=True,
    ).start()


def raise_exit(signum: int, frame: Any) -> None:
    raise SystemExit(f"Received {signal.Signals(signum).name}")


if __name__ == "__main__":  # pragma: no cover
    app()
