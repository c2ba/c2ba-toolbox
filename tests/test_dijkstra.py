from c2ba.utils.dijkstra import (
    Graph,
    dijkstra,
)


def test_dijkstra() -> None:
    graph = Graph()

    edges = [
        ("X", "A", 7),
        ("X", "B", 2),
        ("X", "C", 3),
        ("X", "E", 4),
        ("A", "B", 3),
        ("A", "D", 4),
        ("B", "D", 4),
        ("B", "H", 5),
        ("C", "L", 2),
        ("D", "F", 1),
        ("F", "H", 3),
        ("G", "H", 2),
        ("G", "Y", 2),
        ("I", "J", 6),
        ("I", "K", 4),
        ("I", "L", 4),
        ("J", "L", 1),
        ("K", "Y", 5),
    ]

    for edge in edges:
        graph.add_edge(*edge)

    assert dijkstra(graph, "X", "Y") == ["X", "B", "H", "G", "Y"]
