import json
import os
from pathlib import (
    Path,
)


def main() -> None:
    paths = []
    for root, _dirs, files in os.walk(
        Path(__file__).parent / "rebalance_bundles", topdown=False
    ):
        for name in (name for name in files if name.endswith(".json")):
            paths.append(os.path.join(root, name))

    for input_test_bundle in paths:
        with open(input_test_bundle, "r", encoding="utf-8") as in_file_io:
            rebalance_result = json.loads(in_file_io.read())
        out_file = input_test_bundle.replace(".json", ".html")
        print(f"Making report {out_file}")
        with open(out_file, "w", encoding="utf-8") as out_file_io:
            out_file_io.writelines(rebalance_result["html_report"])


if __name__ == "__main__":
    main()
