import json
from datetime import (
    datetime,
)
from pathlib import (
    Path,
)

from freezegun import (
    freeze_time,
)


@freeze_time("2021-01-01")
def main() -> None:
    paths = []
    for f in (Path(__file__).parent).iterdir():
        print(f)
        if f.suffix == ".json" and f.name != "exchange_context.json":
            paths.append(f)

    new_bundles = Path(__file__).parent
    new_bundles.mkdir(exist_ok=True)

    for input_test_bundle in paths:
        print(f"processing {input_test_bundle}")
        with open(input_test_bundle, "r", encoding="utf-8") as in_file_io:
            rebalance_result = json.loads(in_file_io.read())

        new_rebalance_result = rebalance_result

        compute_rebalance_output = new_rebalance_result["compute_rebalance_output"]

        del compute_rebalance_output["py/object"]
        compute_rebalance_output["start_time"] = datetime.now().strftime(
            "%Y-%m-%d %H:%M:%S"
        )
        for record in compute_rebalance_output["portfolio_records"]:
            del record["py/object"]
            for entry in record:
                if isinstance(record[entry], dict) and "py/reduce" in record[entry]:
                    record[entry] = record[entry]["py/reduce"][1]["py/tuple"][0]

        for record in compute_rebalance_output["rebalance_order_records"]:
            del record["py/object"]
            for entry in record:
                if isinstance(record[entry], dict) and "py/reduce" in record[entry]:
                    record[entry] = record[entry]["py/reduce"][1]["py/tuple"][0]

        compute_rebalance_output["total_usdc_fees"] = compute_rebalance_output[
            "total_usdc_fees"
        ]["py/reduce"][1]["py/tuple"][0]

        compute_rebalance_input = new_rebalance_result["compute_rebalance_input"]
        del compute_rebalance_input["py/object"]
        rebalance_config = compute_rebalance_input["rebalance_config"]
        del rebalance_config["py/object"]
        del rebalance_config["crypto_alloc"]
        del rebalance_config["btc_relative_alloc"]
        del rebalance_config["alt_coins"]
        del rebalance_config["usdc_alloc"]
        del rebalance_config["btc_alloc"]
        del rebalance_config["alt_alloc"]
        del rebalance_config["alt_unit_alloc"]

        rebalance_config["threshold"] = rebalance_config["threshold"]["py/reduce"][1][
            "py/tuple"
        ][0]
        rebalance_config["asset_allocs"] = rebalance_config["target_alloc"]
        del rebalance_config["target_alloc"]
        for entry in rebalance_config["asset_allocs"]:
            rebalance_config["asset_allocs"][entry] = rebalance_config["asset_allocs"][
                entry
            ]["py/reduce"][1]["py/tuple"][0]

        exchange_context = compute_rebalance_input["exchange_context"]
        del exchange_context["py/object"]
        for entry in exchange_context["asset_quantities"]:
            exchange_context["asset_quantities"][entry] = exchange_context[
                "asset_quantities"
            ][entry]["py/reduce"][1]["py/tuple"][0]

        out_file = new_bundles / input_test_bundle.name
        with open(out_file, "w", encoding="utf-8") as out_file_io:
            out_file_io.write(json.dumps(new_rebalance_result, indent=4))


if __name__ == "__main__":
    main()
