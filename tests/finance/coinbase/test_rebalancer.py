import json
import logging
import time
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from pathlib import (
    Path,
)
from threading import (
    Thread,
)
from typing import (
    Any,
    Dict,
)
from unittest.mock import (
    Mock,
)

import pytest
import requests
import uvicorn
from fastapi import (
    FastAPI,
    Request,
)
from fastapi.exceptions import (
    HTTPException,
)
from fastapi.responses import (
    JSONResponse,
)
from freezegun import (
    freeze_time,
)
from pytest_mock import (
    MockerFixture,
)
from pytest_regressions.file_regression import (
    FileRegressionFixture,
)
from pytest_subtests import (
    SubTests,
)

from c2ba.finance.coinbase.exchange import (
    CoinbaseExchange,
    ExchangeRequests,
    make_exchange_context,
)
from c2ba.finance.coinbase.rebalancer import (
    ComputeRebalanceOutput,
    ExchangeContext,
    RebalanceConfig,
    RebalanceResult,
    rebalance,
    round_from_precision,
)

logger = logging.getLogger(__name__)


@freeze_time("2012-01-14")
def test_rebalance_on_empty_portfolio(
    mocker: MockerFixture,
    file_regression: FileRegressionFixture,
) -> None:
    exchange = Mock()
    exchange_context = ExchangeContext(
        currencies={
            "BTC": {
                "max_precision": "0.00000001",
            },
            "USDC": {
                "max_precision": "0.000001",
            },
        },
        products={
            "BTC-USDC": {"base_increment": "0.00000001"},
        },
        asset_quantities={
            "BTC": Decimal(0),
            "USDC": Decimal(0),
        },
        fees={
            "taker_fee_rate": "0.005",
        },
        tickers={
            "BTC-USDC": {
                "price": "36436.1",
            },
        },
    )
    mocker.patch(
        "c2ba.finance.coinbase.rebalancer.make_exchange_context",
        return_value=exchange_context,
    )

    rebalance_config = RebalanceConfig(
        asset_allocs={
            "USDC": Decimal(100),
        },
        threshold=Decimal(0),
    )

    rebalance_result = rebalance(rebalance_config, exchange, dry_run=False)
    file_regression.check(
        json.dumps(
            rebalance_result.to_json(),
            indent=4,
        ).strip()
        + "\n",
        extension=".json",
    )


def test_rebalance_on_bundles(
    regression_bundle_runner: Any,
    mocker: MockerFixture,
) -> None:
    exchange = Mock()

    bundles = regression_bundle_runner.bundles("rebalance_bundles", "rebalance_bundles")
    with pytest.raises(StopIteration):
        bundle = next(bundles)
        while True:
            mocker.patch(
                "c2ba.finance.coinbase.rebalancer.make_exchange_context",
                return_value=ExchangeContext.from_json(
                    bundle.input["compute_rebalance_input"]["exchange_context"]
                ),
            )
            with freeze_time(bundle.input["compute_rebalance_output"]["start_time"]):
                rebalance_result = rebalance(
                    RebalanceConfig.from_json(
                        bundle.input["compute_rebalance_input"]["rebalance_config"]
                    ),
                    exchange,
                    dry_run=True,
                )
            rebalance_result.compute_rebalance_output.start_time = (
                fakedatetime_to_datetime(
                    rebalance_result.compute_rebalance_output.start_time
                )
            )
            assert rebalance_result is not None
            bundle = bundles.send(rebalance_result.to_json())


@freeze_time("2012-01-14")
def test_rebalance_can_buy_new_assets(
    mocker: MockerFixture,
    file_regression: FileRegressionFixture,
) -> None:
    exchange_context = make_test_exchange_context(
        asset_quantities={
            "BTC": Decimal("0.5"),
            "USDC": Decimal("5000"),
        }
    )

    # Putting some XLM and ETH in portfolio
    rebalance_config = RebalanceConfig(
        asset_allocs={
            "USDC": Decimal("50"),
            "BTC": Decimal("25"),
            "XLM": Decimal("12.5"),
            "ETH": Decimal("12.5"),
        },
        threshold=Decimal(0),
    )

    exchange = Mock()
    mocker.patch(
        "c2ba.finance.coinbase.rebalancer.make_exchange_context",
        return_value=exchange_context,
    )

    rebalance_result = rebalance(rebalance_config, exchange, dry_run=False)

    file_regression.check(
        json.dumps(
            rebalance_result.to_json(),
            indent=4,
        ).strip()
        + "\n",
        extension=".json",
    )


@freeze_time("2012-01-14")
def test_rebalance_dont_rebalance_if_trigger_line_cannot_rebalance(
    mocker: MockerFixture,
    file_regression: FileRegressionFixture,
) -> None:
    btc_value = Decimal("0.5")
    eth_value = Decimal("0.00160900")
    eth_btc = Decimal("0.03218")
    eth_qty = eth_value / eth_btc
    xlm_value = eth_value
    xlm_btc = Decimal("0.00000777")
    xlm_qty = xlm_value / xlm_btc  # Put the same funds in XLM
    exchange_context = make_test_exchange_context(
        asset_quantities={
            "BTC": btc_value,
            "ETH": Decimal("1.11")
            * eth_qty,  # High unbalance (+11%), enough to trigger
            "XLM": Decimal("1.01") * xlm_qty,  # Low unbalance, not enough to trigger
        }
    )

    btc_ratio = btc_value / (btc_value + eth_value + xlm_value)
    eth_ratio = eth_value / (btc_value + eth_value + xlm_value)
    xlm_ratio = xlm_value / (btc_value + eth_value + xlm_value)
    rebalance_config = RebalanceConfig(
        asset_allocs={
            "USDC": Decimal("0"),
            "BTC": Decimal("100") * btc_ratio,
            "ETH": Decimal("100") * eth_ratio,
            "XLM": Decimal("100") * xlm_ratio,
        },
        threshold=Decimal(5),  # 5% threshold
    )
    # In this context the portfolio should match the 5% threshold unbalance due to ETH (trigger line)
    # However this results in an invalid coinbase order (funs and size are too low according to exchange rules)
    # But the XLM line results in a valid coinbase order, with just a few tokens
    # The rebalance should not be triggered in that case

    exchange = Mock()
    mocker.patch(
        "c2ba.finance.coinbase.rebalancer.make_exchange_context",
        return_value=exchange_context,
    )
    rebalance_result = rebalance(rebalance_config, exchange, dry_run=False)

    assert rebalance_result.should_rebalance is False

    file_regression.check(
        json.dumps(
            rebalance_result.to_json(),
            indent=4,
        ).strip()
        + "\n",
        extension=".json",
    )


@freeze_time("2012-01-14")
def test_rebalance_can_sell_all_usdc(
    mocker: MockerFixture,
    file_regression: FileRegressionFixture,
) -> None:
    usdc_count = Decimal(5000)
    exchange_context = make_test_exchange_context(
        asset_quantities={
            "BTC": Decimal(0.5),
            "USDC": usdc_count,
        }
    )

    rebalance_config = RebalanceConfig(
        asset_allocs={
            "USDC": Decimal("0"),
            "BTC": Decimal("100"),
        },
        threshold=Decimal(0),
    )

    exchange = Mock()
    mocker.patch(
        "c2ba.finance.coinbase.rebalancer.make_exchange_context",
        return_value=exchange_context,
    )

    rebalance_result = rebalance(rebalance_config, exchange, dry_run=False)

    file_regression.check(
        json.dumps(
            rebalance_result.to_json(),
            indent=4,
        ).strip()
        + "\n",
        extension=".json",
    )

    found = False
    for order in rebalance_result.compute_rebalance_output.rebalance_order_records:
        if order.asset == "USDC":
            assert order.asset_qty == usdc_count
            found = True
            break

    assert found

    # todo: asserts that all USDCs are in rebalance and coinbase orders


def test_rebalance_can_sell_full_crypto_line() -> None:
    pass


def test_rebalance_can_sell_all_btcs() -> None:
    pass


def test_rebalance_can_handle_portfolio_without_btcs() -> None:
    # Define usdc+alt only portfolio and try to rebalance it
    pass


def test_rebalance_can_handle_portfolio_with_alts_only() -> None:
    # Define crypto only with alts and try to rebalance it
    pass


def test_rebalance_can_handle_portfolio_without_usdcs() -> None:
    # Define crypto only portfolio and try to rebalance it
    pass


def test_rebalance_dont_touch_lines_unreferenced_in_config() -> None:
    # Define a portfolio owning assets not referenced in rebalance config
    # The algo should not try to BUY / SELL them
    pass


def test_rebalance_percents_are_limited_to_assets_referenced_in_config() -> None:
    # Define a portfolio owning assets not referenced in rebalance config
    # The algo should compute percentages without taking these assets into account
    pass


def test_rebalance_can_sell_usdcs_to_buy_other_assets() -> None:
    # Define a portfolio having not enough BTC to buy all assets, but enough USDCs
    # The algorithm should be able to sell USDCs for BTCs, then BTCs for other assets
    pass


@freeze_time("2012-01-14")
def test_rebalance_raises_when_usdc_btc_ticker_is_not_in_exchange(
    mocker: MockerFixture,
) -> None:
    exchange_context = make_test_exchange_context(
        asset_quantities={
            "BTC": Decimal(0.5),
            "USDC": Decimal(5000),
        }
    )
    del exchange_context.tickers["BTC-USDC"]

    # Putting some XLM and ETH in portfolio
    rebalance_config = RebalanceConfig(
        asset_allocs={
            "USDC": Decimal("50"),
            "BTC": Decimal("25"),
            "XLM": Decimal("12.5"),
            "ETH": Decimal("12.5"),
        },
        threshold=Decimal(0),
    )

    exchange = Mock()
    mocker.patch(
        "c2ba.finance.coinbase.rebalancer.make_exchange_context",
        return_value=exchange_context,
    )

    with pytest.raises(ValueError, match="BTC-USDC not in all_tickers"):
        rebalance(rebalance_config, exchange, dry_run=False)


@freeze_time("2012-01-14")
def test_rebalance_raises_when_required_ticker_is_not_in_exchange(
    mocker: MockerFixture,
) -> None:
    exchange_context = make_test_exchange_context(
        asset_quantities={
            "BTC": Decimal(0.5),
            "USDC": Decimal(5000),
        }
    )
    del exchange_context.tickers["XLM-BTC"]

    # Putting some XLM and ETH in portfolio
    rebalance_config = RebalanceConfig(
        asset_allocs={
            "USDC": Decimal("50"),
            "BTC": Decimal("25"),
            "XLM": Decimal("10.5"),
            "ETH": Decimal("14.5"),
        },
        threshold=Decimal(0),
    )

    exchange = Mock()
    mocker.patch(
        "c2ba.finance.coinbase.rebalancer.make_exchange_context",
        return_value=exchange_context,
    )

    with pytest.raises(ValueError, match="XLM-BTC not in all_tickers"):
        rebalance(rebalance_config, exchange, dry_run=False)


def test_round_from_precision() -> None:
    assert round_from_precision("0.10000000") == 1
    assert round_from_precision("0.1") == 1
    assert round_from_precision("0.01") == 2
    assert round_from_precision("0.00001") == 5
    assert round_from_precision("1.0") == 0
    assert round_from_precision("1.0000") == 0
    assert round_from_precision("0.0010") == 3
    assert round_from_precision("1") == 0

    with pytest.raises(ValueError, match="Invalid precision"):
        round_from_precision("2")
    with pytest.raises(ValueError, match="Invalid precision"):
        round_from_precision("10")
    with pytest.raises(ValueError, match="Invalid precision"):
        round_from_precision("1.1")
    with pytest.raises(ValueError, match="Invalid precision"):
        round_from_precision("0.2")
    with pytest.raises(ValueError, match="Invalid precision"):
        round_from_precision("0.003")


def fakedatetime_to_datetime(dt: Any) -> datetime:  # FakeDatime
    return datetime(
        dt.year,
        dt.month,
        dt.day,
        dt.hour,
        dt.minute,
        dt.second,
        dt.microsecond,
        dt.tzinfo,
    )


def make_test_exchange_context(asset_quantities: Dict[str, Decimal]) -> ExchangeContext:
    return ExchangeContext(
        currencies={
            "BTC": {
                "max_precision": "0.00000001",
            },
            "USDC": {
                "max_precision": "0.000001",
            },
            "XLM": {
                "max_precision": "0.0000001",
            },
            "ETH": {
                "max_precision": "0.00000001",
            },
        },
        products={
            "BTC-USDC": {
                "base_min_size": "0.00100000",
                "base_max_size": "280.00000000",
                "quote_increment": "0.01000000",
                "base_increment": "0.00000001",
                "min_market_funds": "10",
                "max_market_funds": "1000000",
            },
            "XLM-BTC": {
                "base_min_size": "1.00000000",
                "base_max_size": "600000.00000000",
                "quote_increment": "0.00000001",
                "base_increment": "1.00000000",
                "min_market_funds": "0.001",
                "max_market_funds": "50",
            },
            "ETH-BTC": {
                "base_min_size": "0.01000000",
                "base_max_size": "2400.00000000",
                "quote_increment": "0.00001000",
                "base_increment": "0.00000001",
                "min_market_funds": "0.001",
                "max_market_funds": "80",
            },
        },
        # No XLM in portfolio
        asset_quantities=asset_quantities,
        fees={
            "taker_fee_rate": "0.005",
        },
        tickers={
            "BTC-USDC": {
                "price": "36436.1",
            },
            "XLM-BTC": {
                "price": "0.00000777",
            },
            "ETH-BTC": {
                "price": "0.03218",
            },
        },
    )


def test_make_exchange_context(
    original_datadir: Path,
    subtests: SubTests,
) -> None:
    requests_map: Dict[str, ExchangeRequests] = {}

    web_app = FastAPI(docs_url=None, redoc_url=None)
    web_app_port = get_port()

    @web_app.api_route("/{full_path:path}")
    def mock_route(request: Request) -> JSONResponse:
        path = request.path_params["full_path"]
        if path not in requests_map:
            raise HTTPException(
                status_code=404, detail=f"Path {path} not found in history"
            )
        history_request = requests_map[path]
        return JSONResponse(history_request["response_json"])

    def run_web_app(web_app: FastAPI) -> None:
        uvicorn.run(
            web_app,
            host="0.0.0.0",  # noqa: S104
            port=web_app_port,
        )

    api_url = f"http://localhost:{web_app_port}"
    Thread(target=run_web_app, args=(web_app,), daemon=True).start()

    wait_for_server(api_url)

    for bundle_path in (original_datadir / "exchange_context_bundles").iterdir():
        if bundle_path.suffix != ".json":
            continue
        with subtests.test(msg=f"test_make_exchange_context.{bundle_path.name}"):
            input_object = json.loads(bundle_path.read_text(encoding="utf-8"))

            requests_map = {
                f"{request['endpoint']}": request
                for request in input_object["requests_history"]
            }

            exchange = CoinbaseExchange(None, api_url)
            with exchange.record_history() as history:  # noqa: F841 type: ignore
                exchange_context = make_exchange_context(
                    exchange, input_object["currencies"]
                )

            assert exchange_context.to_json() == input_object["exchange_context"]


def wait_for_server(url: str) -> None:
    for _retry_count in range(32):
        try:
            requests.get(url)
            return
        except requests.exceptions.ConnectionError:
            pass
        time.sleep(1)
    raise AssertionError(f"{url} unreachable after timeout")


def get_port() -> int:
    from socket import (
        socket,
    )

    with socket() as s:
        s.bind(("", 0))
        return s.getsockname()[1]


def test_rebalance_result_methods() -> None:
    result = RebalanceResult(
        compute_rebalance_output=ComputeRebalanceOutput(
            start_time=Mock(),
            portfolio_records=[],
            rebalance_order_records=[],
            total_usdc_fees=Decimal("42.42"),
        ),
        compute_rebalance_input=Mock(),
        exchange_order_records=Mock(),
        should_rebalance=False,
        submit_orders_output=None,
        html_report=["  first_line", "second_line", "last line\n"],
    )
    assert result.total_usdc_fees == Decimal("42.42")
    assert result.get_html_report() == "first_line\nsecond_line\nlast line"
