from decimal import (
    Decimal,
)
from typing import (
    Any,
)

import pytest

import c2ba.finance.coinbase.rebalancer.take_profit.exceptions as errors
from c2ba.finance.coinbase.rebalancer.take_profit import (
    TakeProfitConfig,
    TakeProfitState,
    compute_all_time_high_from_candles,
    compute_take_profit_projection_dataframe,
    make_take_profit_state_from_candles,
)
from c2ba.utils import (
    make_decimals,
)


def test_compute_all_time_high_from_candles() -> None:
    assert Decimal(0) == compute_all_time_high_from_candles([])
    assert Decimal(12) == compute_all_time_high_from_candles(
        [[1615939200, 54138.01, 12, 56924.26, 58925.54, 22687.57433043]],
    )
    assert Decimal(42) == compute_all_time_high_from_candles(
        [[0, 0, 42, 0, 0, 0]],
    )
    assert Decimal(3) == compute_all_time_high_from_candles(
        [[1, 2, 3, 4, 5, 6]],
    )
    assert Decimal(9) == compute_all_time_high_from_candles(
        [
            [1, 2, 3, 4, 5, 6],
            [7, 8, 9, 10, 11, 12],
        ]
    )
    assert Decimal(9) == compute_all_time_high_from_candles(
        [
            [7, 8, 9, 10, 11, 12],
            [1, 2, 3, 4, 5, 6],
        ]
    )
    assert Decimal(15) == compute_all_time_high_from_candles(
        [
            [1, 2, 3, 4, 5, 6],
            [13, 14, 15, 16, 17, 18],
            [7, 8, 9, 10, 11, 12],
        ]
    )


def test_regression_compute_all_time_high_from_candles(
    regression_bundle_runner: Any,
) -> None:
    bundles = regression_bundle_runner.bundles("input_candles", "output_all_time_highs")
    with pytest.raises(StopIteration):
        bundle = next(bundles)
        while True:
            ath = compute_all_time_high_from_candles(bundle.input)
            bundle = bundles.send({"ath": str(ath)})


def test_regression_make_take_profit_state_from_candles_and_compute_crypto_alloc(
    regression_bundle_runner: Any,
) -> None:
    take_profit_configs = {
        "BTC-USD": TakeProfitConfig(
            crypto_allocs=make_decimals([76.4, 50.0, 38.2, 21.4, 14.6, 10.0]),
            prices=make_decimals([20000, 30000, 40000, 50000, 100000, 200000]),
        ),
        "XLM-EUR": TakeProfitConfig(
            crypto_allocs=make_decimals([100.0, 50.0, 25.0, 12.5, 6.25, 3.125]),
            prices=make_decimals([0.34, 0.35, 0.36, 0.37, 0.38, 0.39]),
        ),
    }

    bundles = regression_bundle_runner.bundles(
        "input_candles",
        "output_make_take_profit_state_from_candles_and_compute_crypto_alloc",
    )
    with pytest.raises(StopIteration):
        bundle = next(bundles)
        while True:
            take_profit_config = None
            for name, config in take_profit_configs.items():
                if bundle.name.startswith(name):
                    take_profit_config = config
            assert take_profit_config is not None
            state = make_take_profit_state_from_candles(
                take_profit_config, bundle.input
            )
            crypto_alloc = state.crypto_alloc()
            bundle = bundles.send(
                {
                    "crypto_alloc": str(crypto_alloc),
                    "state": state.to_json(),
                }
            )


def test_regression_compute_take_profit_projection_dataframe(
    regression_bundle_runner: Any,
) -> None:
    take_profit_configs = {
        "BTC-USD": TakeProfitConfig(
            crypto_allocs=make_decimals([76.4, 50.0, 38.2, 21.4, 14.6, 10.0]),
            prices=make_decimals([20000, 30000, 40000, 50000, 100000, 200000]),
        ),
        "XLM-EUR": TakeProfitConfig(
            crypto_allocs=make_decimals([100.0, 50.0, 25.0, 12.5, 6.25, 3.125]),
            prices=make_decimals([0.34, 0.35, 0.36, 0.37, 0.38, 0.39]),
        ),
    }

    bundles = regression_bundle_runner.bundles(
        "input_candles",
        "output_compute_take_profit_projection_dataframe",
        output_extension=".csv",
    )
    with pytest.raises(StopIteration):
        bundle = next(bundles)
        while True:
            take_profit_config = None
            for name, config in take_profit_configs.items():
                if bundle.name.startswith(name):
                    take_profit_config = config
            assert take_profit_config is not None
            state = make_take_profit_state_from_candles(
                take_profit_config, bundle.input
            )
            df = compute_take_profit_projection_dataframe(
                take_profit_config,
                state.ath,
                last_price=state.ath * Decimal("1.2"),
                current_take_profit_crypto_alloc=take_profit_config.crypto_allocs[1],
                portfolio_total=Decimal("10000"),
            )
            bundle = bundles.send(df)


def test_invalid_take_profit_config_raise_error() -> None:
    with pytest.raises(
        errors.TakeProfitConfigEmptyError,
    ):
        TakeProfitConfig(
            crypto_allocs=[],
            prices=[],
        )

    with pytest.raises(
        errors.TakeProfitConfigMatchError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(0)],
            prices=[],
        )

    with pytest.raises(
        errors.TakeProfitConfigMatchError,
    ):
        TakeProfitConfig(
            crypto_allocs=[],
            prices=[Decimal(0)],
        )

    with pytest.raises(
        errors.TakeProfitConfigMatchError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(10)],
            prices=[Decimal(1), Decimal(2)],
        )

    with pytest.raises(
        errors.TakeProfitConfigNonPercentageCryptoAllocError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(70), Decimal(50), Decimal(-1)],
            prices=[Decimal(0), Decimal(20), Decimal(30)],
        )

    with pytest.raises(
        errors.TakeProfitConfigNonPercentageCryptoAllocError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(110), Decimal(50), Decimal(10)],
            prices=[Decimal(0), Decimal(20), Decimal(30)],
        )

    with pytest.raises(
        errors.TakeProfitConfigNegativePriceError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(100), Decimal(50), Decimal(0)],
            prices=[Decimal(-10), Decimal(20), Decimal(30)],
        )

    with pytest.raises(
        errors.TakeProfitConfigNonDecreasingCryptoAllocsError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(70), Decimal(80), Decimal(10)],
            prices=[Decimal(10), Decimal(20), Decimal(30)],
        )

    with pytest.raises(
        errors.TakeProfitConfigNonDecreasingCryptoAllocsError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(70), Decimal(50), Decimal(70)],
            prices=[Decimal(10), Decimal(20), Decimal(30)],
        )

    with pytest.raises(
        errors.TakeProfitConfigNonIncreasingPricesError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(100), Decimal(50), Decimal(0)],
            prices=[Decimal(10), Decimal(5), Decimal(100)],
        )

    with pytest.raises(
        errors.TakeProfitConfigNonIncreasingPricesError,
    ):
        TakeProfitConfig(
            crypto_allocs=[Decimal(100), Decimal(50), Decimal(0)],
            prices=[Decimal(0), Decimal(50), Decimal(25)],
        )


def test_compute_take_profit_state_from_all_time_high() -> None:
    take_profit_config = TakeProfitConfig(
        crypto_allocs=[Decimal(10), Decimal(9), Decimal(5), Decimal(1)],
        prices=[Decimal(10), Decimal(50), Decimal(90), Decimal(100)],
    )

    # State increase as ath reaches new highs
    expecteds = [
        # new state, current state, current_ath, new high price
        # starting from 0 state
        (0, 0, 0.0, 0),
        (0, 0, 0.0, 9.99),
        (1, 0, 0.0, 10),
        (1, 0, 0.0, 15),
        (1, 0, 0.0, 49.99),
        (2, 0, 0.0, 50),
        (2, 0, 0.0, 89.99),
        (3, 0, 0.0, 90),
        (3, 0, 0.0, 99.9),
        (4, 0, 0.0, 100),
        (4, 0, 0.0, 150),
        (4, 0, 0.0, 1000),
        # starting from 2 state: state never decrease, so we expect 2 from the beginning
        (2, 2, 0.0, 0),
        (2, 2, 0.0, 9.99),
        (2, 2, 0.0, 10),
        (2, 2, 0.0, 15),
        (2, 2, 0.0, 49.99),
        (2, 2, 0.0, 50),
        (2, 2, 0.0, 89.99),
        (3, 2, 0.0, 90),
        (3, 2, 0.0, 99.9),
        (4, 2, 0.0, 100),
        (4, 2, 0.0, 150),
        (4, 2, 0.0, 1000),
        # starting from 4 state (final):state never decrease, so we expect 4 from the beginning
        (4, 4, 0.0, 0),
        (4, 4, 0.0, 9.99),
        (4, 4, 0.0, 10),
        (4, 4, 0.0, 15),
        (4, 4, 0.0, 49.99),
        (4, 4, 0.0, 50),
        (4, 4, 0.0, 89.99),
        (4, 4, 0.0, 90),
        (4, 4, 0.0, 99.9),
        (4, 4, 0.0, 100),
        (4, 4, 0.0, 150),
        (4, 4, 0.0, 1000),
    ]

    for expected_state, current_state, current_ath, high_price in expecteds:
        state = TakeProfitState(
            Decimal(str(current_ath)), current_state, take_profit_config
        )
        new_state = state.update(Decimal(str(high_price)))
        assert expected_state == new_state.state


def test_take_profit_state_crypto_alloc() -> None:
    take_profit_config = TakeProfitConfig(
        crypto_allocs=[Decimal(10), Decimal(9), Decimal(5), Decimal(1)],
        prices=[Decimal(10), Decimal(50), Decimal(90), Decimal(100)],
    )

    state = TakeProfitState(Decimal("0.5"), 0, take_profit_config)

    assert state.crypto_alloc() == Decimal("10")

    state = state.update(Decimal("10"))

    assert state.crypto_alloc() == Decimal("10")

    state = state.update(Decimal("30.0"))

    assert state.crypto_alloc() == Decimal("9.5")

    state = state.update(Decimal("15.0"))

    # crypto alloc never goes back to a previous value once a new ath has been reached
    assert state.crypto_alloc() == Decimal("9.5")

    state = state.update(Decimal("40.0"))

    assert state.crypto_alloc() == Decimal("9.25")

    state = state.update(Decimal("50.0"))

    assert state.crypto_alloc() == Decimal("9")

    state = state.update(Decimal("70.0"))

    assert state.crypto_alloc() == Decimal("7")

    # pump it
    state = state.update(Decimal("1000.0"))

    assert state.crypto_alloc() == Decimal("1")

    # dump it
    state = state.update(Decimal("1.0"))

    # stable alloc remains safe after the dump
    assert state.crypto_alloc() == Decimal("1")
