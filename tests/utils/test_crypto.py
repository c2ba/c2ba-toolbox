import pytest
import toml
from cryptography.fernet import (
    InvalidToken,
)
from typer.testing import (
    CliRunner,
)

from c2ba.utils import (
    crypto,
)


def test_key_decrypt_encrypt_with_salt() -> None:
    cli_runner = CliRunner(mix_stderr=False)

    passphrase = "j'aime les chiens"  # noqa: S105

    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "config",
            passphrase,
        ],
    )
    assert result.exit_code == 0
    config_str = result.stdout
    config = toml.loads(config_str)

    message = "j'apprend le japonais"
    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "--key-passphrase",
            passphrase,
            "--key-salt",
            config["key_salt"],
            "encrypt",
            message,
        ],
    )
    assert result.exit_code == 0
    encrypted_message = result.stdout.strip()

    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "--key-passphrase",
            passphrase,
            "--key-salt",
            config["key_salt"],
            "decrypt",
            encrypted_message,
        ],
    )
    assert result.exit_code == 0
    assert result.stdout.strip() == message


def test_key_decrypt_encrypt_with_config() -> None:
    cli_runner = CliRunner(mix_stderr=False)

    passphrase = "j'aime les chiens"  # noqa: S105

    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "config",
            passphrase,
        ],
    )
    assert result.exit_code == 0
    config_str = result.stdout

    message = "j'apprend le japonais"
    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "--key-passphrase",
            passphrase,
            "--key-config",
            config_str,
            "encrypt",
            message,
        ],
    )
    assert result.exit_code == 0
    encrypted_message = result.stdout.strip()

    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "--key-passphrase",
            passphrase,
            "--key-config",
            config_str,
            "decrypt",
            encrypted_message,
        ],
    )
    assert result.exit_code == 0
    assert result.stdout.strip() == message


def test_key_decrypt_encrypt_with_config_and_stdin() -> None:
    cli_runner = CliRunner(mix_stderr=False)

    passphrase = "j'aime les chiens"  # noqa: S105

    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "config",
            passphrase,
        ],
    )
    assert result.exit_code == 0
    config_str = result.stdout

    message = "j'apprend le japonais"
    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "--key-passphrase",
            passphrase,
            "--key-config",
            config_str,
            "encrypt",
        ],
        input=f"{message}",
    )
    assert result.exit_code == 0
    encrypted_message = result.stdout.strip()

    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "--key-passphrase",
            passphrase,
            "--key-config",
            config_str,
            "decrypt",
        ],
        input=f"{encrypted_message}",
    )
    assert result.exit_code == 0
    assert result.stdout.strip() == message


def test_key_decrypt_encrypt_with_config_mismatch_fails() -> None:
    cli_runner = CliRunner(mix_stderr=False)

    passphrase = "j'aime les chiens"  # noqa: S105

    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "config",
            "another passphrase",
        ],
    )
    assert result.exit_code == 0
    another_config_str = result.stdout

    message = "j'apprend le japonais"
    result = cli_runner.invoke(
        crypto.app,
        [
            "key",
            "--key-passphrase",
            passphrase,
            "--key-config",
            another_config_str,
            "encrypt",
            message,
        ],
    )
    assert result.exit_code != 0
    assert "Provided passphrase does not match config." in result.stderr


def test_decrypt_without_passphrase_fails() -> None:
    cli_runner = CliRunner(mix_stderr=False)
    encrypted = "gAAAAABgO7pmQgv3InAyVmlfqdkhdu8hhZiL68yur9bWdhgZqUckNZzNCaa9t_vD46DYM3Ii-Vzjg6JrDFd8rQ-ls2OgKs4nbw=="
    result = cli_runner.invoke(
        crypto.app,
        ["key", "decrypt", encrypted],
    )
    assert result.exit_code != 0
    assert "A key passphrase and salt must be provided." in result.stderr


def test_decrypt_without_salt_fails() -> None:
    cli_runner = CliRunner(mix_stderr=False)
    encrypted = "gAAAAABgO7pmQgv3InAyVmlfqdkhdu8hhZiL68yur9bWdhgZqUckNZzNCaa9t_vD46DYM3Ii-Vzjg6JrDFd8rQ-ls2OgKs4nbw=="
    result = cli_runner.invoke(
        crypto.app,
        ["key", "--key-passphrase", "this is a passphrase", "decrypt", encrypted],
    )
    assert result.exit_code != 0
    assert "A key passphrase and salt must be provided." in result.stderr


def test_encrypt_without_passphrase_fails() -> None:
    cli_runner = CliRunner(mix_stderr=False)
    message = "message to encrypt"
    result = cli_runner.invoke(
        crypto.app,
        ["key", "encrypt", message],
    )
    assert result.exit_code != 0
    assert "A key passphrase and salt must be provided." in result.stderr


def test_encrypt_without_salt_fails() -> None:
    cli_runner = CliRunner(mix_stderr=False)
    message = "message to encrypt"
    result = cli_runner.invoke(
        crypto.app,
        ["key", "--key-passphrase", "this is a passphrase", "encrypt", message],
    )
    assert result.exit_code != 0
    assert "A key passphrase and salt must be provided." in result.stderr


def test_password_hash_check() -> None:
    cli_runner = CliRunner(mix_stderr=False)
    password = "password to hash"  # noqa: S105
    result = cli_runner.invoke(
        crypto.app,
        ["password", "hash", password],
    )
    assert result.exit_code == 0

    password_hash = result.stdout.strip()
    result = cli_runner.invoke(
        crypto.app,
        ["password", "check", password, password_hash],
    )
    assert result.exit_code == 0
    assert "Success" in result.stdout

    result = cli_runner.invoke(
        crypto.app,
        ["password", "hash", "another password to hash"],
    )
    assert result.exit_code == 0
    other_password_hash = result.stdout.strip()

    result = cli_runner.invoke(
        crypto.app,
        ["password", "check", password, other_password_hash],
    )
    assert result.exit_code != 0
    assert "Failure" in result.stdout


def test_password_workflow() -> None:
    passphrase = "j'aime les chiens"  # noqa: S105
    salt = crypto.random_salt()
    hash_pass = crypto.hash_password(passphrase, salt)
    assert crypto.check_password(passphrase, hash_pass)
    assert crypto.get_salt_from_hash(hash_pass) == salt

    wrong_passphrase = "j'aime les chats"  # noqa: S105
    assert not crypto.check_password(wrong_passphrase, hash_pass)


def test_encrypter_with_wrong_passphrase_cannot_decrypt() -> None:
    passphrase = "j'aime les chiens"  # noqa: S105
    key = crypto.make_passphrase_config(passphrase)

    encrypter = crypto.Encrypter(passphrase, key.key_salt)

    msg = "ceci est un test"
    encrypted_msg = encrypter.encrypt(msg)

    assert encrypter.decrypt(encrypted_msg) == msg

    wrong_passphrase = "j'aime les chats"  # noqa: S105
    bad_encrypter = crypto.Encrypter(wrong_passphrase, key.key_salt)

    with pytest.raises(InvalidToken):
        assert bad_encrypter.decrypt(encrypted_msg) != msg
