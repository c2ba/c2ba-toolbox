from c2ba.dev import (
    __main__,
)
from c2ba.utils.testing import (
    run_test_nosubcommand_displays_help,
    run_test_subcommands,
    run_test_version,
)


def test_version() -> None:
    run_test_version(__main__)


def test_nosubcommand_displays_help() -> None:
    run_test_nosubcommand_displays_help(__main__)


def test_subcommands() -> None:
    run_test_subcommands(
        __main__,
        [
            "changelog",
            "release",
        ],
    )
