# Changelog

All notable changes to this project will be documented in this file

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Contrib 1 Lorem Ipsum. *awesome, amazing.* (by Awesome Contributor \<true.awesome.contributor@amazing.mail.io\>)

### Changed

- Contrib 4 Ipsum Lorem. *awesome, amazing.* (by Awesome Contributor)

## 1.28.3-alpha.14+special.strange.5

### Deprecated

- Contrib 6 Lorem Ipsum. *awesome, amazing.*

## 1.28.3-alpha.12+special.strange.5

### Fixed

- Contrib 3 Lorem Ipsum. *awesome, amazing.* (by Awesome Contributor \<true.awesome.contributor@amazing.mail.io\>)

## 1.0.2

### Added

- Contrib 2 Lorem Ipsum. (by Awesome Contributor \<true.awesome.contributor@amazing.mail.io\>)
- Contrib 7 Another Added. *another, added.*

### Other

- Contrib 5 Another thing. *a, few, other, tags.* (by true.awesome.contributor@amazing.mail.io)

---
*Generated with c2ba.changelog 0.1.0*
