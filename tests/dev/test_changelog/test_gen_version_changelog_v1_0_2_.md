# 1.0.2

## Added

- Contrib 2 Lorem Ipsum. (by Awesome Contributor \<true.awesome.contributor@amazing.mail.io\>)
- Contrib 7 Another Added. *another, added.*

## Other

- Contrib 5 Another thing. *a, few, other, tags.* (by true.awesome.contributor@amazing.mail.io)
