import shutil
from pathlib import (
    Path,
)
from unittest.mock import (
    Mock,
)

import pytest
import tomlkit
from _pytest.fixtures import (
    FixtureRequest,
)
from pytest_mock import (
    MockerFixture,
)
from pytest_regressions.common import (
    check_text_files,
)
from pytest_regressions.file_regression import (
    FileRegressionFixture,
)
from typer.testing import (
    CliRunner,
)

from c2ba.dev import (
    changelog,
)
from c2ba.utils import (
    pushd,
    run_command,
)
from c2ba.utils.logging import (
    LogLevel,
)
from c2ba.utils.testing import (
    run_test_nosubcommand_displays_help,
    run_test_version,
)

changelogs_fields = (
    "filename",
    "changelog_type",
    "description",
    "version",
    "tags",
    "remote_url",
    "merge_requests",
    "issues",
    "author_name",
    "author_email",
)

changelogs_inputs = [
    (
        "contrib-1",
        "Added",
        "Contrib 1 Lorem Ipsum",
        "unreleased",
        "awesome amazing",
        "https://amazing.repositories.com/the-repo",
        "48 5 9 named-merge-request",
        "4 7 named-issue 12",
        "Awesome Contributor",
        "true.awesome.contributor@amazing.mail.io",
    ),
    (
        "contrib-2",
        "Added",
        "Contrib 2 Lorem Ipsum",
        "1.0.2",
        "",
        "https://amazing.repositories.com/the-repo",
        "48 5 9 named-merge-request",
        "",
        "Awesome Contributor",
        "true.awesome.contributor@amazing.mail.io",
    ),
    (
        "contrib-3",
        "Fixed",
        "Contrib 3 Lorem Ipsum",
        "v1.28.3-alpha.12+special.strange.5",
        "awesome amazing",
        "https://amazing.repositories.com/the-repo",
        "",
        "4 7 named-issue 12",
        "Awesome Contributor",
        "true.awesome.contributor@amazing.mail.io",
    ),
    (
        "contrib-4",
        "Changed",
        "Contrib 4 Ipsum Lorem",
        "unreleased",
        "awesome amazing",
        "",
        "another-merge-request",
        "one-issue two-issue",
        "Awesome Contributor",
        "",
    ),
    (
        "contrib-5",
        "Other",
        "Contrib 5 Another thing",
        "v1.0.2",
        "a few other tags",
        "",
        "",
        "",
        "",
        "true.awesome.contributor@amazing.mail.io",
    ),
    (
        "contrib-6",
        "Deprecated",
        "Contrib 6 Lorem Ipsum",
        "v1.28.3-alpha.14+special.strange.5",
        "awesome amazing",
        "https://amazing.repositories.com/the-repo",
        "",
        "4 7 named-issue 12",
        "",
        "",
    ),
    (
        "contrib-7",
        "Added",
        "Contrib 7 Another Added",
        "1.0.2",
        "another added",
        "",
        "",
        "",
        "",
        "",
    ),
]


@pytest.mark.parametrize(
    changelogs_fields,
    changelogs_inputs,
    ids=[
        "contrib-1",
        "contrib-2",
        "contrib-3",
        "contrib-4",
        "contrib-5",
        "contrib-6",
        "contrib-7",
    ],
)
def test_add_changelog(
    filename: str,
    changelog_type: str,
    description: str,
    version: str,
    tags: str,
    remote_url: str,
    merge_requests: str,
    issues: str,
    author_name: str,
    author_email: str,
    tmp_path: Path,
    original_datadir: Path,
    request: FixtureRequest,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "add",
            "--type",
            changelog_type,
            "--description",
            description,
            "--filename",
            filename,
            "--tags",
            tags,
            "--version",
            version,
            "--remote-url",
            remote_url,
            "--merge-requests",
            merge_requests,
            "--issues",
            issues,
            "--author-name",
            author_name,
            "--author-email",
            author_email,
        ],
    )
    assert app_result.exit_code == 0

    changelogs_dir = tmp_path / "changelogs"
    assert changelogs_dir.exists()
    assert changelogs_dir.is_dir()

    version_dir = version
    if version != "unreleased" and not version.startswith("v"):
        version_dir = f"v{version_dir}"

    changelog_path = changelogs_dir / version_dir / f"{filename}.toml"
    assert changelog_path.exists()
    assert changelog_path.is_file()

    perform_file_regression_check(
        changelog_path,
        original_datadir / "reference_changelogs" / version_dir / f"{filename}.toml",
        request,
    )


def test_add_changelog_with_invalid_version_exits_with_error(
    tmp_path: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    version = "not_a_semantic_version"
    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "add",
            "--type",
            "Added",
            "--description",
            "Lorem Ipsum",
            "--filename",
            "branch-name",
            "--tags",
            "",
            "--version",
            version,
            "--remote-url",
            "",
            "--merge-requests",
            "",
            "--issues",
            "",
            "--author-name",
            "",
            "--author-email",
            "",
        ],
    )
    assert app_result.exit_code != 0


def test_add_existing_changelog_exits_with_error(
    tmp_path: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    filename = "changelog-filename"

    Path(tmp_path / "changelogs" / "v1.0.0").mkdir(parents=True)
    with open(tmp_path / "changelogs" / "v1.0.0" / f"{filename}.toml", "w") as file_io:
        file_io.write("\n")

    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "add",
            "--type",
            "Added",
            "--description",
            "Lorem Ipsum",
            "--filename",
            filename,
            "--tags",
            "",
            "--version",
            "v1.0.0",
            "--remote-url",
            "",
            "--merge-requests",
            "",
            "--issues",
            "",
            "--author-name",
            "",
            "--author-email",
            "",
        ],
    )
    assert app_result.exit_code != 0
    assert f"{filename}.toml already exists" in app_result.stderr


@pytest.mark.parametrize(
    "with_unreleased",
    [True, False],
    ids=["changelog_with_unreleased", "changelog_without_unreleased"],
)
def test_gen_full_changelog(
    with_unreleased: bool,
    tmp_path: Path,
    file_regression: FileRegressionFixture,
    datadir: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    prepare_changelogs(tmp_path, datadir)

    if not with_unreleased:
        # Test that having or not "unreleased" does not affect sorting of versions
        shutil.rmtree(tmp_path / "changelogs" / "unreleased")

    with open(tmp_path / "changelogs" / "spurious-file", "w") as spurious_file_io:
        spurious_file_io.write("This is a spurious file !")

    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "gen",
        ],
    )
    assert app_result.exit_code == 0

    file_regression.check(
        app_result.stdout, extension=".md", encoding="utf-8", newline="\n"
    )


def prepare_changelogs(
    tmp_path: Path,
    datadir: Path,
) -> None:
    shutil.copytree(datadir / "reference_changelogs", tmp_path / "changelogs")


@pytest.mark.parametrize(
    "version",
    [
        "unreleased",
        "1.0.2",
        "v1.0.2",
        "v1.28.3-alpha.12+special.strange.5",
        "v1.28.3-alpha.14+special.strange.5",
    ],
)
def test_gen_version_changelog(
    version: str,
    tmp_path: Path,
    file_regression: FileRegressionFixture,
    datadir: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    prepare_changelogs(tmp_path, datadir)

    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "gen",
            "--version",
            version,
        ],
    )
    assert app_result.exit_code == 0

    file_regression.check(
        app_result.stdout, extension=".md", encoding="utf-8", newline="\n"
    )


@pytest.mark.parametrize(
    "version",
    [
        "v0.1.0",
        "v0.2.0",
        "not_a_semantic_version",
    ],
)
def test_gen_invalid_version_changelog_exits_with_error(
    version: str,
    tmp_path: Path,
    datadir: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    prepare_changelogs(tmp_path, datadir)

    with open(tmp_path / "changelogs" / "v0.2.0", "w") as fake_changelog_io:
        fake_changelog_io.write("This is a not a directory !")

    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "gen",
            "--version",
            version,
        ],
    )
    assert app_result.exit_code != 0


def test_gen_version_changelog_with_invalid_changelog_exists_with_error(
    tmp_path: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    Path(tmp_path / "changelogs" / "v0.2.0").mkdir(parents=True)
    with open(
        tmp_path / "changelogs" / "v0.2.0" / "a-change-log.toml", "w"
    ) as invalid_changelog_io:
        # changelog without tool.changelog.version
        invalid_changelog_io.write('description = "Contrib 2 Lorem Ipsum"\n')
        invalid_changelog_io.write('type = "Added"\n')
        invalid_changelog_io.write("tags = []\n")

    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "gen",
            "--version",
            "0.2.0",
        ],
    )
    assert app_result.exit_code != 0
    assert " no tool.changelog.version entry" in app_result.stderr


def test_upgrade_changelogs(
    tmp_path: Path,
    original_datadir: Path,
    mocker: MockerFixture,
    request: FixtureRequest,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    prepare_changelogs(tmp_path, original_datadir)

    with open(
        tmp_path / "changelogs" / "unreleased" / "spurious-file", "w"
    ) as spurious_file_io:
        spurious_file_io.write("This is a spurious not toml file !")

    def upgrade_to_v1000_0_0(changelog: tomlkit.api._TOMLDocument) -> None:
        changelog["tool"]["changelog"]["version"] = "1000.0.0"

    def upgrade_to_v5000_0_0(changelog: tomlkit.api._TOMLDocument) -> None:
        changelog["tool"]["changelog"]["version"] = "5000.0.0"

    # Manually upgrade one changelog, this one should not change
    with open(
        tmp_path / "changelogs" / "unreleased" / "contrib-1.toml",
        "r+",
        encoding="utf-8",
        newline="\n",
    ) as file_io:
        contrib1_changelog = tomlkit.loads(file_io.read())
        upgrade_to_v1000_0_0(contrib1_changelog)
        upgrade_to_v5000_0_0(contrib1_changelog)
        file_io.seek(0)
        file_io.write(tomlkit.dumps(contrib1_changelog))

    mocker.patch(
        "c2ba.dev.changelog.migration_functions",
        {
            "1000.0.0": upgrade_to_v1000_0_0,
            "5000.0.0": upgrade_to_v5000_0_0,
        },
    )

    app_result = cli_runner.invoke(
        changelog.app,
        [
            "--project-path",
            str(tmp_path),
            "upgrade",
        ],
    )
    assert app_result.exit_code == 0

    Path(tmp_path / "changelogs" / "unreleased" / "spurious-file").unlink()

    perform_directory_regression_check(
        tmp_path / "changelogs", original_datadir / "upgraded_changelogs", request
    )


def perform_file_regression_check(
    test_file: Path,
    reference_file: Path,
    request: FixtureRequest,
) -> None:
    if request.config.getoption("force_regen") or not reference_file.exists():
        reference_file.parent.mkdir(parents=True, exist_ok=True)
        shutil.copyfile(test_file, reference_file)
        pytest.fail(f"File not found in data directory, created:\n- {reference_file}.")
        return
    check_text_files(test_file, reference_file, encoding="utf-8")


def perform_directory_regression_check(
    test_directory: Path,
    reference_directory: Path,
    request: FixtureRequest,
) -> None:
    if request.config.getoption("force_regen") or not reference_directory.exists():
        if reference_directory.exists():
            shutil.rmtree(reference_directory)
        shutil.copytree(test_directory, reference_directory)
        pytest.fail(
            f"'Directory not found in data directory, created:\n- {reference_directory}."
        )
        return

    def _perform_directory_regression_check(
        test_directory: Path,
        reference_directory: Path,
    ) -> None:
        for dir_entry in reference_directory.iterdir():
            assert (test_directory / dir_entry.name).exists()

        for dir_entry in test_directory.iterdir():
            assert (reference_directory / dir_entry.name).exists()

            if dir_entry.is_dir():
                _perform_directory_regression_check(
                    dir_entry, reference_directory / dir_entry.name
                )
            else:
                check_text_files(
                    dir_entry, reference_directory / dir_entry.name, encoding="utf-8"
                )

    _perform_directory_regression_check(test_directory, reference_directory)


def test_given_a_poetry_project_with_repository_url_then_get_remote_url_returns_repository_url(
    tmp_path: Path,
) -> None:
    with open(tmp_path / "pyproject.toml", "w") as pyproject_io:
        pyproject_io.write("[tool.poetry]\n")
        pyproject_io.write('repository = "https://awesome-repo"\n')

    with pushd(tmp_path):
        assert changelog.get_remote_url() == "https://awesome-repo"


def test_given_a_poetry_project_without_repository_url_then_get_remote_url_returns_empty_string(
    tmp_path: Path,
) -> None:
    with open(tmp_path / "pyproject.toml", "w") as pyproject_io:
        pyproject_io.write("[tool.poetry]\n")
        pyproject_io.write('name = "a project without repository"\n')

    with pushd(tmp_path):
        assert changelog.get_remote_url() == ""


def test_given_a_git_repo_then_get_remote_url_returns_origin_url(
    tmp_path: Path,
) -> None:
    with pushd(tmp_path):
        run_command("git init")
        run_command("git remote add origin awesome-remote")
        assert changelog.get_remote_url() == "awesome-remote"


def test_given_a_git_repo_without_remote_then_get_remote_url_returns_empty_string(
    tmp_path: Path,
) -> None:
    with pushd(tmp_path):
        run_command("git init")
        assert changelog.get_remote_url() == ""


def test_given_a_git_repo_then_git_state_is_defined(
    tmp_path: Path,
) -> None:
    with pushd(tmp_path):
        run_command("git init")
        run_command("git remote add origin awesome-remote")
        run_command("git checkout -b awesome-branch")
        run_command("git config --local user.name awesome-user")
        run_command("git config --local user.email awesome-user@awesomemail.io")

        changelog.main(
            ctx=Mock(),
            log_level=LogLevel.INFO.value,
            version=False,
            project_path=tmp_path,
        )

        assert changelog.state["git.branch.name"] == "awesome-branch"
        assert changelog.state["git.user.name"] == "awesome-user"
        assert changelog.state["git.user.email"] == "awesome-user@awesomemail.io"
        assert changelog.state["remote.url"] == "awesome-remote"


def test_state_getter(mocker: MockerFixture) -> None:
    mocker.patch("c2ba.dev.changelog.state", {"mock.key": "mock.value"})
    assert changelog.state_getter("mock.key")() == "mock.value"


def test_given_a_changelog_needs_upgrade_then_a_warning_is_emitted(
    mocker: MockerFixture,
    datadir: Path,
) -> None:
    def upgrade_to_v0_3_0(changelog: tomlkit.api._TOMLDocument) -> None:
        changelog["tool"]["changelog"]["version"] = "0.3.0"

    def upgrade_to_v0_5_0(changelog: tomlkit.api._TOMLDocument) -> None:
        changelog["tool"]["changelog"]["version"] = "0.5.0"

    mocker.patch(
        "c2ba.dev.changelog.migration_functions",
        {
            "0.3.0": upgrade_to_v0_3_0,
            "0.5.0": upgrade_to_v0_5_0,
        },
    )
    logger = mocker.patch("c2ba.dev.changelog.logger")

    filepath = datadir / "reference_changelogs" / "unreleased" / "contrib-1.toml"
    changelog.Changelog(filepath)

    assert logger.warning.called
    logger.warning.assert_any_call(
        f"The changelog {filepath} has been upgraded for loading. "
        "Consider running 'upgrade' command to upgrade all changelogs"
    )


def test_version() -> None:
    run_test_version(changelog)


def test_nosubcommand_displays_help() -> None:
    run_test_nosubcommand_displays_help(changelog)
