import os
import shutil
from pathlib import (
    Path,
)

import pytest
from typer.testing import (
    CliRunner,
)

from c2ba.dev import (
    release,
)
from c2ba.utils import (
    pushd,
    run_command,
)
from c2ba.utils.testing import (
    run_test_nosubcommand_displays_help,
    run_test_version,
)

# c2ba-release gitlab create version_string --artifacts artifacts
# c2ba-release gitlab remove

version_python_file = Path("src") / "awesome_package" / "version.py"


@pytest.mark.parametrize(
    ("version", "with_changelogs"),
    [
        ("2.0.0", True),
        ("v2.0.0", False),
        ("v1.48.1-beta.12+special.strange.5", True),
        ("0.1.3-preview.14+special.strange.5", False),
    ],
)
def test_create_new_release(
    version: str, with_changelogs: bool, tmp_path: Path
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=with_changelogs, with_unreleased=True)

    version_tag = f"v{version}" if not version.startswith("v") else version

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", version],
    )
    assert app_result.exit_code == 0
    assert app_result.stdout.strip() == version_tag

    if with_changelogs:
        assert (tmp_path / "changelogs" / "unreleased").exists() is False
        assert (tmp_path / "changelogs" / version_tag).exists() is True

        for changelog_file_path in (
            Path(__file__).parent
            / "test_changelog"
            / "reference_changelogs"
            / "unreleased"
        ).iterdir():
            new_changelog_path = (
                tmp_path / "changelogs" / version_tag / changelog_file_path.name
            )
            assert new_changelog_path.exists() is True
            with open(new_changelog_path, "r") as f:
                changelog_content = f.read()
            with open(changelog_file_path, "r") as f:
                compare_to_content = f.read()
            assert changelog_content == compare_to_content

    with open(tmp_path / version_python_file, "r") as version_python_file_io:
        assert f'__version__ = "{version_tag[1:]}"\n' in version_python_file_io.read()

    with open(tmp_path / "pyproject.toml", "r") as pyproject_io:
        assert f'version = "{version_tag[1:]}"' in pyproject_io.read()

    with pushd(tmp_path):
        assert run_command("git describe --tags --dirty").strip() == f"{version_tag}"
        git_status = run_command("git status --porcelain")
        assert "changelogs/unreleased" not in git_status
        assert f"changelogs/{version_tag}" not in git_status
        assert f"{version_python_file}" not in git_status
        assert "pyproject.toml" not in git_status
        git_message = run_command("git log --pretty=%s -1")
        assert git_message.strip() == f"Release {version_tag}"


def test_create_new_release_in_dirty_repository_fails(tmp_path: Path) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=False, with_unreleased=True)
        with open(version_python_file, "w") as version_io:
            # Add change to a file but don't commit
            version_io.writelines('__version__ = "0.1.0-preview.1"\n')

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", "v0.2.0"],
    )
    assert app_result.exit_code != 0
    assert app_result.stderr != ""


def test_create_new_release_with_invalid_poetry_version_fails(tmp_path: Path) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=False, with_unreleased=True)

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", "0.2.0-invalidpre"],
    )
    assert app_result.exit_code != 0
    assert app_result.stderr != ""


def test_create_new_release_with_existing_tag_fails(tmp_path: Path) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=False, with_unreleased=True)
        run_command("git tag v0.2.0")

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", "v0.2.0"],
    )
    assert app_result.exit_code != 0
    assert app_result.stderr != ""


def test_create_new_release_without_python_version_file_fails(tmp_path: Path) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=False, with_unreleased=True)
        os.remove(version_python_file)
        run_command("git add .")
        run_command('git commit -m "Remove pyproject file"')

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", "v0.2.0"],
    )
    assert app_result.exit_code != 0
    assert app_result.stderr != ""


def test_create_new_release_without_pyproject_file_fails(tmp_path: Path) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=False, with_unreleased=True)
        os.remove(Path("pyproject.toml"))
        run_command("git add .")
        run_command('git commit -m "Remove pyproject file"')

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", "v0.2.0"],
    )
    assert app_result.exit_code != 0
    assert app_result.stderr != ""


def test_create_new_release_with_pyproject_file_without_poetry_section_fails(
    tmp_path: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=False, with_unreleased=True)
        with open("pyproject.toml", "w") as pyproject_io:
            pyproject_io.write("")
        run_command("git add .")
        run_command('git commit -m "Empty pyproject file"')

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", "v0.2.0"],
    )
    assert app_result.exit_code != 0
    assert app_result.stderr != ""


def test_create_new_release_with_pyproject_file_without_poetry_project_name_fails(
    tmp_path: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    with pushd(tmp_path):
        make_test_repo(with_changelogs=False, with_unreleased=True)
        with open("pyproject.toml", "w") as pyproject_io:
            pyproject_io.write("[tool.poetry]\n")
            pyproject_io.write('version = "0.1.0-preview.0"\n')
        run_command("git add .")
        run_command('git commit -m "Empty pyproject file"')

    app_result = cli_runner.invoke(
        release.app,
        ["create", "--project-path", str(tmp_path), "new", "v0.2.0"],
    )
    assert app_result.exit_code != 0
    assert app_result.stderr != ""


def make_test_repo(with_changelogs: bool, with_unreleased: bool) -> None:
    if with_changelogs:
        datasets_dir = Path(__file__).parent / "test_changelog"
        shutil.copytree(datasets_dir / "reference_changelogs", "changelogs")

    with open("pyproject.toml", "w") as pyproject_io:
        pyproject_io.write("[tool.poetry]\n")
        pyproject_io.write('name = "awesome_package"\n')
        pyproject_io.write('version = "0.1.0-preview.0"\n')

    (Path("src") / "awesome_package").mkdir(parents=True)
    with open(version_python_file, "w") as version_io:
        version_io.writelines('__version__ = "0.1.0-preview.0"\n')

    if with_changelogs and with_unreleased:
        # Ensure unreleased changelogs folder exists before running the test
        assert (Path("changelogs") / "unreleased").exists() is True
    else:
        if (Path("changelogs") / "unreleased").exists():
            shutil.rmtree(Path("changelogs") / "unreleased")

    run_command("git init")
    run_command('git config --local user.email "amazing.contributor@awesome.com"')
    run_command('git config --local user.name "The Contributor"')
    run_command("git add .")
    run_command('git commit -m "Initial commit"')


def test_version() -> None:
    run_test_version(release)


def test_nosubcommand_displays_help() -> None:
    run_test_nosubcommand_displays_help(release)
