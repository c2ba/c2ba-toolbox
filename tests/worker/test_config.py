from copy import (
    deepcopy,
)
from dataclasses import (
    dataclass,
)
from decimal import (
    Decimal,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
    Dict,
)
from unittest.mock import (
    Mock,
)

import pytest
import toml
from typer.testing import (
    CliRunner,
)

from c2ba.utils.crypto import (
    Encrypter,
    check_password,
    hash_password,
    make_passphrase_config,
)
from c2ba.utils.testing import (
    scoped_environ,
)
from c2ba.worker.config import (
    AssetCollection,
    CoinbaseproConfig,
    Config,
    app,
    app_make_sample_config,
    compute_crypto_assets_allocations,
    compute_portfolio_allocations,
)


def test_coinbase_pro_config_from_env_vars() -> None:
    config = Mock()
    env = {
        "CBPRO_API_KEY": "the_api_key",
        "CBPRO_API_SECRET": "the_secret_key",
        "CBPRO_API_PASS": "the_pass",
    }
    with scoped_environ(env, False):
        cbpro_config = CoinbaseproConfig(
            {},
            config,
        )
        assert cbpro_config.api_key == env["CBPRO_API_KEY"]
        assert cbpro_config.api_secret == env["CBPRO_API_SECRET"]
        assert cbpro_config.api_pass == env["CBPRO_API_PASS"]


@dataclass
class WorkerConfig:
    private_config: Dict[str, Any]
    public_config: Dict[str, Any]
    passphrase: str


def test_worker_config_from_directory(
    tmp_path: Path,
    worker_config: WorkerConfig,
) -> None:
    passphrase = worker_config.passphrase
    private_config = worker_config.private_config
    public_config = worker_config.public_config

    config = Config(tmp_path, passphrase)

    config.log_config()

    user = config.load_user("test_user")
    assert user is not None
    assert user.username == "test_user"
    assert (
        user.admin_password_hash
        == public_config["users"]["test_user"]["admin_password_hash"]
    )
    assert (
        user.guest_password_hash
        == public_config["users"]["test_user"]["guest_password_hash"]
    )
    assert check_password(
        private_config["users"]["test_user"]["admin"], user.admin_password_hash
    )
    assert check_password(
        private_config["users"]["test_user"]["guest"], user.guest_password_hash
    )
    assert config.host() == private_config["host"]
    assert config.get("host", False) == public_config["host"]
    assert config.get("host", True) == private_config["host"]

    assert (
        config.webapp_token_auth_secret_key()
        == private_config["webapp_token_auth_secret_key"]
    )
    assert (
        config.get("webapp_token_auth_secret_key", False)
        == public_config["webapp_token_auth_secret_key"]
    )
    assert (
        config.get("webapp_token_auth_secret_key", True)
        == private_config["webapp_token_auth_secret_key"]
    )

    assert config.get("", True) == public_config
    assert config.get("", False) == public_config

    portfolios = config.get_portfolios()
    assert portfolios.keys() == private_config["portfolios"].keys()


def test_worker_config_encrypt_command(
    tmp_path: Path,
    worker_config: WorkerConfig,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    message = "a good message !"
    result = cli_runner.invoke(
        app,
        [
            "--config-dir-path",
            str(tmp_path),
            "--key-passphrase",
            worker_config.passphrase,
            "encrypt",
            message,
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    encrypted_output = result.stdout.strip()

    config = Config(tmp_path, worker_config.passphrase)
    assert message == config.decrypt(encrypted_output)


def test_worker_config_make_sample(
    tmp_path: Path,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)
    test_passphrase = "this_is_a_test_passphrase_77"  # noqa: S105
    test_username = "test_user"
    test_admin_password = "test_admin_password_92"  # noqa: S105
    test_guest_password = "test_guest_password_29"  # noqa: S105
    test_portfolio_name = "test_portfolio"
    test_portfolio_cbpro_api_key = "cbpro_api_key"
    test_portfolio_cbpro_api_secret = "cbpro_api_secret"  # noqa: S105
    test_portfolio_cbpro_api_pass = "cbpro_api_pass"  # noqa: S105
    result = cli_runner.invoke(
        app_make_sample_config,
        [
            str(tmp_path / "config.toml"),
            "--username",
            test_username,
            "--admin-password",
            test_admin_password,
            "--guest-password",
            test_guest_password,
            "--portfolio-name",
            test_portfolio_name,
            "--coinbase-pro-api-pass",
            test_portfolio_cbpro_api_pass,
            "--coinbase-pro-api-secret",
            test_portfolio_cbpro_api_secret,
            "--coinbase-pro-api-key",
            test_portfolio_cbpro_api_key,
            "--key-passphrase",
            test_passphrase,
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    config_dict = toml.loads((tmp_path / "config.toml").read_text(encoding="utf-8"))

    # Check that the file is marked with a version, so we can handle migration later
    assert "tool" in config_dict
    assert "config" in config_dict["tool"]
    assert "version" in config_dict["tool"]["config"]

    config = Config(tmp_path, test_passphrase)

    assert len(config.get_users()) == 1
    user = config.load_user(test_username)
    assert user is not None
    assert check_password(
        test_admin_password,
        user.admin_password_hash,
    )
    assert check_password(
        test_guest_password,
        user.guest_password_hash,
    )

    asset_collections = config.get_asset_collections()
    assert len(asset_collections) == 1
    sample_collection_name = list(asset_collections.keys())[0]
    assert len(asset_collections[sample_collection_name].assets) == 0
    assert len(asset_collections[sample_collection_name].sub_collections) > 0

    portfolios = config.get_portfolios()
    assert len(portfolios) == 1

    assert test_portfolio_name in portfolios

    portfolio = portfolios[test_portfolio_name]
    assert portfolio.user == test_username
    assert portfolio.coinbasepro.api_key == test_portfolio_cbpro_api_key
    assert portfolio.coinbasepro.api_secret == test_portfolio_cbpro_api_secret
    assert portfolio.coinbasepro.api_pass == test_portfolio_cbpro_api_pass

    assert portfolio.rebalance.crypto_assets_collection == sample_collection_name
    assert portfolio.rebalance.crypto_assets_default_weight == 1
    assert len(portfolio.rebalance.crypto_assets_custom_weights) > 0

    assert len(portfolio.rebalance.crypto_alloc.crypto_allocs) > 0
    assert len(portfolio.rebalance.crypto_alloc.prices) > 0


def test_worker_config_get_command(
    tmp_path: Path,
    worker_config: WorkerConfig,
) -> None:
    config = Config(tmp_path, worker_config.passphrase)

    cli_runner = CliRunner(mix_stderr=False)

    prefix_args = [
        "--config-dir-path",
        str(tmp_path),
        "--key-passphrase",
        worker_config.passphrase,
    ]

    result = cli_runner.invoke(
        app,
        [
            *prefix_args,
            "get",
            "host",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    assert result.stdout.strip() == config.get("host", False)

    result = cli_runner.invoke(
        app,
        [*prefix_args, "get", "host", "--decrypt"],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    assert result.stdout.strip() == config.get("host", True)

    result = cli_runner.invoke(
        app,
        [
            *prefix_args,
            "get",
            "users",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    assert result.stdout.strip() == toml.dumps(config.get("users", False)).strip()

    result = cli_runner.invoke(
        app,
        [
            *prefix_args,
            "get",
            "portfolios.my_special_portfolio.rebalance.crypto_assets",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    assert (
        result.stdout.strip()
        == toml.dumps(
            config.get("portfolios.my_special_portfolio.rebalance.crypto_assets", False)
        ).strip()
    )


@pytest.fixture()
def worker_config(tmp_path: Path) -> WorkerConfig:
    """
    Create config in temporary directory and return it as dictionnaries for tests.
    """
    passphrase = "This_Is_a_passphrase_1234_"  # noqa: S105
    key = make_passphrase_config(passphrase)
    encrypter = Encrypter(passphrase, key.key_salt)

    private_config: Dict[str, Any] = {
        "host": "host.random.com",
        "webapp_token_auth_secret_key": "a secret key no one knows",
        "encrypt": key.__dict__,
        "users": {
            "test_user": {
                "admin": "the admin password of test_user",
                "guest": "the guest password of test_user",
            }
        },
        "asset_collections": {
            "collection1": {
                "assets": ["BTC", "LTC"],
            },
            "collection2": {
                "nicecoins": {"assets": ["BTC", "ETH"]},
                "shitcoins": {"assets": ["EOS", "BCH"]},
                "deficoins": {
                    "dexcoins": {"assets": ["UNI", "SUSHI"]},
                    "synthcoins": {"assets": ["SNX", "UMA"]},
                },
            },
        },
        "portfolios": {
            "my_special_portfolio": {
                "user": "test_user",
                "coinbasepro": {
                    "api_key": "my_special_portfolio api key",
                    "api_secret": "my_special_portfolio api secret",
                    "api_pass": "my_special_portfolio api pass",
                },
                "rebalance": {
                    "crypto_alloc": {
                        "watch": {
                            "product": "BTC-USD",
                            "ath_timeframe": "1d",
                            "ath_origin": "2021-01-01",
                        },
                        "take_profit_strategy": {
                            "crypto_allocs": [76.4, 50.0, 38.2, 21.4, 14.6, 10.0],
                            "prices": [64000, 90000, 120000, 150000, 200000, 250000],
                        },
                    },
                    "crypto_assets": {
                        "collection": "collection2",
                        "default_weight": 1.0,
                        "custom_weights": {
                            "shitcoins": 0.5,
                            "deficoins.synthcoins.SNX": 2,
                            "deficoins.dexcoins": 0.5,
                            "nicecoins.BTC": 3,
                        },
                    },
                    "threshold": "10",
                },
            }
        },
    }

    public_config = deepcopy(private_config)
    public_config["host"] = encrypter.encrypt(public_config["host"])
    public_config["webapp_token_auth_secret_key"] = encrypter.encrypt(
        public_config["webapp_token_auth_secret_key"]
    )
    public_config["users"]["test_user"]["admin_password_hash"] = hash_password(
        public_config["users"]["test_user"]["admin"]
    )
    del public_config["users"]["test_user"]["admin"]
    public_config["users"]["test_user"]["guest_password_hash"] = hash_password(
        public_config["users"]["test_user"]["guest"]
    )
    del public_config["users"]["test_user"]["guest"]

    for field in (
        "api_key",
        "api_secret",
        "api_pass",
    ):
        public_config["portfolios"]["my_special_portfolio"]["coinbasepro"][
            field
        ] = encrypter.encrypt(
            public_config["portfolios"]["my_special_portfolio"]["coinbasepro"][field]
        )

    with open(tmp_path / "config.toml", "w") as config_io:
        config_io.write(toml.dumps(public_config))

    return WorkerConfig(
        private_config,
        public_config,
        passphrase,
    )


def test_asset_collection_from_config_dict() -> None:
    asset_collection = AssetCollection.from_config_dict(
        {
            "A": {
                "assets": ["A1", "A2", "A3"],
                "A_1": {
                    "assets": ["A_11", "A_12"],
                    "A_1_1": {},
                },
                "A_2": {
                    "assets": [],
                    "A_2_2": {},
                    "A_2_3": {
                        "assets": ["A_2_31"],
                    },
                },
            },
            "B": {
                "B_1": {
                    "assets": ["B_11", "B_12"],
                    "B_2_1": {
                        "assets": ["B_2_11"],
                    },
                },
            },
            "C": {"assets": ["C_1"]},
        }
    )
    assert "A" in asset_collection.sub_collections
    assert "B" in asset_collection.sub_collections
    assert "C" in asset_collection.sub_collections
    assert len(asset_collection.sub_collections) == 3
    assert asset_collection.assets == []

    assert asset_collection.sub_collections["A"].assets == ["A1", "A2", "A3"]
    assert len(asset_collection.sub_collections["A"].sub_collections) == 2
    assert "A_1" in asset_collection.sub_collections["A"].sub_collections
    assert "A_2" in asset_collection.sub_collections["A"].sub_collections

    assert asset_collection.sub_collections["A"].sub_collections["A_1"].assets == [
        "A_11",
        "A_12",
    ]
    assert (
        "A_1_1"
        in asset_collection.sub_collections["A"].sub_collections["A_1"].sub_collections
    )
    assert asset_collection.sub_collections["A"].sub_collections["A_1"].sub_collections[
        "A_1_1"
    ] == AssetCollection([], {})

    assert asset_collection.sub_collections["A"].sub_collections["A_2"].assets == []
    assert asset_collection.sub_collections["A"].sub_collections["A_2"].sub_collections[
        "A_2_2"
    ] == AssetCollection([], {})
    assert asset_collection.sub_collections["A"].sub_collections["A_2"].sub_collections[
        "A_2_3"
    ].assets == ["A_2_31"]

    assert asset_collection.sub_collections["B"].assets == []
    assert "B_1" in asset_collection.sub_collections["B"].sub_collections
    assert asset_collection.sub_collections["B"].sub_collections["B_1"].assets == [
        "B_11",
        "B_12",
    ]
    assert asset_collection.sub_collections["B"].sub_collections["B_1"].sub_collections[
        "B_2_1"
    ].assets == ["B_2_11"]

    assert asset_collection.sub_collections["C"].assets == ["C_1"]
    assert len(asset_collection.sub_collections["C"].sub_collections) == 0


def test_compute_crypto_assets_allocations() -> None:
    all_assets = [
        "FAKE",
        "TRVE",
        "BTC",
        "ETH",
        "EOS",
        "BCH",
        "UNI",
        "SUSHI",
        "SNX",
        "UMA",
        "XLM",
        "NEO",
        "WTF",
        "BTC",
        "NOT",
    ]

    collection_dict = {
        "assets": ["FAKE", "TRVE"],
        "nicecoins": {"assets": ["BTC", "ETH"]},
        "shitcoins": {"assets": ["EOS", "BCH"]},
        "deficoins": {
            "dexcoins": {"assets": ["UNI", "SUSHI"]},
            "synthcoins": {"assets": ["SNX", "UMA"]},
        },
        "othercoins": {
            "othercoins1": {
                "othercoins1_1": {
                    "assets": ["XLM", "NEO"],
                },
                "othercoins1_2": {
                    "assets": ["UNI", "WTF"],  # UNI is also here
                },
            },
            "othercoins2": {"assets": ["BTC", "NOT"]},  # BTC is also here
        },
    }

    collection = AssetCollection.from_config_dict(collection_dict)

    # The default weight of each element (asset or collection), set if not specified in custom_weights
    default_weight = Decimal("2")
    # The custom weight of some elements
    custom_weights = {
        "TRVE": Decimal("0.1"),
        "nicecoins.BTC": Decimal("3"),
        "shitcoins": Decimal("0.5"),
        "shitcoins.BCH": Decimal("0"),
        "deficoins.synthcoins.SNX": Decimal("2"),
        "deficoins.dexcoins": Decimal("0.5"),
        "othercoins": Decimal("0.75"),
        "othercoins.othercoins1.othercoins1_1": Decimal("0"),
        "othercoins.othercoins2": Decimal("0.25"),
    }

    # Compute normalization factor of each collection from weights of elements
    root_factor = 1 / (
        custom_weights.get("FAKE", default_weight)
        + custom_weights.get("TRVE", default_weight)
        + custom_weights.get("nicecoins", default_weight)
        + custom_weights.get("shitcoins", default_weight)
        + custom_weights.get("deficoins", default_weight)
        + custom_weights.get("othercoins", default_weight)
    )
    nicecoins_factor = custom_weights.get("nicecoins", default_weight) / (
        custom_weights.get("nicecoins.BTC", default_weight)
        + custom_weights.get("nicecoins.ETH", default_weight)
    )
    shitcoins_factor = custom_weights.get("shitcoins", default_weight) / (
        custom_weights.get("shitcoins.EOS", default_weight)
        + custom_weights.get("shitcoins.BCH", default_weight)
    )
    deficoins_factor = custom_weights.get("deficoins", default_weight) / (
        custom_weights.get("deficoins.dexcoins", default_weight)
        + custom_weights.get("deficoins.synthcoins", default_weight)
    )
    dexcoins_factor = custom_weights.get("deficoins.dexcoins", default_weight) / (
        custom_weights.get("deficoins.dexcoins.UNI", default_weight)
        + custom_weights.get("deficoins.dexcoins.SUSHI", default_weight)
    )
    synthcoins_factor = custom_weights.get("deficoins.synthcoins", default_weight) / (
        custom_weights.get("deficoins.synthcoins.SNX", default_weight)
        + custom_weights.get("deficoins.synthcoins.UMA", default_weight)
    )
    othercoins_factor = custom_weights.get("othercoins", default_weight) / (
        custom_weights.get("othercoins.othercoins1", default_weight)
        + custom_weights.get("othercoins.othercoins2", default_weight)
    )
    othercoins1_factor = custom_weights.get(
        "othercoins.othercoins1", default_weight
    ) / (
        custom_weights.get("othercoins.othercoins1.othercoins1_1", default_weight)
        + custom_weights.get("othercoins.othercoins1.othercoins1_2", default_weight)
    )
    othercoins1_1_factor = custom_weights.get(
        "othercoins.othercoins1.othercoins1_1", default_weight
    ) / (
        custom_weights.get("othercoins.othercoins1.othercoins1_1.XLM", default_weight)
        + custom_weights.get("othercoins.othercoins1.othercoins1_1.NEO", default_weight)
    )
    othercoins1_2_factor = custom_weights.get(
        "othercoins.othercoins1.othercoins1_2", default_weight
    ) / (
        custom_weights.get("othercoins.othercoins1.othercoins1_2.UNI", default_weight)
        + custom_weights.get("othercoins.othercoins1.othercoins1_2.WTF", default_weight)
    )
    othercoins2_factor = custom_weights.get(
        "othercoins.othercoins2", default_weight
    ) / (
        custom_weights.get("othercoins.othercoins2.BTC", default_weight)
        + custom_weights.get("othercoins.othercoins2.NOT", default_weight)
    )

    # Compute expected weight of each asset based on its parent collections
    expected_weights = {
        "FAKE": root_factor * custom_weights.get("FAKE", default_weight),
        "TRVE": root_factor * custom_weights.get("TRVE", default_weight),
        "BTC": root_factor
        * (
            (nicecoins_factor * custom_weights.get("nicecoins.BTC", default_weight))
            + (
                othercoins_factor
                * othercoins2_factor
                * custom_weights.get("othercoins.othercoins2.BTC", default_weight)
            )
        ),  # Accumulate weights of BTC
        "ETH": root_factor
        * (nicecoins_factor * custom_weights.get("nicecoins.ETH", default_weight)),
        "EOS": root_factor
        * (shitcoins_factor * custom_weights.get("shitcoins.EOS", default_weight)),
        "BCH": root_factor
        * (shitcoins_factor * custom_weights.get("shitcoins.BCH", default_weight)),
        "UNI": root_factor
        * (
            (
                deficoins_factor
                * dexcoins_factor
                * custom_weights.get("deficoins.dexcoins.UNI", default_weight)
            )
            + (
                othercoins_factor
                * othercoins1_factor
                * othercoins1_2_factor
                * custom_weights.get(
                    "othercoins.othercoins1.othercoins1_2.UNI", default_weight
                )
            )
        ),  # Accumulate of UNI
        "SUSHI": root_factor
        * (
            deficoins_factor
            * dexcoins_factor
            * custom_weights.get("deficoins.dexcoins.SUSHI", default_weight)
        ),
        "SNX": root_factor
        * (
            deficoins_factor
            * synthcoins_factor
            * custom_weights.get("deficoins.synthcoins.SNX", default_weight)
        ),
        "UMA": root_factor
        * (
            deficoins_factor
            * synthcoins_factor
            * custom_weights.get("deficoins.synthcoins.UMA", default_weight)
        ),
        "XLM": root_factor
        * (
            othercoins_factor
            * othercoins1_factor
            * othercoins1_1_factor
            * custom_weights.get(
                "othercoins.othercoins1.othercoins1_1.XLM", default_weight
            )
        ),
        "NEO": root_factor
        * (
            othercoins_factor
            * othercoins1_factor
            * othercoins1_1_factor
            * custom_weights.get(
                "othercoins.othercoins1.othercoins1_1.NEO", default_weight
            )
        ),
        "WTF": root_factor
        * (
            othercoins_factor
            * othercoins1_factor
            * othercoins1_2_factor
            * custom_weights.get(
                "othercoins.othercoins1.othercoins1_2.WTF", default_weight
            )
        ),
        "NOT": root_factor
        * (
            othercoins_factor
            * othercoins2_factor
            * custom_weights.get("othercoins.othercoins2.NOT", default_weight)
        ),
    }
    # Normalize and convert to percentages
    sum_weights = Decimal("0")
    for weight in expected_weights.values():
        sum_weights += weight
    for asset in expected_weights:
        expected_weights[asset] = Decimal("100") * expected_weights[asset] / sum_weights

    crypto_asset_allocs = compute_crypto_assets_allocations(
        collection,
        default_weight=default_weight,
        custom_weights=custom_weights,
    )

    assert set(all_assets) == set(crypto_asset_allocs.keys())
    sum_allocs = Decimal("0")
    for alloc in crypto_asset_allocs.values():
        sum_allocs += alloc
    assert sum_allocs == Decimal("100")
    for asset in all_assets:
        assert expected_weights[asset] == pytest.approx(crypto_asset_allocs[asset])

    crypto_alloc = Decimal("100")
    usdc_alloc = Decimal("100") - crypto_alloc
    asset_allocations = compute_portfolio_allocations(
        crypto_asset_allocs,
        crypto_alloc,
    )
    assert set(all_assets + ["USDC"]) == set(asset_allocations.keys())
    sum_allocs = Decimal("0")
    for alloc in crypto_asset_allocs.values():
        sum_allocs += alloc
    assert sum_allocs == Decimal("100")
    assert asset_allocations["USDC"] == usdc_alloc
    for asset in all_assets:
        assert expected_weights[asset] * (
            crypto_alloc / Decimal("100")
        ) == pytest.approx(asset_allocations[asset])

    crypto_alloc = Decimal("25")
    usdc_alloc = Decimal("100") - crypto_alloc
    asset_allocations = compute_portfolio_allocations(crypto_asset_allocs, crypto_alloc)
    assert set(all_assets + ["USDC"]) == set(asset_allocations.keys())
    sum_allocs = Decimal("0")
    for alloc in asset_allocations.values():
        sum_allocs += alloc
    assert sum_allocs == Decimal("100")
    assert asset_allocations["USDC"] == usdc_alloc
    for asset in all_assets:
        assert expected_weights[asset] * (
            crypto_alloc / Decimal("100")
        ) == pytest.approx(asset_allocations[asset])

    crypto_alloc = Decimal("0")
    usdc_alloc = Decimal("100") - crypto_alloc
    asset_allocations = compute_portfolio_allocations(crypto_asset_allocs, crypto_alloc)
    assert set(all_assets + ["USDC"]) == set(asset_allocations.keys())
    sum_allocs = Decimal("0")
    for alloc in asset_allocations.values():
        sum_allocs += alloc
    assert sum_allocs == Decimal("100")
    assert asset_allocations["USDC"] == usdc_alloc
    for asset in all_assets:
        assert expected_weights[asset] * (
            crypto_alloc / Decimal("100")
        ) == pytest.approx(asset_allocations[asset])
