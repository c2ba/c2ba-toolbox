from copy import (
    deepcopy,
)
from dataclasses import (
    dataclass,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
    Dict,
)

import pytest
import toml
from pytest_mock import (
    MockerFixture,
)
from typer.testing import (
    CliRunner,
)

from c2ba.utils.crypto import (
    Encrypter,
    hash_password,
    make_passphrase_config,
)
from c2ba.worker.__main__ import (
    app,
)

# todo: test rebalancer directly instead => the above import fail at test discovery because of some type annot
# in webserver code


@dataclass
class WorkerConfig:
    private_config: Dict[str, Any]
    public_config: Dict[str, Any]
    passphrase: str


def test_worker_app(
    mocker: MockerFixture,
    tmp_path: Path,
    worker_config: WorkerConfig,
) -> None:
    cli_runner = CliRunner(mix_stderr=False)

    mocker.patch(
        "c2ba.worker.__main__.keep_running",
        return_value=False,
    )

    mocker.patch(
        "c2ba.worker.__main__.start_web_server",
    )
    mocker.patch(
        "c2ba.worker.__main__.start_rebalancer",
    )

    result = cli_runner.invoke(
        app,
        [
            "--config-dir-path",
            str(tmp_path),
            "--key-passphrase",
            worker_config.passphrase,
            "run",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0


@pytest.fixture()
def worker_config(tmp_path: Path) -> WorkerConfig:
    """
    Create config in temporary directory and return it as dictionnaries for tests.
    """
    passphrase = "This_Is_a_passphrase_1234_"  # noqa: S105
    key = make_passphrase_config(passphrase)
    encrypter = Encrypter(passphrase, key.key_salt)

    private_config: Dict[str, Any] = {
        "host": "host.random.com",
        "webapp_token_auth_secret_key": "a secret key no one knows",
        "encrypt": key.__dict__,
        "users": {
            "test_user": {
                "admin": "the admin password of test_user",
                "guest": "the guest password of test_user",
            }
        },
        "portfolios": {
            "my_special_portfolio": {
                "user": "test_user",
                "coinbasepro": {
                    "api_key": "my_special_portfolio api key",
                    "api_secret": "my_special_portfolio api secret",
                    "api_pass": "my_special_portfolio api pass",
                },
                "rebalance": {
                    "crypto_alloc": "14.2",
                    "btc_relative_alloc": "89.5",
                    "alt_coins": [
                        "BCH",
                        "EOS",
                        "ETH",
                    ],
                    "threshold": "10",
                },
            }
        },
    }

    public_config = deepcopy(private_config)
    public_config["host"] = encrypter.encrypt(public_config["host"])
    public_config["webapp_token_auth_secret_key"] = encrypter.encrypt(
        public_config["webapp_token_auth_secret_key"]
    )
    public_config["users"]["test_user"]["admin_password_hash"] = hash_password(
        public_config["users"]["test_user"]["admin"]
    )
    del public_config["users"]["test_user"]["admin"]
    public_config["users"]["test_user"]["guest_password_hash"] = hash_password(
        public_config["users"]["test_user"]["guest"]
    )
    del public_config["users"]["test_user"]["guest"]

    for field in (
        "api_key",
        "api_secret",
        "api_pass",
    ):
        public_config["portfolios"]["my_special_portfolio"]["coinbasepro"][
            field
        ] = encrypter.encrypt(
            public_config["portfolios"]["my_special_portfolio"]["coinbasepro"][field]
        )

    with open(tmp_path / "config.toml", "w") as config_io:
        config_io.write(toml.dumps(public_config))

    return WorkerConfig(
        private_config,
        public_config,
        passphrase,
    )
