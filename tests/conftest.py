import difflib
import json
import os
from pathlib import (
    Path,
)
from tempfile import (
    NamedTemporaryFile,
    TemporaryDirectory,
)
from typing import (
    Any,
    Callable,
    Generator,
    List,
    Optional,
)

import pytest
from _pytest.fixtures import (
    FixtureRequest,
)
from attr import (
    dataclass,
)
from pytest_subtests import (
    SubTests,
)

# Control precisely the test running environment to avoid any side effect
# from local devenv config
# Each test should specify specific environment variables if required
for key in os.environ:
    if key.startswith("C2BA_"):
        del os.environ[key]

# todo: keep an eye on https://github.com/tiangolo/fastapi/issues/2881 so we can put
# back this to the test
collect_ignore = ["worker/test_worker.py"]


@pytest.fixture()
def temporary_file() -> Generator[str, None, None]:
    # On windows we cannot use tempfile.NamedTemporaryFile() directly because
    # the file cannot be written while still open
    tmpf = NamedTemporaryFile(delete=False)
    tmpf.close()
    try:
        yield tmpf.name
    finally:
        os.unlink(tmpf.name)


# todo: not required, replace with tmp_path fixture from pytest
@pytest.fixture()
def temporary_dir() -> Generator[Path, None, None]:
    with TemporaryDirectory() as tmpdir:
        yield Path(tmpdir)


@dataclass
class Bundle:
    name: str
    filename: str
    filepath: Path
    input: Any  # noqa: A003


class RegressionBundleRunner:
    def __init__(
        self,
        original_datadir: Path,
        tmp_path: Path,
        request: FixtureRequest,
        subtests: SubTests,
    ) -> None:
        self.original_datadir = original_datadir
        self.tmp_path = tmp_path
        self.request = request
        self.subtests = subtests

    def bundles(
        self,
        input_directory: Path,
        output_directory: Path,
        input_extension: str = ".json",
        input_reader: Optional[Callable[[Path], Any]] = None,
        output_extension: str = ".json",
        output_writer: Optional[Callable[[Path, Any], None]] = None,
    ) -> Generator[Bundle, None, None]:
        _reader_from_extension = {
            ".json": lambda path: json.loads(path.read_text(encoding="utf-8"))
        }
        _writer_from_extension = {
            ".json": lambda path, data: path.write_text(
                json.dumps(data, indent=4).strip() + "\n",
                encoding="utf-8",
            ),
            ".csv": lambda path, data: path.write_text(
                data.to_csv(encoding="utf-8", line_terminator="\n"),
                encoding="utf-8",
            ),
            ".txt": lambda path, data: path.write_text(
                data,
                encoding="utf-8",
            ),
        }

        if not input_extension.startswith("."):
            input_extension = f".{input_extension}"

        if input_reader is None:
            input_reader = _reader_from_extension[input_extension]

        if not output_extension.startswith("."):
            output_extension = f".{output_extension}"

        if output_writer is None:
            output_writer = _writer_from_extension[output_extension]

        for input_path in (self.original_datadir / input_directory).iterdir():
            if not input_path.suffix.endswith(input_extension):
                continue

            with self.subtests.test(msg=f"{self.request.node.name}.{input_path.name}"):
                bundle_input = input_reader(input_path)

                bundle_output = yield Bundle(
                    name=input_path.stem,
                    filename=input_path.name,
                    filepath=input_path,
                    input=bundle_input,
                )
                assert bundle_output is not None

                output_path = (
                    self.original_datadir
                    / output_directory
                    / input_path.with_suffix(output_extension).name
                )

                if not output_path.exists() or self.request.config.getoption(
                    "force_regen"
                ):
                    output_path.parent.mkdir(exist_ok=True)
                    output_writer(output_path, bundle_output)
                    pytest.fail(f"Created {output_path}.")
                else:
                    candidate_output = (
                        self.tmp_path / input_path.with_suffix(output_extension).name
                    )
                    output_writer(candidate_output, bundle_output)
                    check_text_files(candidate_output, output_path, encoding="utf-8")


@pytest.fixture()
def regression_bundle_runner(
    original_datadir: Path,
    tmp_path: Path,
    request: FixtureRequest,
    subtests: SubTests,
) -> RegressionBundleRunner:
    return RegressionBundleRunner(original_datadir, tmp_path, request, subtests)


def check_text_files(
    obtained_fn: Any,
    expected_fn: Any,
    fix_callback: Callable[[List[str]], List[str]] = lambda x: x,
    encoding: Optional[str] = None,
) -> None:
    """
    Compare two files contents. If the files differ, show the diff and write a nice HTML
    diff file into the data directory.

    :param Path obtained_fn: path to obtained file during current testing.

    :param Path expected_fn: path to the expected file, obtained from previous testing.

    :param str encoding: encoding used to open the files.

    :param callable fix_callback:
        A callback to "fix" the contents of the obtained (first) file.
        This callback receives a list of strings (lines) and must also return a list of lines,
        changed as needed.
        The resulting lines will be used to compare with the contents of expected_fn.
    """
    __tracebackhide__ = True

    obtained_fn = Path(obtained_fn)
    expected_fn = Path(expected_fn)
    obtained_lines = fix_callback(obtained_fn.read_text(encoding=encoding).splitlines())
    expected_lines = expected_fn.read_text(encoding=encoding).splitlines()

    if obtained_lines != expected_lines:
        diff_lines = list(
            difflib.unified_diff(expected_lines, obtained_lines, lineterm="")
        )
        limit = 64
        if len(diff_lines) > limit:
            diff = [
                f"FILES DIFFER ON {len(diff_lines)} LINES (only showing first {limit}):",
                str(expected_fn),
                str(obtained_fn),
            ]
            diff_lines = diff_lines[:limit]
        else:
            diff = ["FILES DIFFER:", str(expected_fn), str(obtained_fn)]

        diff += diff_lines
        raise AssertionError("\n".join(diff))
